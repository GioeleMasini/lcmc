grammar FOOL;

@header{
	import java.util.ArrayList;
	import java.util.HashMap; 
	import ast.*;
}

@lexer::members {
  int lexicalErrors=0;
}

@members{
	private ArrayList<HashMap<String, STentry>> symTable = new ArrayList<HashMap<String, STentry>>();
	private int nestingLevel = -1;
	//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
	//il "fronte" della lista di tabelle � symTable.get(nestingLevel)
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/
  
/*
  DEFINIZIONE prog
  
  prog  
    : exp SEMIC                        
    | LET cllist declist IN exp SEMIC
    ;
*/
prog returns [Node ast]
  : e=exp SEMIC	{ $ast = new ProgNode($e.ast); }
	| LET 
	    {
		    nestingLevel++;
		    HashMap<String, STentry> hm = new HashMap<String, STentry>();
		    symTable.add(hm);
	    }
    c=cllist d=declist IN e=exp SEMIC 
	    {
		    symTable.remove(nestingLevel--);
		    $ast = new ProgLetInNode(new ArrayList<Node>($d.astlist), $e.ast);
	    }
	;

/*
  DEFINIZIONE cllist
  
  cllist  
    : ( CLASS ID (EXTENDS ID)? LPAR (ID COLON basic (COMMA ID COLON basic)* )? RPAR    
        CLPAR
          ( FUN ID COLON basic LPAR (ID COLON type (COMMA ID COLON type)* )? RPAR
              (LET (VAR ID COLON basic ASS exp SEMIC)* IN)? exp 
            SEMIC
          )*                
        CRPAR
      )*
    ; 
*/
cllist  
  : (CLASS ID (EXTENDS ID)? LPAR (ID COLON basic (COMMA ID COLON basic)* )? RPAR    
      CLPAR
        (FUN ID COLON basic LPAR (ID COLON type (COMMA ID COLON type)* )? RPAR
            (LET (VAR ID COLON basic ASS exp SEMIC)* IN)? exp 
          SEMIC
        )*                
      CRPAR
    )*
  ; 

/*
  DEFINIZIONE declist
  
  declist 
    : (
        (VAR ID COLON type ASS exp
          | FUN ID COLON basic LPAR (ID COLON type (COMMA ID COLON type)* )? RPAR 
          (LET declist IN)? exp 
        ) SEMIC 
      )*
    ;
*/
declist	returns [ArrayList<DecNode> astlist]
  :   {
		    $astlist = new ArrayList<DecNode>();
		    int offset = -2;
		  }
	  (
	    (VAR i=ID COLON t=type ASS e=exp
		    {
		      VarNode v = new VarNode($i.text, $t.ast, $e.ast); 
		      $astlist.add(v);
		      HashMap<String, STentry> hm = symTable.get(nestingLevel); 
          
		      if (hm.put($i.text, new STentry(nestingLevel, $t.ast, offset)) != null) {   // $t.ast
			      System.out.println("Var id " + $i.text + " at line " + $i.line + " already declared");
			      System.exit(0);
			    }
			    
          offset--;
          if ($t.ast instanceof ArrowTypeNode) {
            offset--; //Se la variabile contiene una funzione ha bisogno di offset doppio
          }
		    }  
		  | FUN i=ID COLON b=basic
		    {
		      //inserimento di ID nella symtable
		      FunNode f = new FunNode($i.text, $b.ast);
		      $astlist.add(f);
		      HashMap<String, STentry> hm = symTable.get(nestingLevel);
		      
		      STentry entry = new STentry(nestingLevel, offset); //separo introducendo "entry"
		      if (hm.put($i.text, entry) != null) {
		        System.out.println("Fun id " + $i.text + " at line " + $i.line + " already declared");
		        System.exit(0);
		      }
		      
          offset -= 2; //Le funzioni hanno bisogno di offset doppio
          
          //creare una nuova hashmap per la symTable
		      nestingLevel++;
		      HashMap<String, STentry> hmn = new HashMap<String, STentry>();
		      symTable.add(hmn);
		    }
	    LPAR 
		    {
		      ArrayList<Node> parTypes = new ArrayList<Node>();
		      int paroffset = 1;
		    } //
		  (fid=ID COLON fty=type
			  { 
			    parTypes.add($fty.ast); //
			    ParNode fpar = new ParNode($fid.text, $fty.ast);
			    f.addPar(fpar);
			    
			    if ($fty.ast instanceof ArrowTypeNode) {
			      paroffset++;
			    }
			    
			    if (hmn.put($fid.text, new STentry(nestingLevel, $fty.ast, paroffset)) != null) {
			      System.out.println("Parameter id " + $fid.text + " at line " + $fid.line + " already declared");
			      System.exit(0);
			    }
          
          paroffset++;
			  }
			  (COMMA id=ID COLON ty=type
				  {
				    parTypes.add($ty.ast); //
				    ParNode par = new ParNode($id.text, $ty.ast);
				    f.addPar(par);
          
	          if ($ty.ast instanceof ArrowTypeNode) {
	            paroffset++;
	          }
          
	          if (hmn.put($id.text, new STentry(nestingLevel, $ty.ast, paroffset)) != null) {
				      System.out.println("Parameter id " + $id.text + " at line " + $id.line + " already declared");
				      System.exit(0);
				    }
				    
            paroffset++;
				  }
				)*
      )? 
		  RPAR {
		    ArrowTypeNode funType = new ArrowTypeNode(parTypes, $b.ast);
		    entry.addType(funType);
		    f.setSymType(funType); 
		  } //
		  (LET d=declist IN)? e=exp 
			  {
			    //chiudere scope
			    symTable.remove(nestingLevel--);
			    f.addDecBody($d.astlist, $e.ast);
			  }
		  ) SEMIC
    )+          
	;

exp	returns [Node ast]
 	: f=term { $ast = $f.ast; }
 	  (PLUS l=term { $ast = new PlusNode($ast, $l.ast); }
 	    | MINUS l=term { $ast = new MinusNode($ast, $l.ast); }
	    | OR l=term { $ast = new OrNode($ast, $l.ast); }
 	  )*
 	;

term returns [Node ast]
	: f=factor { $ast = $f.ast; }
	  (TIMES l=factor { $ast = new MultNode($ast, $l.ast); }
	    | DIV l=factor { $ast = new DivNode($ast, $l.ast); }
	    | AND l=factor { $ast = new AndNode($ast, $l.ast); }
	  )*
	;

factor returns [Node ast]
	: f=value { $ast = $f.ast; }
	  (EQ l=value { $ast = new EqualNode($ast, $l.ast); }
	    | GE l=value { $ast = new GreaterEqualNode($ast, $l.ast); }
	    | LE l=value { $ast = new LowerEqualNode($ast, $l.ast); }
	  )*
 	;

value returns [Node ast]
  : n=INTEGER   
    { $ast = new IntNode(Integer.parseInt($n.text)); }
  | TRUE { $ast = new BoolNode(true); }
  | FALSE { $ast = new BoolNode(false); }
  | NULL { $ast = new NullNode(); }
  | NEW ID LPAR (exp (COMMA exp)* )? RPAR
  | IF x=exp THEN CLPAR y=exp CRPAR ELSE CLPAR z=exp CRPAR
    { $ast = new IfNode($x.ast, $y.ast, $z.ast); }
  | NOT LPAR e=exp RPAR { $ast = new NotNode($e.ast); }
  | PRINT LPAR e=exp RPAR { $ast = new PrintNode($e.ast); }
  | LPAR e=exp RPAR { $ast= $e.ast; }
  | i=ID
	    {
	      //cercare la dichiarazione
	      int j = nestingLevel;
	      STentry entry = null;
	      while (j >= 0 && entry == null) {
	        entry = (symTable.get(j--)).get($i.text);
	      }
	      if (entry == null) {
	        System.out.println("Id " + $i.text + " at line " + $i.line + " not declared");
	        System.exit(0);
	      }
	      $ast = new IdNode($i.text, entry, nestingLevel);
	    }
    (LPAR { ArrayList<Node> argList = new ArrayList<Node>(); }
      (fa=exp { argList.add($fa.ast); }
        (COMMA a=exp { argList.add($a.ast); })*
      )?
      RPAR { $ast = new CallNode($i.text, entry, argList, nestingLevel); }
    )?
  ;

type returns [Node ast]
  : b=basic { $ast = $b.ast; } 
  | a=arrow { $ast = $a.ast; }
  ;

basic returns [Node ast]
  : INT  { $ast = new IntTypeNode(); }
  | BOOL { $ast = new BoolTypeNode(); }
  | ID   { System.out.println("TODO Identificativo classi"); }
  ;

arrow returns [Node ast]
  : LPAR 
      {
        // Inizializzo l'array di parametri
        ArrayList<Node> parTypes = new ArrayList<Node>();
      }
    (fty=type
      {
        // Aggiungo il primo parametro all'array
        parTypes.add($fty.ast);
      }
    (COMMA ty=type
	    {
        // Aggiungo il tutti gli altri parametri all'array
	      parTypes.add($ty.ast);
	    }
    )*)? RPAR ARROW b=basic
      {
        // Creo il nodo arrow con il valore di ritorno e i parametri
        // Lo registro nella variabile $ast che verrà ritornata
        $ast = new ArrowTypeNode(parTypes, $b.ast);
      }
  ;


/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/
PLUS    : '+';
MINUS   : '-';
TIMES   : '*';
DIV     : '/';
LPAR    : '(';
RPAR    : ')';
CLPAR   : '{';
CRPAR   : '}';
SEMIC   : ';';
COLON   : ':'; 
COMMA   : ',';
DOT     : '.';
OR      : '||';
AND     : '&&';
NOT     : 'not';
GE      : '>=';
LE      : '<=';
EQ      : '==';  
ASS     : '=';
TRUE    : 'true';
FALSE   : 'false';
IF      : 'if';
THEN    : 'then';
ELSE    : 'else';
PRINT   : 'print';
LET     : 'let'; 
IN      : 'in';  
VAR     : 'var';
FUN     : 'fun'; 
CLASS   : 'class'; 
EXTENDS : 'extends'; 
NEW     : 'new'; 
NULL    : 'null';    
INT     : 'int';
BOOL    : 'bool';
ARROW   : '->';
INTEGER : '0' | ('-')?(('1'..'9')('0'..'9')*);

ID      : ('a'..'z'|'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9')*;

WHITESP : ( '\t' | ' ' | '\r' | '\n' )+ { $channel = HIDDEN; };

COMMENT : '/*' .* '*/' { $channel = HIDDEN; };
 
ERR     : . { System.out.println("Invalid char: " + $text); lexicalErrors++; $channel = HIDDEN; }; 
