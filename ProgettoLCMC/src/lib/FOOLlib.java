package lib;

import java.util.ArrayList;

import ast.ArrowTypeNode;
import ast.BoolTypeNode;
import ast.IntTypeNode;
import ast.Node;

public class FOOLlib {

	private static int labCount = 0;

	private static int funLabCount = 0;

	private static String funCode = "";

	// valuta se il tipo "a" � <= al tipo "b", dove "a" e "b" sono tipi di base:
	// int o bool
	public static boolean isSubtype(Node n1, Node n2) {
		Class<? extends Node> class1 = n1.getClass();

		if (class1.equals(ArrowTypeNode.class)) {
			Class<? extends Node> class2 = n2.getClass();

			// Verifico che entrambi gli elementi siano funzioni
			if (!class1.equals(class2)) {
				return false;
			}

			// Visto che sono funzioni ne faccio il cast a ArrowTypeNode
			ArrowTypeNode arrow1 = (ArrowTypeNode)n1;
			ArrowTypeNode arrow2 = (ArrowTypeNode)n2;

			// Verifico che il valore di ritorno di n2 sia un sottotipo di quello di n1
			// e che il numero di parametri che essi accettano sia identico
			if (!isSubtype(arrow1.getRet(), arrow2.getRet()) ||
					arrow1.getParList().size() != arrow2.getParList().size()) {
				return false;
			}

			ArrayList<Node> parameters1 = arrow1.getParList();
			ArrayList<Node> parameters2 = arrow2.getParList();

			// Verifico che ogni parametro di n1 sia un sottotipo del corrispondente parametro di n2
			for (int i = 0; i < parameters1.size(); i++) {
				if (!isSubtype(parameters2.get(i), parameters1.get(i))) {
					return false;
				}
			}

			// Se tutte le verifiche sono andate a buon fine n1 è un sottotipo di n2
			return true;
		} else {
			return n1.getClass().equals(n2.getClass())
					|| ((n1 instanceof BoolTypeNode) && (n2 instanceof IntTypeNode)); //
		}
	}

	public static String freshLabel() {
		return "label" + (labCount++);
	}

	public static String freshFunLabel() {
		return "function" + (funLabCount++);
	}

	public static void putCode(String c) {
		funCode += "\n" + c; // aggiunge una linea vuota di separazione prima di
								// funzione
	}

	public static String getCode() {
		return funCode;
	}

}