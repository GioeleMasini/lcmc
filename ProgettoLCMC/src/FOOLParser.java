// $ANTLR 3.5.2 /home/osboxes/workspace/ProgettoLCMC/FOOL.g 2017-04-24 21:56:54

	import java.util.ArrayList;
	import java.util.HashMap; 
	import ast.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ARROW", "ASS", "BOOL", 
		"CLASS", "CLPAR", "COLON", "COMMA", "COMMENT", "CRPAR", "DIV", "DOT", 
		"ELSE", "EQ", "ERR", "EXTENDS", "FALSE", "FUN", "GE", "ID", "IF", "IN", 
		"INT", "INTEGER", "LE", "LET", "LPAR", "MINUS", "NEW", "NOT", "NULL", 
		"OR", "PLUS", "PRINT", "RPAR", "SEMIC", "THEN", "TIMES", "TRUE", "VAR", 
		"WHITESP"
	};
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ARROW=5;
	public static final int ASS=6;
	public static final int BOOL=7;
	public static final int CLASS=8;
	public static final int CLPAR=9;
	public static final int COLON=10;
	public static final int COMMA=11;
	public static final int COMMENT=12;
	public static final int CRPAR=13;
	public static final int DIV=14;
	public static final int DOT=15;
	public static final int ELSE=16;
	public static final int EQ=17;
	public static final int ERR=18;
	public static final int EXTENDS=19;
	public static final int FALSE=20;
	public static final int FUN=21;
	public static final int GE=22;
	public static final int ID=23;
	public static final int IF=24;
	public static final int IN=25;
	public static final int INT=26;
	public static final int INTEGER=27;
	public static final int LE=28;
	public static final int LET=29;
	public static final int LPAR=30;
	public static final int MINUS=31;
	public static final int NEW=32;
	public static final int NOT=33;
	public static final int NULL=34;
	public static final int OR=35;
	public static final int PLUS=36;
	public static final int PRINT=37;
	public static final int RPAR=38;
	public static final int SEMIC=39;
	public static final int THEN=40;
	public static final int TIMES=41;
	public static final int TRUE=42;
	public static final int VAR=43;
	public static final int WHITESP=44;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public FOOLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return FOOLParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/osboxes/workspace/ProgettoLCMC/FOOL.g"; }


		private ArrayList<HashMap<String, STentry>> symTable = new ArrayList<HashMap<String, STentry>>();
		private int nestingLevel = -1;
		//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
		//il "fronte" della lista di tabelle � symTable.get(nestingLevel)



	// $ANTLR start "prog"
	// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:32:1: prog returns [Node ast] : (e= exp SEMIC | LET c= cllist d= declist IN e= exp SEMIC );
	public final Node prog() throws RecognitionException {
		Node ast = null;


		Node e =null;
		ArrayList<DecNode> d =null;

		try {
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:33:3: (e= exp SEMIC | LET c= cllist d= declist IN e= exp SEMIC )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==FALSE||(LA1_0 >= ID && LA1_0 <= IF)||LA1_0==INTEGER||LA1_0==LPAR||(LA1_0 >= NEW && LA1_0 <= NULL)||LA1_0==PRINT||LA1_0==TRUE) ) {
				alt1=1;
			}
			else if ( (LA1_0==LET) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:33:5: e= exp SEMIC
					{
					pushFollow(FOLLOW_exp_in_prog44);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog46); 
					 ast = new ProgNode(e); 
					}
					break;
				case 2 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:34:4: LET c= cllist d= declist IN e= exp SEMIC
					{
					match(input,LET,FOLLOW_LET_in_prog53); 

							    nestingLevel++;
							    HashMap<String, STentry> hm = new HashMap<String, STentry>();
							    symTable.add(hm);
						    
					pushFollow(FOLLOW_cllist_in_prog69);
					cllist();
					state._fsp--;

					pushFollow(FOLLOW_declist_in_prog73);
					d=declist();
					state._fsp--;

					match(input,IN,FOLLOW_IN_in_prog75); 
					pushFollow(FOLLOW_exp_in_prog79);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog81); 

							    symTable.remove(nestingLevel--);
							    ast = new ProgLetInNode(new ArrayList<Node>(d), e);
						    
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "prog"



	// $ANTLR start "cllist"
	// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:61:1: cllist : ( CLASS ID ( EXTENDS ID )? LPAR ( ID COLON basic ( COMMA ID COLON basic )* )? RPAR CLPAR ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )* CRPAR )* ;
	public final void cllist() throws RecognitionException {
		try {
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:62:3: ( ( CLASS ID ( EXTENDS ID )? LPAR ( ID COLON basic ( COMMA ID COLON basic )* )? RPAR CLPAR ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )* CRPAR )* )
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:62:5: ( CLASS ID ( EXTENDS ID )? LPAR ( ID COLON basic ( COMMA ID COLON basic )* )? RPAR CLPAR ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )* CRPAR )*
			{
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:62:5: ( CLASS ID ( EXTENDS ID )? LPAR ( ID COLON basic ( COMMA ID COLON basic )* )? RPAR CLPAR ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )* CRPAR )*
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( (LA10_0==CLASS) ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:62:6: CLASS ID ( EXTENDS ID )? LPAR ( ID COLON basic ( COMMA ID COLON basic )* )? RPAR CLPAR ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )* CRPAR
					{
					match(input,CLASS,FOLLOW_CLASS_in_cllist106); 
					match(input,ID,FOLLOW_ID_in_cllist108); 
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:62:15: ( EXTENDS ID )?
					int alt2=2;
					int LA2_0 = input.LA(1);
					if ( (LA2_0==EXTENDS) ) {
						alt2=1;
					}
					switch (alt2) {
						case 1 :
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:62:16: EXTENDS ID
							{
							match(input,EXTENDS,FOLLOW_EXTENDS_in_cllist111); 
							match(input,ID,FOLLOW_ID_in_cllist113); 
							}
							break;

					}

					match(input,LPAR,FOLLOW_LPAR_in_cllist117); 
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:62:34: ( ID COLON basic ( COMMA ID COLON basic )* )?
					int alt4=2;
					int LA4_0 = input.LA(1);
					if ( (LA4_0==ID) ) {
						alt4=1;
					}
					switch (alt4) {
						case 1 :
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:62:35: ID COLON basic ( COMMA ID COLON basic )*
							{
							match(input,ID,FOLLOW_ID_in_cllist120); 
							match(input,COLON,FOLLOW_COLON_in_cllist122); 
							pushFollow(FOLLOW_basic_in_cllist124);
							basic();
							state._fsp--;

							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:62:50: ( COMMA ID COLON basic )*
							loop3:
							while (true) {
								int alt3=2;
								int LA3_0 = input.LA(1);
								if ( (LA3_0==COMMA) ) {
									alt3=1;
								}

								switch (alt3) {
								case 1 :
									// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:62:51: COMMA ID COLON basic
									{
									match(input,COMMA,FOLLOW_COMMA_in_cllist127); 
									match(input,ID,FOLLOW_ID_in_cllist129); 
									match(input,COLON,FOLLOW_COLON_in_cllist131); 
									pushFollow(FOLLOW_basic_in_cllist133);
									basic();
									state._fsp--;

									}
									break;

								default :
									break loop3;
								}
							}

							}
							break;

					}

					match(input,RPAR,FOLLOW_RPAR_in_cllist140); 
					match(input,CLPAR,FOLLOW_CLPAR_in_cllist152); 
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:64:9: ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( (LA9_0==FUN) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:64:10: FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC
							{
							match(input,FUN,FOLLOW_FUN_in_cllist163); 
							match(input,ID,FOLLOW_ID_in_cllist165); 
							match(input,COLON,FOLLOW_COLON_in_cllist167); 
							pushFollow(FOLLOW_basic_in_cllist169);
							basic();
							state._fsp--;

							match(input,LPAR,FOLLOW_LPAR_in_cllist171); 
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:64:34: ( ID COLON type ( COMMA ID COLON type )* )?
							int alt6=2;
							int LA6_0 = input.LA(1);
							if ( (LA6_0==ID) ) {
								alt6=1;
							}
							switch (alt6) {
								case 1 :
									// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:64:35: ID COLON type ( COMMA ID COLON type )*
									{
									match(input,ID,FOLLOW_ID_in_cllist174); 
									match(input,COLON,FOLLOW_COLON_in_cllist176); 
									pushFollow(FOLLOW_type_in_cllist178);
									type();
									state._fsp--;

									// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:64:49: ( COMMA ID COLON type )*
									loop5:
									while (true) {
										int alt5=2;
										int LA5_0 = input.LA(1);
										if ( (LA5_0==COMMA) ) {
											alt5=1;
										}

										switch (alt5) {
										case 1 :
											// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:64:50: COMMA ID COLON type
											{
											match(input,COMMA,FOLLOW_COMMA_in_cllist181); 
											match(input,ID,FOLLOW_ID_in_cllist183); 
											match(input,COLON,FOLLOW_COLON_in_cllist185); 
											pushFollow(FOLLOW_type_in_cllist187);
											type();
											state._fsp--;

											}
											break;

										default :
											break loop5;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_cllist194); 
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:65:13: ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )?
							int alt8=2;
							int LA8_0 = input.LA(1);
							if ( (LA8_0==LET) ) {
								alt8=1;
							}
							switch (alt8) {
								case 1 :
									// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:65:14: LET ( VAR ID COLON basic ASS exp SEMIC )* IN
									{
									match(input,LET,FOLLOW_LET_in_cllist209); 
									// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:65:18: ( VAR ID COLON basic ASS exp SEMIC )*
									loop7:
									while (true) {
										int alt7=2;
										int LA7_0 = input.LA(1);
										if ( (LA7_0==VAR) ) {
											alt7=1;
										}

										switch (alt7) {
										case 1 :
											// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:65:19: VAR ID COLON basic ASS exp SEMIC
											{
											match(input,VAR,FOLLOW_VAR_in_cllist212); 
											match(input,ID,FOLLOW_ID_in_cllist214); 
											match(input,COLON,FOLLOW_COLON_in_cllist216); 
											pushFollow(FOLLOW_basic_in_cllist218);
											basic();
											state._fsp--;

											match(input,ASS,FOLLOW_ASS_in_cllist220); 
											pushFollow(FOLLOW_exp_in_cllist222);
											exp();
											state._fsp--;

											match(input,SEMIC,FOLLOW_SEMIC_in_cllist224); 
											}
											break;

										default :
											break loop7;
										}
									}

									match(input,IN,FOLLOW_IN_in_cllist228); 
									}
									break;

							}

							pushFollow(FOLLOW_exp_in_cllist232);
							exp();
							state._fsp--;

							match(input,SEMIC,FOLLOW_SEMIC_in_cllist245); 
							}
							break;

						default :
							break loop9;
						}
					}

					match(input,CRPAR,FOLLOW_CRPAR_in_cllist280); 
					}
					break;

				default :
					break loop10;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "cllist"



	// $ANTLR start "declist"
	// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:84:1: declist returns [ArrayList<DecNode> astlist] : ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON b= basic LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ ;
	public final ArrayList<DecNode> declist() throws RecognitionException {
		ArrayList<DecNode> astlist = null;


		Token i=null;
		Token fid=null;
		Token id=null;
		Node t =null;
		Node e =null;
		Node b =null;
		Node fty =null;
		Node ty =null;
		ArrayList<DecNode> d =null;

		try {
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:85:3: ( ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON b= basic LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ )
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:85:7: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON b= basic LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			{

					    astlist = new ArrayList<DecNode>();
					    int offset = -2;
					  
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:89:4: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON b= basic LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			int cnt15=0;
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( (LA15_0==FUN||LA15_0==VAR) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:90:6: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON b= basic LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC
					{
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:90:6: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON b= basic LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp )
					int alt14=2;
					int LA14_0 = input.LA(1);
					if ( (LA14_0==VAR) ) {
						alt14=1;
					}
					else if ( (LA14_0==FUN) ) {
						alt14=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 14, 0, input);
						throw nvae;
					}

					switch (alt14) {
						case 1 :
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:90:7: VAR i= ID COLON t= type ASS e= exp
							{
							match(input,VAR,FOLLOW_VAR_in_declist322); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist326); 
							match(input,COLON,FOLLOW_COLON_in_declist328); 
							pushFollow(FOLLOW_type_in_declist332);
							t=type();
							state._fsp--;

							match(input,ASS,FOLLOW_ASS_in_declist334); 
							pushFollow(FOLLOW_exp_in_declist338);
							e=exp();
							state._fsp--;


									      VarNode v = new VarNode((i!=null?i.getText():null), t, e); 
									      astlist.add(v);
									      HashMap<String, STentry> hm = symTable.get(nestingLevel); 
							          
									      if (hm.put((i!=null?i.getText():null), new STentry(nestingLevel, t, offset)) != null) {   // t
										      System.out.println("Var id " + (i!=null?i.getText():null) + " at line " + (i!=null?i.getLine():0) + " already declared");
										      System.exit(0);
										    }
										    
							          offset--;
							          if (t instanceof ArrowTypeNode) {
							            offset--; //Se la variabile contiene una funzione ha bisogno di offset doppio
							          }
									    
							}
							break;
						case 2 :
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:106:7: FUN i= ID COLON b= basic LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp
							{
							match(input,FUN,FOLLOW_FUN_in_declist356); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist360); 
							match(input,COLON,FOLLOW_COLON_in_declist362); 
							pushFollow(FOLLOW_basic_in_declist366);
							b=basic();
							state._fsp--;


									      //inserimento di ID nella symtable
									      FunNode f = new FunNode((i!=null?i.getText():null), b);
									      astlist.add(f);
									      HashMap<String, STentry> hm = symTable.get(nestingLevel);
									      
									      STentry entry = new STentry(nestingLevel, offset); //separo introducendo "entry"
									      if (hm.put((i!=null?i.getText():null), entry) != null) {
									        System.out.println("Fun id " + (i!=null?i.getText():null) + " at line " + (i!=null?i.getLine():0) + " already declared");
									        System.exit(0);
									      }
									      
							          offset -= 2; //Le funzioni hanno bisogno di offset doppio
							          
							          //creare una nuova hashmap per la symTable
									      nestingLevel++;
									      HashMap<String, STentry> hmn = new HashMap<String, STentry>();
									      symTable.add(hmn);
									    
							match(input,LPAR,FOLLOW_LPAR_in_declist381); 

									      ArrayList<Node> parTypes = new ArrayList<Node>();
									      int paroffset = 1;
									    
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:131:5: (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )?
							int alt12=2;
							int LA12_0 = input.LA(1);
							if ( (LA12_0==ID) ) {
								alt12=1;
							}
							switch (alt12) {
								case 1 :
									// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:131:6: fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )*
									{
									fid=(Token)match(input,ID,FOLLOW_ID_in_declist400); 
									match(input,COLON,FOLLOW_COLON_in_declist402); 
									pushFollow(FOLLOW_type_in_declist406);
									fty=type();
									state._fsp--;

									 
												    parTypes.add(fty); //
												    ParNode fpar = new ParNode((fid!=null?fid.getText():null), fty);
												    f.addPar(fpar);
												    
												    if (fty instanceof ArrowTypeNode) {
												      paroffset++;
												    }
												    
												    if (hmn.put((fid!=null?fid.getText():null), new STentry(nestingLevel, fty, paroffset)) != null) {
												      System.out.println("Parameter id " + (fid!=null?fid.getText():null) + " at line " + (fid!=null?fid.getLine():0) + " already declared");
												      System.exit(0);
												    }
									          
									          paroffset++;
												  
									// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:148:6: ( COMMA id= ID COLON ty= type )*
									loop11:
									while (true) {
										int alt11=2;
										int LA11_0 = input.LA(1);
										if ( (LA11_0==COMMA) ) {
											alt11=1;
										}

										switch (alt11) {
										case 1 :
											// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:148:7: COMMA id= ID COLON ty= type
											{
											match(input,COMMA,FOLLOW_COMMA_in_declist421); 
											id=(Token)match(input,ID,FOLLOW_ID_in_declist425); 
											match(input,COLON,FOLLOW_COLON_in_declist427); 
											pushFollow(FOLLOW_type_in_declist431);
											ty=type();
											state._fsp--;


															    parTypes.add(ty); //
															    ParNode par = new ParNode((id!=null?id.getText():null), ty);
															    f.addPar(par);
											          
												          if (ty instanceof ArrowTypeNode) {
												            paroffset++;
												          }
											          
												          if (hmn.put((id!=null?id.getText():null), new STentry(nestingLevel, ty, paroffset)) != null) {
															      System.out.println("Parameter id " + (id!=null?id.getText():null) + " at line " + (id!=null?id.getLine():0) + " already declared");
															      System.exit(0);
															    }
															    
											            paroffset++;
															  
											}
											break;

										default :
											break loop11;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_declist462); 

									    ArrowTypeNode funType = new ArrowTypeNode(parTypes, b);
									    entry.addType(funType);
									    f.setSymType(funType); 
									  
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:172:5: ( LET d= declist IN )?
							int alt13=2;
							int LA13_0 = input.LA(1);
							if ( (LA13_0==LET) ) {
								alt13=1;
							}
							switch (alt13) {
								case 1 :
									// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:172:6: LET d= declist IN
									{
									match(input,LET,FOLLOW_LET_in_declist472); 
									pushFollow(FOLLOW_declist_in_declist476);
									d=declist();
									state._fsp--;

									match(input,IN,FOLLOW_IN_in_declist478); 
									}
									break;

							}

							pushFollow(FOLLOW_exp_in_declist484);
							e=exp();
							state._fsp--;


										    //chiudere scope
										    symTable.remove(nestingLevel--);
										    f.addDecBody(d, e);
										  
							}
							break;

					}

					match(input,SEMIC,FOLLOW_SEMIC_in_declist500); 
					}
					break;

				default :
					if ( cnt15 >= 1 ) break loop15;
					EarlyExitException eee = new EarlyExitException(15, input);
					throw eee;
				}
				cnt15++;
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return astlist;
	}
	// $ANTLR end "declist"



	// $ANTLR start "exp"
	// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:182:1: exp returns [Node ast] : f= term ( PLUS l= term | MINUS l= term | OR l= term )* ;
	public final Node exp() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:183:3: (f= term ( PLUS l= term | MINUS l= term | OR l= term )* )
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:183:5: f= term ( PLUS l= term | MINUS l= term | OR l= term )*
			{
			pushFollow(FOLLOW_term_in_exp535);
			f=term();
			state._fsp--;

			 ast = f; 
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:184:5: ( PLUS l= term | MINUS l= term | OR l= term )*
			loop16:
			while (true) {
				int alt16=4;
				switch ( input.LA(1) ) {
				case PLUS:
					{
					alt16=1;
					}
					break;
				case MINUS:
					{
					alt16=2;
					}
					break;
				case OR:
					{
					alt16=3;
					}
					break;
				}
				switch (alt16) {
				case 1 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:184:6: PLUS l= term
					{
					match(input,PLUS,FOLLOW_PLUS_in_exp544); 
					pushFollow(FOLLOW_term_in_exp548);
					l=term();
					state._fsp--;

					 ast = new PlusNode(ast, l); 
					}
					break;
				case 2 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:185:9: MINUS l= term
					{
					match(input,MINUS,FOLLOW_MINUS_in_exp560); 
					pushFollow(FOLLOW_term_in_exp564);
					l=term();
					state._fsp--;

					 ast = new MinusNode(ast, l); 
					}
					break;
				case 3 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:186:8: OR l= term
					{
					match(input,OR,FOLLOW_OR_in_exp575); 
					pushFollow(FOLLOW_term_in_exp579);
					l=term();
					state._fsp--;

					 ast = new OrNode(ast, l); 
					}
					break;

				default :
					break loop16;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "exp"



	// $ANTLR start "term"
	// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:190:1: term returns [Node ast] : f= factor ( TIMES l= factor | DIV l= factor | AND l= factor )* ;
	public final Node term() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:191:2: (f= factor ( TIMES l= factor | DIV l= factor | AND l= factor )* )
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:191:4: f= factor ( TIMES l= factor | DIV l= factor | AND l= factor )*
			{
			pushFollow(FOLLOW_factor_in_term606);
			f=factor();
			state._fsp--;

			 ast = f; 
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:192:4: ( TIMES l= factor | DIV l= factor | AND l= factor )*
			loop17:
			while (true) {
				int alt17=4;
				switch ( input.LA(1) ) {
				case TIMES:
					{
					alt17=1;
					}
					break;
				case DIV:
					{
					alt17=2;
					}
					break;
				case AND:
					{
					alt17=3;
					}
					break;
				}
				switch (alt17) {
				case 1 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:192:5: TIMES l= factor
					{
					match(input,TIMES,FOLLOW_TIMES_in_term614); 
					pushFollow(FOLLOW_factor_in_term618);
					l=factor();
					state._fsp--;

					 ast = new MultNode(ast, l); 
					}
					break;
				case 2 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:193:8: DIV l= factor
					{
					match(input,DIV,FOLLOW_DIV_in_term629); 
					pushFollow(FOLLOW_factor_in_term633);
					l=factor();
					state._fsp--;

					 ast = new DivNode(ast, l); 
					}
					break;
				case 3 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:194:8: AND l= factor
					{
					match(input,AND,FOLLOW_AND_in_term644); 
					pushFollow(FOLLOW_factor_in_term648);
					l=factor();
					state._fsp--;

					 ast = new AndNode(ast, l); 
					}
					break;

				default :
					break loop17;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "term"



	// $ANTLR start "factor"
	// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:198:1: factor returns [Node ast] : f= value ( EQ l= value | GE l= value | LE l= value )* ;
	public final Node factor() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:199:2: (f= value ( EQ l= value | GE l= value | LE l= value )* )
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:199:4: f= value ( EQ l= value | GE l= value | LE l= value )*
			{
			pushFollow(FOLLOW_value_in_factor673);
			f=value();
			state._fsp--;

			 ast = f; 
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:200:4: ( EQ l= value | GE l= value | LE l= value )*
			loop18:
			while (true) {
				int alt18=4;
				switch ( input.LA(1) ) {
				case EQ:
					{
					alt18=1;
					}
					break;
				case GE:
					{
					alt18=2;
					}
					break;
				case LE:
					{
					alt18=3;
					}
					break;
				}
				switch (alt18) {
				case 1 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:200:5: EQ l= value
					{
					match(input,EQ,FOLLOW_EQ_in_factor681); 
					pushFollow(FOLLOW_value_in_factor685);
					l=value();
					state._fsp--;

					 ast = new EqualNode(ast, l); 
					}
					break;
				case 2 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:201:8: GE l= value
					{
					match(input,GE,FOLLOW_GE_in_factor696); 
					pushFollow(FOLLOW_value_in_factor700);
					l=value();
					state._fsp--;

					 ast = new GreaterEqualNode(ast, l); 
					}
					break;
				case 3 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:202:8: LE l= value
					{
					match(input,LE,FOLLOW_LE_in_factor711); 
					pushFollow(FOLLOW_value_in_factor715);
					l=value();
					state._fsp--;

					 ast = new LowerEqualNode(ast, l); 
					}
					break;

				default :
					break loop18;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "factor"



	// $ANTLR start "value"
	// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:206:1: value returns [Node ast] : (n= INTEGER | TRUE | FALSE | NULL | NEW ID LPAR ( exp ( COMMA exp )* )? RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | NOT LPAR e= exp RPAR | PRINT LPAR e= exp RPAR | LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )? );
	public final Node value() throws RecognitionException {
		Node ast = null;


		Token n=null;
		Token i=null;
		Node x =null;
		Node y =null;
		Node z =null;
		Node e =null;
		Node fa =null;
		Node a =null;

		try {
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:207:3: (n= INTEGER | TRUE | FALSE | NULL | NEW ID LPAR ( exp ( COMMA exp )* )? RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | NOT LPAR e= exp RPAR | PRINT LPAR e= exp RPAR | LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )? )
			int alt24=10;
			switch ( input.LA(1) ) {
			case INTEGER:
				{
				alt24=1;
				}
				break;
			case TRUE:
				{
				alt24=2;
				}
				break;
			case FALSE:
				{
				alt24=3;
				}
				break;
			case NULL:
				{
				alt24=4;
				}
				break;
			case NEW:
				{
				alt24=5;
				}
				break;
			case IF:
				{
				alt24=6;
				}
				break;
			case NOT:
				{
				alt24=7;
				}
				break;
			case PRINT:
				{
				alt24=8;
				}
				break;
			case LPAR:
				{
				alt24=9;
				}
				break;
			case ID:
				{
				alt24=10;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 24, 0, input);
				throw nvae;
			}
			switch (alt24) {
				case 1 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:207:5: n= INTEGER
					{
					n=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_value742); 
					 ast = new IntNode(Integer.parseInt((n!=null?n.getText():null))); 
					}
					break;
				case 2 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:209:5: TRUE
					{
					match(input,TRUE,FOLLOW_TRUE_in_value757); 
					 ast = new BoolNode(true); 
					}
					break;
				case 3 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:210:5: FALSE
					{
					match(input,FALSE,FOLLOW_FALSE_in_value765); 
					 ast = new BoolNode(false); 
					}
					break;
				case 4 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:211:5: NULL
					{
					match(input,NULL,FOLLOW_NULL_in_value773); 
					 ast = new NullNode(); 
					}
					break;
				case 5 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:212:5: NEW ID LPAR ( exp ( COMMA exp )* )? RPAR
					{
					match(input,NEW,FOLLOW_NEW_in_value781); 
					match(input,ID,FOLLOW_ID_in_value783); 
					match(input,LPAR,FOLLOW_LPAR_in_value785); 
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:212:17: ( exp ( COMMA exp )* )?
					int alt20=2;
					int LA20_0 = input.LA(1);
					if ( (LA20_0==FALSE||(LA20_0 >= ID && LA20_0 <= IF)||LA20_0==INTEGER||LA20_0==LPAR||(LA20_0 >= NEW && LA20_0 <= NULL)||LA20_0==PRINT||LA20_0==TRUE) ) {
						alt20=1;
					}
					switch (alt20) {
						case 1 :
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:212:18: exp ( COMMA exp )*
							{
							pushFollow(FOLLOW_exp_in_value788);
							exp();
							state._fsp--;

							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:212:22: ( COMMA exp )*
							loop19:
							while (true) {
								int alt19=2;
								int LA19_0 = input.LA(1);
								if ( (LA19_0==COMMA) ) {
									alt19=1;
								}

								switch (alt19) {
								case 1 :
									// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:212:23: COMMA exp
									{
									match(input,COMMA,FOLLOW_COMMA_in_value791); 
									pushFollow(FOLLOW_exp_in_value793);
									exp();
									state._fsp--;

									}
									break;

								default :
									break loop19;
								}
							}

							}
							break;

					}

					match(input,RPAR,FOLLOW_RPAR_in_value800); 
					}
					break;
				case 6 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:213:5: IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR
					{
					match(input,IF,FOLLOW_IF_in_value806); 
					pushFollow(FOLLOW_exp_in_value810);
					x=exp();
					state._fsp--;

					match(input,THEN,FOLLOW_THEN_in_value812); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value814); 
					pushFollow(FOLLOW_exp_in_value818);
					y=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value820); 
					match(input,ELSE,FOLLOW_ELSE_in_value822); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value824); 
					pushFollow(FOLLOW_exp_in_value828);
					z=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value830); 
					 ast = new IfNode(x, y, z); 
					}
					break;
				case 7 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:215:5: NOT LPAR e= exp RPAR
					{
					match(input,NOT,FOLLOW_NOT_in_value842); 
					match(input,LPAR,FOLLOW_LPAR_in_value844); 
					pushFollow(FOLLOW_exp_in_value848);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value850); 
					 ast = new NotNode(e); 
					}
					break;
				case 8 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:216:5: PRINT LPAR e= exp RPAR
					{
					match(input,PRINT,FOLLOW_PRINT_in_value858); 
					match(input,LPAR,FOLLOW_LPAR_in_value860); 
					pushFollow(FOLLOW_exp_in_value864);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value866); 
					 ast = new PrintNode(e); 
					}
					break;
				case 9 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:217:5: LPAR e= exp RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_value874); 
					pushFollow(FOLLOW_exp_in_value878);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value880); 
					 ast = e; 
					}
					break;
				case 10 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:218:5: i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )?
					{
					i=(Token)match(input,ID,FOLLOW_ID_in_value890); 

						      //cercare la dichiarazione
						      int j = nestingLevel;
						      STentry entry = null;
						      while (j >= 0 && entry == null) {
						        entry = (symTable.get(j--)).get((i!=null?i.getText():null));
						      }
						      if (entry == null) {
						        System.out.println("Id " + (i!=null?i.getText():null) + " at line " + (i!=null?i.getLine():0) + " not declared");
						        System.exit(0);
						      }
						      ast = new IdNode((i!=null?i.getText():null), entry, nestingLevel);
						    
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:232:5: ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )?
					int alt23=2;
					int LA23_0 = input.LA(1);
					if ( (LA23_0==LPAR) ) {
						alt23=1;
					}
					switch (alt23) {
						case 1 :
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:232:6: LPAR (fa= exp ( COMMA a= exp )* )? RPAR
							{
							match(input,LPAR,FOLLOW_LPAR_in_value904); 
							 ArrayList<Node> argList = new ArrayList<Node>(); 
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:233:7: (fa= exp ( COMMA a= exp )* )?
							int alt22=2;
							int LA22_0 = input.LA(1);
							if ( (LA22_0==FALSE||(LA22_0 >= ID && LA22_0 <= IF)||LA22_0==INTEGER||LA22_0==LPAR||(LA22_0 >= NEW && LA22_0 <= NULL)||LA22_0==PRINT||LA22_0==TRUE) ) {
								alt22=1;
							}
							switch (alt22) {
								case 1 :
									// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:233:8: fa= exp ( COMMA a= exp )*
									{
									pushFollow(FOLLOW_exp_in_value917);
									fa=exp();
									state._fsp--;

									 argList.add(fa); 
									// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:234:9: ( COMMA a= exp )*
									loop21:
									while (true) {
										int alt21=2;
										int LA21_0 = input.LA(1);
										if ( (LA21_0==COMMA) ) {
											alt21=1;
										}

										switch (alt21) {
										case 1 :
											// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:234:10: COMMA a= exp
											{
											match(input,COMMA,FOLLOW_COMMA_in_value930); 
											pushFollow(FOLLOW_exp_in_value934);
											a=exp();
											state._fsp--;

											 argList.add(a); 
											}
											break;

										default :
											break loop21;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_value955); 
							 ast = new CallNode((i!=null?i.getText():null), entry, argList, nestingLevel); 
							}
							break;

					}

					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "value"



	// $ANTLR start "type"
	// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:240:1: type returns [Node ast] : (b= basic |a= arrow );
	public final Node type() throws RecognitionException {
		Node ast = null;


		Node b =null;
		Node a =null;

		try {
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:241:3: (b= basic |a= arrow )
			int alt25=2;
			int LA25_0 = input.LA(1);
			if ( (LA25_0==BOOL||LA25_0==ID||LA25_0==INT) ) {
				alt25=1;
			}
			else if ( (LA25_0==LPAR) ) {
				alt25=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 25, 0, input);
				throw nvae;
			}

			switch (alt25) {
				case 1 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:241:5: b= basic
					{
					pushFollow(FOLLOW_basic_in_type983);
					b=basic();
					state._fsp--;

					 ast = b; 
					}
					break;
				case 2 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:242:5: a= arrow
					{
					pushFollow(FOLLOW_arrow_in_type994);
					a=arrow();
					state._fsp--;

					 ast = a; 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "type"



	// $ANTLR start "basic"
	// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:245:1: basic returns [Node ast] : ( INT | BOOL | ID );
	public final Node basic() throws RecognitionException {
		Node ast = null;


		try {
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:246:3: ( INT | BOOL | ID )
			int alt26=3;
			switch ( input.LA(1) ) {
			case INT:
				{
				alt26=1;
				}
				break;
			case BOOL:
				{
				alt26=2;
				}
				break;
			case ID:
				{
				alt26=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}
			switch (alt26) {
				case 1 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:246:5: INT
					{
					match(input,INT,FOLLOW_INT_in_basic1013); 
					 ast = new IntTypeNode(); 
					}
					break;
				case 2 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:247:5: BOOL
					{
					match(input,BOOL,FOLLOW_BOOL_in_basic1022); 
					 ast = new BoolTypeNode(); 
					}
					break;
				case 3 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:248:5: ID
					{
					match(input,ID,FOLLOW_ID_in_basic1030); 
					 System.out.println("TODO Identificativo classi"); 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "basic"



	// $ANTLR start "arrow"
	// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:251:1: arrow returns [Node ast] : LPAR (fty= type ( COMMA ty= type )* )? RPAR ARROW b= basic ;
	public final Node arrow() throws RecognitionException {
		Node ast = null;


		Node fty =null;
		Node ty =null;
		Node b =null;

		try {
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:252:3: ( LPAR (fty= type ( COMMA ty= type )* )? RPAR ARROW b= basic )
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:252:5: LPAR (fty= type ( COMMA ty= type )* )? RPAR ARROW b= basic
			{
			match(input,LPAR,FOLLOW_LPAR_in_arrow1051); 

			        // Inizializzo l'array di parametri
			        ArrayList<Node> parTypes = new ArrayList<Node>();
			      
			// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:257:5: (fty= type ( COMMA ty= type )* )?
			int alt28=2;
			int LA28_0 = input.LA(1);
			if ( (LA28_0==BOOL||LA28_0==ID||LA28_0==INT||LA28_0==LPAR) ) {
				alt28=1;
			}
			switch (alt28) {
				case 1 :
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:257:6: fty= type ( COMMA ty= type )*
					{
					pushFollow(FOLLOW_type_in_arrow1069);
					fty=type();
					state._fsp--;


					        // Aggiungo il primo parametro all'array
					        parTypes.add(fty);
					      
					// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:262:5: ( COMMA ty= type )*
					loop27:
					while (true) {
						int alt27=2;
						int LA27_0 = input.LA(1);
						if ( (LA27_0==COMMA) ) {
							alt27=1;
						}

						switch (alt27) {
						case 1 :
							// /home/osboxes/workspace/ProgettoLCMC/FOOL.g:262:6: COMMA ty= type
							{
							match(input,COMMA,FOLLOW_COMMA_in_arrow1084); 
							pushFollow(FOLLOW_type_in_arrow1088);
							ty=type();
							state._fsp--;


							        // Aggiungo il tutti gli altri parametri all'array
								      parTypes.add(ty);
								    
							}
							break;

						default :
							break loop27;
						}
					}

					}
					break;

			}

			match(input,RPAR,FOLLOW_RPAR_in_arrow1106); 
			match(input,ARROW,FOLLOW_ARROW_in_arrow1108); 
			pushFollow(FOLLOW_basic_in_arrow1112);
			b=basic();
			state._fsp--;


			        // Creo il nodo arrow con il valore di ritorno e i parametri
			        // Lo registro nella variabile ast che verrà ritornata
			        ast = new ArrowTypeNode(parTypes, b);
			      
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "arrow"

	// Delegated rules



	public static final BitSet FOLLOW_exp_in_prog44 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog46 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LET_in_prog53 = new BitSet(new long[]{0x0000080000200100L});
	public static final BitSet FOLLOW_cllist_in_prog69 = new BitSet(new long[]{0x0000080000200000L});
	public static final BitSet FOLLOW_declist_in_prog73 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_IN_in_prog75 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_prog79 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog81 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CLASS_in_cllist106 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist108 = new BitSet(new long[]{0x0000000040080000L});
	public static final BitSet FOLLOW_EXTENDS_in_cllist111 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist113 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_cllist117 = new BitSet(new long[]{0x0000004000800000L});
	public static final BitSet FOLLOW_ID_in_cllist120 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist122 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist124 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_cllist127 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist129 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist131 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist133 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_cllist140 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_cllist152 = new BitSet(new long[]{0x0000000000202000L});
	public static final BitSet FOLLOW_FUN_in_cllist163 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist165 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist167 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist169 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_cllist171 = new BitSet(new long[]{0x0000004000800000L});
	public static final BitSet FOLLOW_ID_in_cllist174 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist176 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_cllist178 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_cllist181 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist183 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist185 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_cllist187 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_cllist194 = new BitSet(new long[]{0x0000042769900000L});
	public static final BitSet FOLLOW_LET_in_cllist209 = new BitSet(new long[]{0x0000080002000000L});
	public static final BitSet FOLLOW_VAR_in_cllist212 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist214 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist216 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist218 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASS_in_cllist220 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_cllist222 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_cllist224 = new BitSet(new long[]{0x0000080002000000L});
	public static final BitSet FOLLOW_IN_in_cllist228 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_cllist232 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_cllist245 = new BitSet(new long[]{0x0000000000202000L});
	public static final BitSet FOLLOW_CRPAR_in_cllist280 = new BitSet(new long[]{0x0000000000000102L});
	public static final BitSet FOLLOW_VAR_in_declist322 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist326 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist328 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_declist332 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASS_in_declist334 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_declist338 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_FUN_in_declist356 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist360 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist362 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_declist366 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_declist381 = new BitSet(new long[]{0x0000004000800000L});
	public static final BitSet FOLLOW_ID_in_declist400 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist402 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_declist406 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_declist421 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist425 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist427 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_declist431 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_declist462 = new BitSet(new long[]{0x0000042769900000L});
	public static final BitSet FOLLOW_LET_in_declist472 = new BitSet(new long[]{0x0000080000200000L});
	public static final BitSet FOLLOW_declist_in_declist476 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_IN_in_declist478 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_declist484 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_declist500 = new BitSet(new long[]{0x0000080000200002L});
	public static final BitSet FOLLOW_term_in_exp535 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_PLUS_in_exp544 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_term_in_exp548 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_MINUS_in_exp560 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_term_in_exp564 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_OR_in_exp575 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_term_in_exp579 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_factor_in_term606 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_TIMES_in_term614 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_factor_in_term618 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_DIV_in_term629 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_factor_in_term633 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_AND_in_term644 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_factor_in_term648 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_value_in_factor673 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_EQ_in_factor681 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_value_in_factor685 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_GE_in_factor696 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_value_in_factor700 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_LE_in_factor711 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_value_in_factor715 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_INTEGER_in_value742 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_value757 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_value765 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NULL_in_value773 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_in_value781 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_value783 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value785 = new BitSet(new long[]{0x0000046749900000L});
	public static final BitSet FOLLOW_exp_in_value788 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_value791 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value793 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_value800 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_value806 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value810 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_THEN_in_value812 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value814 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value818 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value820 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ELSE_in_value822 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value824 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value828 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value830 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_in_value842 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value844 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value848 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value850 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_value858 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value860 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value864 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value866 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_value874 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value878 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value880 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_value890 = new BitSet(new long[]{0x0000000040000002L});
	public static final BitSet FOLLOW_LPAR_in_value904 = new BitSet(new long[]{0x0000046749900000L});
	public static final BitSet FOLLOW_exp_in_value917 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_value930 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value934 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_value955 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_basic_in_type983 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arrow_in_type994 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INT_in_basic1013 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BOOL_in_basic1022 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_basic1030 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_arrow1051 = new BitSet(new long[]{0x0000004044800080L});
	public static final BitSet FOLLOW_type_in_arrow1069 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_arrow1084 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_arrow1088 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_arrow1106 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ARROW_in_arrow1108 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_arrow1112 = new BitSet(new long[]{0x0000000000000002L});
}
