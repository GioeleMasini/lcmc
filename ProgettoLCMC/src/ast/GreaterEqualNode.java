package ast;

import lib.FOOLlib;

public class GreaterEqualNode implements Node {

	private Node left;
	private Node right;

	public GreaterEqualNode(Node left, Node right) {
		super();
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "GreaterEqual\n"
				+ left.toPrint(indent + "  ")
				+ right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		Node l = left.typeCheck();
		Node r = right.typeCheck();
		IntTypeNode intType = new IntTypeNode();
		if (!FOOLlib.isSubtype(l, intType) || !FOOLlib.isSubtype(r, intType)) {
			System.out.println("Non integers in greater-equal.");
			System.exit(0);
		}
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel();
		String l2 = FOOLlib.freshLabel();
		return right.codeGeneration()
				+ left.codeGeneration()
				+ "bleq " + l1 + "\n"
				+ "push 0\n"
				+ "b " + l2 + "\n"
				+ l1 + ":\n"
				+ "push 1\n"
				+ l2 + ":\n";
	}

}
