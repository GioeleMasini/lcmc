package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class CallNode implements Node {

	private String id;
	private STentry entry;
	private ArrayList<Node> parlist;
	private int nestinglevel;

	public CallNode(String i, STentry e, ArrayList<Node> p, int nl) {
		id = i;
		entry = e;
		parlist = p;
		nestinglevel = nl;
	}

	public String toPrint(String s) { //
		String parlstr = "";
		for (Node par : parlist)
			parlstr += par.toPrint(s + "  ");
		return s + "Call:" + id + " at nestlev " + nestinglevel + "\n"
				+ entry.toPrint(s + "  ")
				+ parlstr;
	}

	public Node typeCheck() { //
		ArrowTypeNode t = null;
		if (entry.getType() instanceof ArrowTypeNode)
			t = (ArrowTypeNode) entry.getType();
		else {
			System.out.println("Invocation of a non-function " + id);
			System.exit(0);
		}
		ArrayList<Node> p = t.getParList();
		if (!(p.size() == parlist.size())) {
			System.out
					.println("Wrong number of parameters in the invocation of "
							+ id);
			System.exit(0);
		}
		for (int i = 0; i < parlist.size(); i++)
			if (!(FOOLlib.isSubtype((parlist.get(i)).typeCheck(), p.get(i)))) {
				System.out.println("Wrong type for " + (i + 1)
						+ "-th parameter in the invocation of " + id);
				System.exit(0);
			}
		return t.getRet();
	}

	public String codeGeneration() {
		String parCode = "";
		for (int i = parlist.size() - 1; i >= 0; i--)
			parCode += parlist.get(i).codeGeneration();

		String getAR = "";
		for (int i = 0; i < nestinglevel - entry.getNestinglevel(); i++)
			getAR += "lw\n";

		return "lfp\n" 		// push $fp. Iniziamo ad inserire nello stack l'activation record con il Control Link
				+ parCode	// Inseriamo il codice di tutti i parametri in ordine contrario

				// Costruisco l'access link
				+ "lfp\n"	// push $fp.
				+ getAR 	// setto AL risalendo la catena statica
				// ora recupero l'indirizzo a cui saltare e lo metto sullo stack
				+ "push " + entry.getOffset() + "\n" // metto offset sullo stack
				+ "add\n"
				+ "lw\n"

				//Recupera indir funzione (Per saltare al codice della funzione)
				+ "lfp\n"
				+ getAR // risalgo la catena statica
				+ "push " + (entry.getOffset() - 1) + "\n"
				+ "add\n"
				+ "lw\n" // carico sullo stack il valore all'indirizzo ottenuto

				+ "js\n";	//Salto e mette il Return Address nel registro RA
	}

}