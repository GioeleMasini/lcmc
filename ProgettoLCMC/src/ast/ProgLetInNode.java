package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class ProgLetInNode implements Node {

	private ArrayList<Node> declist;
	private Node exp;

	public ProgLetInNode(ArrayList<Node> d, Node e) {
		declist = d;
		exp = e;
	}

	public String toPrint(String s) {
		String declstr = "";
		for (Node dec : declist)
			declstr += dec.toPrint(s + "  ");
		return s + "ProgLetIn\n" + declstr + exp.toPrint(s + "  ");
	}

	public Node typeCheck() {
		for (Node dec : declist)
			dec.typeCheck();
		return exp.typeCheck();
	}

	public String codeGeneration() {
		String declCode = "";
		for (Node dec : declist)
			declCode += dec.codeGeneration();

		return "push 0\n"				// Push sullo stack dell'indirizzo del frame pointer
				+ declCode				// Inserimento delle dichiarazioni di "let"
				+ exp.codeGeneration()	// Inserimento dell'espressione di "in"
				+ "halt\n"				// Uscita dal programma
				+ FOOLlib.getCode();	// Inserimento del codice delle funzioni in fondo al file, al termine del "main"
	}

}