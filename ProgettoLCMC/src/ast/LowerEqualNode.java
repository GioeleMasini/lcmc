package ast;

import lib.FOOLlib;

public class LowerEqualNode implements Node {

	private Node left;
	private Node right;

	public LowerEqualNode(Node left, Node right) {
		super();
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "LowerEqual\n" + left.toPrint(indent + "  ")
				+ right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		Node l = left.typeCheck();
		Node r = right.typeCheck();
		IntTypeNode intType = new IntTypeNode();
		if (!FOOLlib.isSubtype(l, intType) || !FOOLlib.isSubtype(r, intType)) {
			System.out.println("Non integers in lower-equal.");
			System.exit(0);
		}
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel();
		String l2 = FOOLlib.freshLabel();
		return left.codeGeneration() + right.codeGeneration() + "bleq " + l1
				+ "\n" + "push 0\n" + "b " + l2 + "\n" + l1 + ":\n"
				+ "push 1\n" + l2 + ":\n";
	}

}
