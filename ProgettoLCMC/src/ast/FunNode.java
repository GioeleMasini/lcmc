package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class FunNode implements DecNode {

	// Nome della funzione
	private String id;
	// Type di ritorno della funzione
	private Node type;
	// ArrowTypeNode, type della funzione,
	// es.: (int)->int
	// es. completo: var myFun:(int)->int = add;
	private Node symType;
	private ArrayList<DecNode> parlist = new ArrayList<DecNode>();
	private ArrayList<DecNode> declist;
	private Node body;

	public FunNode(String i, Node t) {
		id = i;
		type = t;
	}

	public void addDecBody(ArrayList<DecNode> d, Node b) {
		declist = d;
		body = b;
	}

	public void addPar(DecNode p) {
		parlist.add(p);
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (DecNode par : parlist) {
			parlstr += par.toPrint(s + "  ");
		}
		String declstr = "";
		if (declist != null) {
			for (DecNode dec : declist) {
				declstr += dec.toPrint(s + "  ");
			}
		}
		return s + "Fun:" + id + "\n" + type.toPrint(s + "  ") + parlstr
				+ declstr + body.toPrint(s + "  ");
	}

	// valore di ritorno non utilizzato
	public Node typeCheck() {
		if (declist != null) {
			for (Node dec : declist) {
				dec.typeCheck();
			}
		}
		if (!(FOOLlib.isSubtype(body.typeCheck(), type))) {
			System.out.println("Wrong return type for function " + id);
			System.exit(0);
		}
		return null;
	}

	public String codeGeneration() {

		String declCode = "";
		if (declist != null) {
			for (Node dec : declist) {
				declCode += dec.codeGeneration();
			}
		}

		String popDecl = "";
		if (declist != null) {
			for (DecNode dec : declist) {
				popDecl += "pop\n";
				if (dec.getSymType() instanceof ArrowTypeNode) {
					popDecl += "pop\n";
				}
			}
		}

		String popParl = "";
		for (DecNode par: parlist) {
			popParl += "pop\n";
			if (par.getSymType() instanceof ArrowTypeNode) {
				popParl += "pop\n";
			}
		}

		String funl = FOOLlib.freshFunLabel();
		FOOLlib.putCode(
			funl + ":\n"
			+ "cfp\n" // setta $fp a $sp ($sp = stack pointer = indirizzo primo elemento dello stack). L'activation record inizia da qui.
				+ "lra\n" // inserimento return address
					+ declCode // inserimento dichiarazioni locali
						+ body.codeGeneration()
						+ "srv\n" // pop del return value
					+ popDecl
				+ "sra\n" // pop del return address
			+ "pop\n" // pop di AL

			+ popParl
			+ "sfp\n" // setto $fp a valore del CL
			+ "lrv\n" // risultato della funzione sullo stack
			+ "lra\n"
			+ "js\n" // salta a $ra
		);

		return "lfp\n"					// Push sullo stack del frame pointer
			+ "push " + funl + "\n";	// Push sullo stack dell'indirizzo del codice (label)
	}

	public void setSymType(ArrowTypeNode symType) {
		this.symType = symType;
	}

	@Override
	public Node getSymType() {
		return this.symType;
	}

}