package ast;

public class IdNode implements Node {

	private String id;
	private STentry entry;
	private int nestinglevel;

	public IdNode(String i, STentry st, int nl) {
		id = i;
		entry = st;
		nestinglevel = nl;
	}

	public String toPrint(String s) {
		return s + "Id:" + id + " at nestlev " + nestinglevel + "\n"
				+ entry.toPrint(s + "  ");
	}

	public Node typeCheck() {
		return entry.getType();
	}

	public String codeGeneration() {
		String getAR = "";

		for (int i = 0; i < nestinglevel - entry.getNestinglevel(); i++) {
			getAR += "lw\n";
		}

		String code = "push " + entry.getOffset() + "\n" // metto offset sullo stack
				+ "lfp\n"
				+ getAR
				+ "add\n" // risalgo la catena statica
				+ "lw\n"; // carico sullo stack il valore all'indirizzo ottenuto

		if (entry.getType() instanceof ArrowTypeNode) {
			code += "push " + (entry.getOffset() - 1) + "\n"
					+ "lfp\n"
					+ getAR
					+ "add\n"
					+ "lw\n";
		}

		return code;
	}
}