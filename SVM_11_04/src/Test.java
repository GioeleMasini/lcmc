import org.antlr.runtime.*;

public class Test {
    public static void main(String[] args) throws Exception {
     
        String fileName = "prova.asm";
        
        ANTLRFileStream input = new ANTLRFileStream(fileName);
        SVMLexer lexer = new SVMLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SVMParser parser = new SVMParser(tokens);
        
        parser.assembly();
        
        System.out.println("You had: "+lexer.lexicalErrors+" lexical errors and "+parser.getNumberOfSyntaxErrors()+" syntax errors.");
        if (lexer.lexicalErrors>0 || parser.getNumberOfSyntaxErrors()>0) System.exit(1);

        System.out.println("Starting Virtual Machine...");
        ExecuteVM vm = new ExecuteVM(parser.code);
        vm.cpu();
        System.out.println("Executed with success!");
    }
}
