// $ANTLR 3.5.2 /home/osboxes/workspace/SVM_11_04/src/SVM.g 2017-04-11 22:53:30

  import java.util.HashMap;
  import java.util.ArrayList;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class SVMParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ADD", "BRANCH", "BRANCHEQ", "BRANCHLESSEQ", 
		"COL", "COPYFP", "DIV", "ERR", "HALT", "JS", "LABEL", "LOADFP", "LOADHP", 
		"LOADRA", "LOADRV", "LOADW", "MULT", "NUMBER", "POP", "PRINT", "PUSH", 
		"STOREFP", "STOREHP", "STORERA", "STORERV", "STOREW", "SUB", "WHITESP"
	};
	public static final int EOF=-1;
	public static final int ADD=4;
	public static final int BRANCH=5;
	public static final int BRANCHEQ=6;
	public static final int BRANCHLESSEQ=7;
	public static final int COL=8;
	public static final int COPYFP=9;
	public static final int DIV=10;
	public static final int ERR=11;
	public static final int HALT=12;
	public static final int JS=13;
	public static final int LABEL=14;
	public static final int LOADFP=15;
	public static final int LOADHP=16;
	public static final int LOADRA=17;
	public static final int LOADRV=18;
	public static final int LOADW=19;
	public static final int MULT=20;
	public static final int NUMBER=21;
	public static final int POP=22;
	public static final int PRINT=23;
	public static final int PUSH=24;
	public static final int STOREFP=25;
	public static final int STOREHP=26;
	public static final int STORERA=27;
	public static final int STORERV=28;
	public static final int STOREW=29;
	public static final int SUB=30;
	public static final int WHITESP=31;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public SVMParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public SVMParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return SVMParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/osboxes/workspace/SVM_11_04/src/SVM.g"; }


		public int[] code = new int[ExecuteVM.CODESIZE];
		private int i = 0;
		// Mappa di <Label, Numero riga>, ogni label è un riferimento ad una certa riga
	  private HashMap<String, Integer> labelToAddress = new HashMap<String, Integer>();
	  // Mappa di <Numero riga, Label>, salvo la posizione di ogni label nel codice
	  // A fine parsing sostituirò tutte le label con il numero di riga a cui fanno riferimento
	  private HashMap<Integer, String> labelsInCode = new HashMap<Integer, String>();



	// $ANTLR start "assembly"
	// /home/osboxes/workspace/SVM_11_04/src/SVM.g:27:1: assembly : ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )* ;
	public final void assembly() throws RecognitionException {
		Token n=null;
		Token l=null;

		try {
			// /home/osboxes/workspace/SVM_11_04/src/SVM.g:27:9: ( ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )* )
			// /home/osboxes/workspace/SVM_11_04/src/SVM.g:29:5: ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )*
			{
			// /home/osboxes/workspace/SVM_11_04/src/SVM.g:29:5: ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )*
			loop1:
			while (true) {
				int alt1=26;
				switch ( input.LA(1) ) {
				case PUSH:
					{
					int LA1_2 = input.LA(2);
					if ( (LA1_2==NUMBER) ) {
						alt1=1;
					}
					else if ( (LA1_2==LABEL) ) {
						alt1=2;
					}

					}
					break;
				case POP:
					{
					alt1=3;
					}
					break;
				case ADD:
					{
					alt1=4;
					}
					break;
				case SUB:
					{
					alt1=5;
					}
					break;
				case MULT:
					{
					alt1=6;
					}
					break;
				case DIV:
					{
					alt1=7;
					}
					break;
				case STOREW:
					{
					alt1=8;
					}
					break;
				case LOADW:
					{
					alt1=9;
					}
					break;
				case LABEL:
					{
					alt1=10;
					}
					break;
				case BRANCH:
					{
					alt1=11;
					}
					break;
				case BRANCHEQ:
					{
					alt1=12;
					}
					break;
				case BRANCHLESSEQ:
					{
					alt1=13;
					}
					break;
				case JS:
					{
					alt1=14;
					}
					break;
				case LOADRA:
					{
					alt1=15;
					}
					break;
				case STORERA:
					{
					alt1=16;
					}
					break;
				case LOADRV:
					{
					alt1=17;
					}
					break;
				case STORERV:
					{
					alt1=18;
					}
					break;
				case LOADFP:
					{
					alt1=19;
					}
					break;
				case STOREFP:
					{
					alt1=20;
					}
					break;
				case COPYFP:
					{
					alt1=21;
					}
					break;
				case LOADHP:
					{
					alt1=22;
					}
					break;
				case STOREHP:
					{
					alt1=23;
					}
					break;
				case PRINT:
					{
					alt1=24;
					}
					break;
				case HALT:
					{
					alt1=25;
					}
					break;
				}
				switch (alt1) {
				case 1 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:29:7: PUSH n= NUMBER
					{
					match(input,PUSH,FOLLOW_PUSH_in_assembly48); 
					n=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_assembly52); 
					 
					        code[i++] = PUSH; 
					        code[i++] = Integer.parseInt((n!=null?n.getText():null));
					      
					}
					break;
				case 2 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:34:7: PUSH l= LABEL
					{
					match(input,PUSH,FOLLOW_PUSH_in_assembly69); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly73); 

					        code[i++] = PUSH;
					        labelsInCode.put(i++, (l!=null?l.getText():null));
					      
					}
					break;
				case 3 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:39:7: POP
					{
					match(input,POP,FOLLOW_POP_in_assembly92); 

					        code[i++] = POP;
					      
					}
					break;
				case 4 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:43:7: ADD
					{
					match(input,ADD,FOLLOW_ADD_in_assembly111); 

					        code[i++] = ADD;
					      
					}
					break;
				case 5 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:47:7: SUB
					{
					match(input,SUB,FOLLOW_SUB_in_assembly130); 

					        code[i++] = SUB;
					      
					}
					break;
				case 6 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:51:7: MULT
					{
					match(input,MULT,FOLLOW_MULT_in_assembly149); 

					        code[i++] = MULT;
					      
					}
					break;
				case 7 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:55:7: DIV
					{
					match(input,DIV,FOLLOW_DIV_in_assembly167); 

					        code[i++] = DIV;
					      
					}
					break;
				case 8 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:60:7: STOREW
					{
					match(input,STOREW,FOLLOW_STOREW_in_assembly193); 

					        code[i++] = STOREW;
					      
					}
					break;
				case 9 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:65:7: LOADW
					{
					match(input,LOADW,FOLLOW_LOADW_in_assembly218); 

					        code[i++] = LOADW;
					      
					}
					break;
				case 10 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:69:7: l= LABEL COL
					{
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly243); 
					match(input,COL,FOLLOW_COL_in_assembly245); 

					        labelToAddress.put((l!=null?l.getText():null), i);
					      
					}
					break;
				case 11 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:73:7: BRANCH l= LABEL
					{
					match(input,BRANCH,FOLLOW_BRANCH_in_assembly262); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly266); 

					        code[i++] = BRANCH;
					        labelsInCode.put(i++, (l!=null?l.getText():null));
					      
					}
					break;
				case 12 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:78:7: BRANCHEQ l= LABEL
					{
					match(input,BRANCHEQ,FOLLOW_BRANCHEQ_in_assembly283); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly287); 

					        code[i++] = BRANCHEQ;
					        labelsInCode.put(i++, (l!=null?l.getText():null));
					      
					}
					break;
				case 13 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:83:7: BRANCHLESSEQ l= LABEL
					{
					match(input,BRANCHLESSEQ,FOLLOW_BRANCHLESSEQ_in_assembly308); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly312); 

					        code[i++] = BRANCHLESSEQ;
					        labelsInCode.put(i++, (l!=null?l.getText():null));
					      
					}
					break;
				case 14 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:89:7: JS
					{
					match(input,JS,FOLLOW_JS_in_assembly338); 

					        code[i++] = JS;
					      
					}
					break;
				case 15 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:95:7: LOADRA
					{
					match(input,LOADRA,FOLLOW_LOADRA_in_assembly376); 

					        code[i++] = LOADRA;
					      
					}
					break;
				case 16 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:99:7: STORERA
					{
					match(input,STORERA,FOLLOW_STORERA_in_assembly404); 

					        code[i++] = STORERA;
					      
					}
					break;
				case 17 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:103:7: LOADRV
					{
					match(input,LOADRV,FOLLOW_LOADRV_in_assembly431); 

					        code[i++] = LOADRV;
					      
					}
					break;
				case 18 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:107:7: STORERV
					{
					match(input,STORERV,FOLLOW_STORERV_in_assembly448); 

					        code[i++] = STORERV;
					      
					}
					break;
				case 19 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:111:7: LOADFP
					{
					match(input,LOADFP,FOLLOW_LOADFP_in_assembly465); 

					        code[i++] = LOADFP;
					      
					}
					break;
				case 20 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:115:7: STOREFP
					{
					match(input,STOREFP,FOLLOW_STOREFP_in_assembly482); 

					        code[i++] = STOREFP;
					      
					}
					break;
				case 21 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:119:7: COPYFP
					{
					match(input,COPYFP,FOLLOW_COPYFP_in_assembly499); 

					        code[i++] = COPYFP;
					      
					}
					break;
				case 22 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:123:7: LOADHP
					{
					match(input,LOADHP,FOLLOW_LOADHP_in_assembly516); 

					        code[i++] = LOADHP;
					      
					}
					break;
				case 23 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:127:7: STOREHP
					{
					match(input,STOREHP,FOLLOW_STOREHP_in_assembly533); 

					        code[i++] = STOREHP;
					      
					}
					break;
				case 24 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:131:7: PRINT
					{
					match(input,PRINT,FOLLOW_PRINT_in_assembly560); 

					        code[i++] = PRINT;
					      
					}
					break;
				case 25 :
					// /home/osboxes/workspace/SVM_11_04/src/SVM.g:135:7: HALT
					{
					match(input,HALT,FOLLOW_HALT_in_assembly589); 

					        code[i++] = HALT;
					      
					}
					break;

				default :
					break loop1;
				}
			}


			      for (Integer key: labelsInCode.keySet()) {
			        String label = labelsInCode.get(key);
			        code[key] = labelToAddress.get(label);
			      }
			    
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "assembly"

	// Delegated rules



	public static final BitSet FOLLOW_PUSH_in_assembly48 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_NUMBER_in_assembly52 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_PUSH_in_assembly69 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_LABEL_in_assembly73 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_POP_in_assembly92 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_ADD_in_assembly111 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_SUB_in_assembly130 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_MULT_in_assembly149 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_DIV_in_assembly167 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_STOREW_in_assembly193 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LOADW_in_assembly218 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LABEL_in_assembly243 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_COL_in_assembly245 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_BRANCH_in_assembly262 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_LABEL_in_assembly266 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_BRANCHEQ_in_assembly283 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_LABEL_in_assembly287 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_BRANCHLESSEQ_in_assembly308 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_LABEL_in_assembly312 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_JS_in_assembly338 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LOADRA_in_assembly376 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_STORERA_in_assembly404 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LOADRV_in_assembly431 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_STORERV_in_assembly448 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LOADFP_in_assembly465 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_STOREFP_in_assembly482 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_COPYFP_in_assembly499 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LOADHP_in_assembly516 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_STOREHP_in_assembly533 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_PRINT_in_assembly560 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_HALT_in_assembly589 = new BitSet(new long[]{0x000000007FDFF6F2L});
}
