grammar SVM;

@header {
  import java.util.HashMap;
  import java.util.ArrayList;
}

@lexer::members {
  int lexicalErrors=0;
}

@members {
	public int[] code = new int[ExecuteVM.CODESIZE];
	private int i = 0;
	// Mappa di <Label, Numero riga>, ogni label è un riferimento ad una certa riga
  private HashMap<String, Integer> labelToAddress = new HashMap<String, Integer>();
  // Mappa di <Numero riga, Label>, salvo la posizione di ogni label nel codice
  // A fine parsing sostituirò tutte le label con il numero di riga a cui fanno riferimento
  private HashMap<Integer, String> labelsInCode = new HashMap<Integer, String>();
}


/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/

assembly: 
      //push NUMBER on the stack
    ( PUSH n=NUMBER { 
        code[i++] = PUSH; 
        code[i++] = Integer.parseInt($n.text);
      }
      //push the location address pointed by LABEL on the stack      
    | PUSH l=LABEL {
        code[i++] = PUSH;
        labelsInCode.put(i++, $l.text);
      }  
      //pop the top of the stack 
    | POP {
        code[i++] = POP;
      }  
      //replace the two values on top of the stack with their sum
    | ADD {
        code[i++] = ADD;
      }  
      //pop the two values v1 and v2 (respectively) and push v2-v1
    | SUB {
        code[i++] = SUB;
      }  
      //replace the two values on top of the stack with their product 
    | MULT {
        code[i++] = MULT;
      } 
      //pop the two values v1 and v2 (respectively) and push v2/v1
    | DIV {
        code[i++] = DIV;
      }  
      //pop two values: 
      //the second one is written at the memory address pointed by the first one
    | STOREW {
        code[i++] = STOREW;
      } 
      //read the content of the memory cell pointed by the top of the stack
      //and replace the top of the stack with such value
    | LOADW {
        code[i++] = LOADW;
      }      
      //LABEL points at the location of the subsequent instruction
    | l=LABEL COL {
        labelToAddress.put($l.text, i);
      }
      //jump at the instruction pointed by LABEL   
    | BRANCH l=LABEL {
        code[i++] = BRANCH;
        labelsInCode.put(i++, $l.text);
      }
      //pop two values and jump if they are equal
    | BRANCHEQ l=LABEL {
        code[i++] = BRANCHEQ;
        labelsInCode.put(i++, $l.text);
      }    
      //pop two values and jump if the second one is less or equal to the first one
    | BRANCHLESSEQ l=LABEL {
        code[i++] = BRANCHLESSEQ;
        labelsInCode.put(i++, $l.text);
      }  
      //pop one value from the stack:
      //copy the instruction pointer in the RA register and jump to the popped value    
    | JS {
        code[i++] = JS;
      }                   


      //push in the stack the content of the RA register   
    | LOADRA {
        code[i++] = LOADRA;
      }           
      //pop the top of the stack and copy it in the RA register     
    | STORERA {
        code[i++] = STORERA;
      }          
      //push in the stack the content of the RV register    
    | LOADRV {
        code[i++] = LOADRV;
      }
      //pop the top of the stack and copy it in the RV register    
    | STORERV {
        code[i++] = STORERV;
      }
      //push in the stack the content of the FP register   
    | LOADFP {
        code[i++] = LOADFP;
      }
      //pop the top of the stack and copy it in the FP register    
    | STOREFP {
        code[i++] = STOREFP;
      }
      //copy in the FP register the currest stack pointer    
    | COPYFP {
        code[i++] = COPYFP;
      }
      //push in the stack the content of the HP register    
    | LOADHP {
        code[i++] = LOADHP;
      }
      //pop the top of the stack and copy it in the HP register    
    | STOREHP {
        code[i++] = STOREHP;
      }          
      //visualize the top of the stack without removing it   
    | PRINT {
        code[i++] = PRINT;
      }            
      //terminate the execution  
    | HALT {
        code[i++] = HALT;
      }
    )* {
      for (Integer key: labelsInCode.keySet()) {
        String label = labelsInCode.get(key);
        code[key] = labelToAddress.get(label);
      }
    }
 ;
   
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

PUSH   : 'push' ;   
POP  : 'pop' ;  
ADD  : 'add' ;    
SUB  : 'sub' ;  
MULT   : 'mult' ;   
DIV  : 'div' ;  
STOREW   : 'sw' ;   
LOADW  : 'lw' ; 
BRANCH   : 'b' ;  
BRANCHEQ : 'beq' ;  
BRANCHLESSEQ:'bleq' ; 
JS   : 'js' ; 
LOADRA   : 'lra' ;  
STORERA  : 'sra' ;   
LOADRV   : 'lrv' ;  
STORERV  : 'srv' ;  
LOADFP   : 'lfp' ;  
STOREFP  : 'sfp' ;  
COPYFP   : 'cfp' ;      
LOADHP   : 'lhp' ;  
STOREHP  : 'shp' ;  
PRINT  : 'print' ;  
HALT   : 'halt' ; 

COL  : ':' ;
LABEL  : ('a'..'z'|'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9')* ;
NUMBER   : '0' | ('-')?(('1'..'9')('0'..'9')*) ;

WHITESP  : ( '\t' | ' ' | '\r' | '\n' )+    { $channel=HIDDEN; } ;

ERR      : . { System.err.println("Invalid char: "+$text); lexicalErrors++; $channel=HIDDEN; } ;