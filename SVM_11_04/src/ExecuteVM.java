public class ExecuteVM {
    
    public static final int CODESIZE = 10000;
    public static final int MEMSIZE = 10000;
    
    private int[] code;
    private int[] memory = new int[MEMSIZE];
    
    private int ip = 0;
    private int sp = MEMSIZE;
    
    private int ra;
    private int rv;
    private int fp = sp;
    private int hp = 0;
        
    public ExecuteVM(int[] code) {
      this.code = code;
    }
    
    public void cpu() {
    	try {
      while ( true ) {
        int bytecode = code[ip++]; // fetch
        int v1,v2;
        int address;
        switch ( bytecode ) {
    	case SVMParser.PUSH:
    		push(code[ip++]);
    		break;
    	case SVMParser.POP:
    		pop();
    		break;
    	case SVMParser.ADD:
    		v1 = pop();
    		v2 = pop();
    		push(v2 + v1);
    		break;
    	case SVMParser.SUB:
    		v1 = pop();
    		v2 = pop();
    		push(v2 - v1);
    		break;
    	case SVMParser.MULT:
    		v1 = pop();
    		v2 = pop();
    		push(v2 * v1);
    		break;
    	case SVMParser.DIV:
    		v1 = pop();
    		v2 = pop();
    		push(v2 / v1);
    		break;
    	case SVMParser.STOREW:
    		address = pop();
    		v1 = pop();
    		memory[address] = v1; 
    		break;
    	case SVMParser.LOADW:
            address = pop();
            push(memory[address]);
    		break;
    	case SVMParser.BRANCH:
    		address = code[ip];
    		ip = address;
    		break;
    	case SVMParser.BRANCHEQ:
    		v1 = pop();
    		v2 = pop();
    		address = code[ip++];
    		if (v1 == v2) {
    			ip = address;
    		}
    		break;
    	case SVMParser.BRANCHLESSEQ:
    		v1 = pop();
    		v2 = pop();
    		address = code[ip++];
    		if (v2 <= v1) {
    			ip = address;
    		}
    		break;
    	case SVMParser.JS:
    		address = pop();
    		ra = ip;
    		ip = address;
    		break;
    	case SVMParser.LOADRA:
    		push(ra);
    		break;
    	case SVMParser.STORERA:
    		ra = pop();
    		break;
    	case SVMParser.LOADRV:
    		push(rv);
    		break;
    	case SVMParser.STORERV:
    		rv = pop();
    		break;
    	case SVMParser.LOADFP:
    		push(fp);
    		break;
    	case SVMParser.STOREFP:
    		fp = pop();
    		break;
    	case SVMParser.COPYFP:
    		fp = sp;
    		break;
    	case SVMParser.LOADHP:
    		push(hp);
    		break;
    	case SVMParser.STOREHP:
    		hp = pop();
    		break;
    	case SVMParser.PRINT:
    		System.out.println(sp < MEMSIZE ? memory[sp] : "Empty stack!");
    		break;
    	case SVMParser.HALT:
    		return;
        }
      }
    	} catch (Exception e) {
    		System.out.println("Riga: " + ip);
    		e.printStackTrace();
    	}
    } 
    
    private int pop() {
      return memory[sp++];
    }
    
    private void push(int v) {
      memory[--sp] = v;
    }
    
}