package ast;

import lib.FOOLlib;

public class MultNode implements Node {

  private Node left;
  private Node right;
  
  public MultNode (Node l, Node r) {
   left=l;
   right=r;
  }
  
  public String toPrint(String s) {
   return s+"Mult\n" + left.toPrint(s+"  ")  
                     + right.toPrint(s+"  ") ; 
  }
  
  public Node typecheck() {
		if (! ( ( FOOLlib.isSubtype(left.typecheck(), new IntTypeNode()) ) &&
		        ( FOOLlib.isSubtype(right.typecheck(), new IntTypeNode()) ) ) 
		   ) 
		  {
			System.out.println("Non integers in multiplication");
			System.exit(0);
		  };
		return new IntTypeNode();
	  }
  
}  