package ast;
import java.util.ArrayList;

import lib.FOOLlib;

public class FunNode implements Node {

  private String id;
  private Node type; 
  private ArrayList<Node> parlist = new ArrayList<Node>(); // campo "parlist" che � lista di Node
  private ArrayList<Node> declist; 
  private Node body;
  
  public FunNode (String i, Node t) {
   id=i;
   type=t;
  }
  
  public void addDecBody (ArrayList<Node> d, Node b) {
   declist=d;
   body=b;
  }  
  
  public void addPar (Node p) { //metodo "addPar" che aggiunge un nodo a campo "parlist"
   parlist.add(p);  
  }  
  
  public String toPrint(String s) {
		 String parlstr="";
		 for (Node par:parlist){parlstr+=par.toPrint(s+"  ");};
		 String declstr="";
		 if (declist!=null) for (Node dec:declist){declstr+=dec.toPrint(s+"  ");};
	   return s+"Fun:" + id +"\n"
			   +type.toPrint(s+"  ")
			   +parlstr
			   +declstr
               +body.toPrint(s+"  ") ; 
  }
  
  public Node typecheck() {
	for (Node dec:declist)
		  dec.typecheck();
	if (!FOOLlib.isSubtype(body.typecheck(),type))
	  {
		System.out.println("Wrong return type for function "+id);
		System.exit(0);
	  }
    return null;  
  }
    
}  