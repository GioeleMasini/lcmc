package ast;
import java.util.ArrayList;

import lib.FOOLlib;

public class CallNode implements Node {

  private String id;
  private STentry entry; 
  private ArrayList<Node> parlist;
  
  public CallNode (String i, STentry e, ArrayList<Node> p) {
   id=i;
   entry=e;
   parlist=p;
  }
  
  public String toPrint(String s) {
		 String parlstr="";
		 for (Node par:parlist){parlstr+=par.toPrint(s+"  ");};
	   return s+"Call:" + id +"\n"
			   +entry.toPrint(s+"  ")
			   +parlstr; 
  }
  
  //esercizio
  public Node typecheck() {  
	  // ESERC: regola (con subtyping) vista a lezione 

	  // "Invocation of a non-function "+id
	  if (!(entry.getType() instanceof ArrowTypeNode)) {
		  System.out.println("Invocation of a non-function " + id);
		  System.exit(1);
	  }
	  // recupero tipo (deve essere ArrowTypeNode) da STentry
	  ArrowTypeNode node = (ArrowTypeNode)entry.getType();

	  // leggere tipo parametri e tipo di ritorno da un ArrowTypeNode tramite 
	  // i suoi metodi getRet() e getParList()
	  ArrayList<Node> declaratedParlist = node.getParList();
	  
	  // "Wrong number of parameters in the invocation of "+id
	  if (declaratedParlist.size() != parlist.size()) {
		  System.out.println("Wrong number of parameters in the invocation of " + id);
		  System.exit(1);
	  }

	  // "Wrong type for "+i+"-th parameter in the invocation of "+id
	  for (int i = 0; i < parlist.size(); i++) {
		  Node declaratedParam = declaratedParlist.get(i);
		  Node callParam = parlist.get(i);
		  if (!FOOLlib.isSubtype(callParam.typecheck(), declaratedParam)) {
			  System.out.println("Wrong type for " + i + "-th parameter in the invocation of " + id);
			  System.exit(1);
		  }
	  }

	// dopo i check ritorno il tipo di ritorno  
    return node.getRet();
  }
  
  
    
}  