package ast;
import java.util.ArrayList;

public class ArrowTypeNode implements Node {

  private ArrayList<Node> parlist; //tipi dei parametri
  private Node ret; //tipo del ritorno
  
  public ArrowTypeNode (ArrayList<Node> p, Node r) {
   parlist=p;
   ret=r;
  }
  
  public String toPrint(String s) {
	 String parlstr="";
	 for (Node par:parlist){parlstr+=par.toPrint(s+"  ");};
     return s+"ArrowTypeNode\n" + parlstr + ret.toPrint(s+"  ->") ; 
  }
  
  public Node getRet() {
	  return this.ret;
  }
  
  public ArrayList<Node> getParList() {
	  return this.parlist;
  }
  
  //mai invocato
  public Node typecheck() {  
		return null;
  }
    
}  