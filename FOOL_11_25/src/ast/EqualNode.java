package ast;

import lib.FOOLlib;

public class EqualNode implements Node {

  private Node left;
  private Node right;
  
  public EqualNode (Node l, Node r) {
   left=l;
   right=r;
  }
  
  public String toPrint(String s) {
   return s+"Equal\n" + left.toPrint(s+"  ")   
                      + right.toPrint(s+"  ") ; 
  }
  
  public Node typecheck() {
	Node l = left.typecheck();
	Node r = right.typecheck();
	if (! ( FOOLlib.isSubtype(l,r) || FOOLlib.isSubtype(r,l) )   
	   ) 
	  {
		System.out.println("Incompatible types in equal");
		System.exit(0);
	  };
    return new BoolTypeNode();
  }
    
}  