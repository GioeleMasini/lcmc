package ast;

import lib.FOOLlib;

public class IfNode implements Node {

  private Node cond;
  private Node th;
  private Node el;
  
  public IfNode (Node c, Node t, Node e) {
   cond=c;
   th=t;
   el=e;
  }
  
  public String toPrint(String s) {
   return s+"If\n" + cond.toPrint(s+"  ") 
                 + th.toPrint(s+"  ")   
                 + el.toPrint(s+"  ") ; 
  }
  
  public Node typecheck() {

	if (!FOOLlib.isSubtype( cond.typecheck(), new BoolTypeNode()))
	  {
		System.out.println("Non boolean condition in if");
		System.exit(0);
	  };
    
	Node t = th.typecheck();
    Node e = el.typecheck();
    if (FOOLlib.isSubtype(t,e))
      return e;
    if (FOOLlib.isSubtype(e,t))
      return t;
    System.out.println("Incompatible types in then else branches");
	System.exit(0);
	return null;
  }  
}  