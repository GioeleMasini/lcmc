package lib;

import ast.*;

public class FOOLlib {
  
  //valuta se il tipo "a" è <= al tipo "b", dove "a" e "b" sono tipi di base: int o bool
  public static boolean isSubtype (Node a, Node b)
  {
	  if (a == null || b == null) {
		  return a == null && b == null;
	  }
	  if (a.getClass().equals(b.getClass())) {
		  return true;
	  }
	  // Da consegna si possono usare i booleani al posto degli interi
	  if (a.getClass().equals(BoolTypeNode.class) && b.getClass().equals(IntTypeNode.class)) {
		  return true;
	  }
	  return false;
  }
	
}
