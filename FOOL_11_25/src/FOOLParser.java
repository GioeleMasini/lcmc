// $ANTLR 3.5.2 /home/osboxes/workspace/FOOL_11_25/FOOL.g 2017-04-11 22:53:19

import java.util.ArrayList;
import java.util.HashMap;
import ast.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ASS", "BOOL", "CLPAR", "COLON", 
		"COMMA", "COMMENT", "CRPAR", "ELSE", "EQ", "ERR", "FALSE", "FUN", "ID", 
		"IF", "IN", "INT", "INTEGER", "LET", "LPAR", "PLUS", "PRINT", "RPAR", 
		"SEMIC", "THEN", "TIMES", "TRUE", "VAR", "WHITESP"
	};
	public static final int EOF=-1;
	public static final int ASS=4;
	public static final int BOOL=5;
	public static final int CLPAR=6;
	public static final int COLON=7;
	public static final int COMMA=8;
	public static final int COMMENT=9;
	public static final int CRPAR=10;
	public static final int ELSE=11;
	public static final int EQ=12;
	public static final int ERR=13;
	public static final int FALSE=14;
	public static final int FUN=15;
	public static final int ID=16;
	public static final int IF=17;
	public static final int IN=18;
	public static final int INT=19;
	public static final int INTEGER=20;
	public static final int LET=21;
	public static final int LPAR=22;
	public static final int PLUS=23;
	public static final int PRINT=24;
	public static final int RPAR=25;
	public static final int SEMIC=26;
	public static final int THEN=27;
	public static final int TIMES=28;
	public static final int TRUE=29;
	public static final int VAR=30;
	public static final int WHITESP=31;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public FOOLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return FOOLParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/osboxes/workspace/FOOL_11_25/FOOL.g"; }


	private int nestingLevel = -1;
	private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();
	//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
	//il "fronte" della lista di tabelle � symTable.get(nestingLevel)



	// $ANTLR start "prog"
	// /home/osboxes/workspace/FOOL_11_25/FOOL.g:24:1: prog returns [Node ast] : (e= exp SEMIC | LET d= declist IN e= exp SEMIC );
	public final Node prog() throws RecognitionException {
		Node ast = null;


		Node e =null;
		ArrayList<Node> d =null;

		try {
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:25:2: (e= exp SEMIC | LET d= declist IN e= exp SEMIC )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==FALSE||(LA1_0 >= ID && LA1_0 <= IF)||LA1_0==INTEGER||LA1_0==LPAR||LA1_0==PRINT||LA1_0==TRUE) ) {
				alt1=1;
			}
			else if ( (LA1_0==LET) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:25:4: e= exp SEMIC
					{
					pushFollow(FOLLOW_exp_in_prog39);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog41); 
					ast = new ProgNode(e);
					}
					break;
				case 2 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:30:5: LET d= declist IN e= exp SEMIC
					{
					match(input,LET,FOLLOW_LET_in_prog63); 
					nestingLevel++;
					             HashMap<String,STentry> hm = new HashMap<String,STentry> ();
					             symTable.add(hm);
					          
					pushFollow(FOLLOW_declist_in_prog84);
					d=declist();
					state._fsp--;

					match(input,IN,FOLLOW_IN_in_prog86); 
					pushFollow(FOLLOW_exp_in_prog90);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog92); 
					symTable.remove(nestingLevel--);
					          ast = new ProgLetInNode(d,e) ;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "prog"



	// $ANTLR start "declist"
	// /home/osboxes/workspace/FOOL_11_25/FOOL.g:40:1: declist returns [ArrayList<Node> astlist] : ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ ;
	public final ArrayList<Node> declist() throws RecognitionException {
		ArrayList<Node> astlist = null;


		Token i=null;
		Token fid=null;
		Token id=null;
		Node t =null;
		Node e =null;
		Node fty =null;
		Node ty =null;
		ArrayList<Node> d =null;

		try {
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:41:2: ( ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ )
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:41:4: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			{
			astlist = new ArrayList<Node>() ;
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:42:4: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			int cnt6=0;
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0==FUN||LA6_0==VAR) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:42:6: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC
					{
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:42:6: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp )
					int alt5=2;
					int LA5_0 = input.LA(1);
					if ( (LA5_0==VAR) ) {
						alt5=1;
					}
					else if ( (LA5_0==FUN) ) {
						alt5=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 5, 0, input);
						throw nvae;
					}

					switch (alt5) {
						case 1 :
							// /home/osboxes/workspace/FOOL_11_25/FOOL.g:43:13: VAR i= ID COLON t= type ASS e= exp
							{
							match(input,VAR,FOLLOW_VAR_in_declist157); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist161); 
							match(input,COLON,FOLLOW_COLON_in_declist163); 
							pushFollow(FOLLOW_type_in_declist167);
							t=type();
							state._fsp--;

							match(input,ASS,FOLLOW_ASS_in_declist169); 
							pushFollow(FOLLOW_exp_in_declist173);
							e=exp();
							state._fsp--;

							VarNode v = new VarNode((i!=null?i.getText():null),t,e);  
							             astlist.add(v);                                 
							             HashMap<String,STentry> hm = symTable.get(nestingLevel);
							             if ( hm.put((i!=null?i.getText():null),new STentry(nestingLevel,t)) != null  )
							             {System.out.println("Var id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
							              System.exit(0);}  
							            
							}
							break;
						case 2 :
							// /home/osboxes/workspace/FOOL_11_25/FOOL.g:52:13: FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp
							{
							match(input,FUN,FOLLOW_FUN_in_declist214); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist218); 
							match(input,COLON,FOLLOW_COLON_in_declist220); 
							pushFollow(FOLLOW_type_in_declist224);
							t=type();
							state._fsp--;

							//inserimento di ID nella symtable
							               FunNode f = new FunNode((i!=null?i.getText():null),t);      
							               astlist.add(f);                              
							               HashMap<String,STentry> hm = symTable.get(nestingLevel);
							               STentry entry = new STentry(nestingLevel);
							               if ( hm.put((i!=null?i.getText():null), entry) != null  )               
							               {System.out.println("Fun id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
							                System.exit(0);}
							                //creare una nuova hashmap per la symTable
							                nestingLevel++;
							                HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
							                symTable.add(hmn);
							                
							match(input,LPAR,FOLLOW_LPAR_in_declist256); 
							ArrayList<Node> parTypes = new ArrayList<Node>();
							// /home/osboxes/workspace/FOOL_11_25/FOOL.g:68:17: (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )?
							int alt3=2;
							int LA3_0 = input.LA(1);
							if ( (LA3_0==ID) ) {
								alt3=1;
							}
							switch (alt3) {
								case 1 :
									// /home/osboxes/workspace/FOOL_11_25/FOOL.g:68:18: fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )*
									{
									fid=(Token)match(input,ID,FOLLOW_ID_in_declist311); 
									match(input,COLON,FOLLOW_COLON_in_declist313); 
									pushFollow(FOLLOW_type_in_declist317);
									fty=type();
									state._fsp--;

									 
									                  parTypes.add(fty);
									                  ParNode fpar = new ParNode((fid!=null?fid.getText():null),fty); //creo nodo ParNode
									                  f.addPar(fpar);                                 //lo attacco al FunNode con addPar
									                  if ( hmn.put((fid!=null?fid.getText():null),new STentry(nestingLevel, fty)) != null  ) //aggiungo dich a hmn
									                  {System.out.println("Parameter id "+(fid!=null?fid.getText():null)+" at line "+(fid!=null?fid.getLine():0)+" already declared");
									                   System.exit(0);}
									                  
									// /home/osboxes/workspace/FOOL_11_25/FOOL.g:77:19: ( COMMA id= ID COLON ty= type )*
									loop2:
									while (true) {
										int alt2=2;
										int LA2_0 = input.LA(1);
										if ( (LA2_0==COMMA) ) {
											alt2=1;
										}

										switch (alt2) {
										case 1 :
											// /home/osboxes/workspace/FOOL_11_25/FOOL.g:77:20: COMMA id= ID COLON ty= type
											{
											match(input,COMMA,FOLLOW_COMMA_in_declist358); 
											id=(Token)match(input,ID,FOLLOW_ID_in_declist362); 
											match(input,COLON,FOLLOW_COLON_in_declist364); 
											pushFollow(FOLLOW_type_in_declist368);
											ty=type();
											state._fsp--;


											                    parTypes.add(ty);                    
											                    ParNode par = new ParNode((id!=null?id.getText():null),ty);
											                    f.addPar(par);
											                    if ( hmn.put((id!=null?id.getText():null),new STentry(nestingLevel, ty)) != null  )
											                    {System.out.println("Parameter id "+(id!=null?id.getText():null)+" at line "+(id!=null?id.getLine():0)+" already declared");
											                     System.exit(0);}
											                    
											}
											break;

										default :
											break loop2;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_declist465); 
							 entry.addType( new ArrowTypeNode(parTypes,t) ); 
							// /home/osboxes/workspace/FOOL_11_25/FOOL.g:91:15: ( LET d= declist IN )?
							int alt4=2;
							int LA4_0 = input.LA(1);
							if ( (LA4_0==LET) ) {
								alt4=1;
							}
							switch (alt4) {
								case 1 :
									// /home/osboxes/workspace/FOOL_11_25/FOOL.g:91:16: LET d= declist IN
									{
									match(input,LET,FOLLOW_LET_in_declist484); 
									pushFollow(FOLLOW_declist_in_declist488);
									d=declist();
									state._fsp--;

									match(input,IN,FOLLOW_IN_in_declist490); 
									}
									break;

							}

							pushFollow(FOLLOW_exp_in_declist496);
							e=exp();
							state._fsp--;

							//chiudere scope
							               symTable.remove(nestingLevel--);
							               f.addDecBody(d,e); 
							              
							}
							break;

					}

					match(input,SEMIC,FOLLOW_SEMIC_in_declist522); 
					}
					break;

				default :
					if ( cnt6 >= 1 ) break loop6;
					EarlyExitException eee = new EarlyExitException(6, input);
					throw eee;
				}
				cnt6++;
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return astlist;
	}
	// $ANTLR end "declist"



	// $ANTLR start "type"
	// /home/osboxes/workspace/FOOL_11_25/FOOL.g:100:1: type returns [Node ast] : ( INT | BOOL );
	public final Node type() throws RecognitionException {
		Node ast = null;


		try {
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:101:3: ( INT | BOOL )
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0==INT) ) {
				alt7=1;
			}
			else if ( (LA7_0==BOOL) ) {
				alt7=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}

			switch (alt7) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:101:5: INT
					{
					match(input,INT,FOLLOW_INT_in_type556); 
					ast =new IntTypeNode();
					}
					break;
				case 2 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:102:5: BOOL
					{
					match(input,BOOL,FOLLOW_BOOL_in_type565); 
					ast =new BoolTypeNode();
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "type"



	// $ANTLR start "exp"
	// /home/osboxes/workspace/FOOL_11_25/FOOL.g:107:1: exp returns [Node ast] : f= term ( PLUS l= term )* ;
	public final Node exp() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:108:3: (f= term ( PLUS l= term )* )
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:108:5: f= term ( PLUS l= term )*
			{
			pushFollow(FOLLOW_term_in_exp589);
			f=term();
			state._fsp--;

			ast = f;
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:109:7: ( PLUS l= term )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( (LA8_0==PLUS) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:109:8: PLUS l= term
					{
					match(input,PLUS,FOLLOW_PLUS_in_exp600); 
					pushFollow(FOLLOW_term_in_exp604);
					l=term();
					state._fsp--;

					ast = new PlusNode (ast,l);
					}
					break;

				default :
					break loop8;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "exp"



	// $ANTLR start "term"
	// /home/osboxes/workspace/FOOL_11_25/FOOL.g:114:1: term returns [Node ast] : f= factor ( TIMES l= factor )* ;
	public final Node term() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:115:2: (f= factor ( TIMES l= factor )* )
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:115:4: f= factor ( TIMES l= factor )*
			{
			pushFollow(FOLLOW_factor_in_term642);
			f=factor();
			state._fsp--;

			ast = f;
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:116:6: ( TIMES l= factor )*
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( (LA9_0==TIMES) ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:116:7: TIMES l= factor
					{
					match(input,TIMES,FOLLOW_TIMES_in_term652); 
					pushFollow(FOLLOW_factor_in_term656);
					l=factor();
					state._fsp--;

					ast = new MultNode (ast,l);
					}
					break;

				default :
					break loop9;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "term"



	// $ANTLR start "factor"
	// /home/osboxes/workspace/FOOL_11_25/FOOL.g:121:1: factor returns [Node ast] : f= value ( EQ l= value )* ;
	public final Node factor() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:122:2: (f= value ( EQ l= value )* )
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:122:4: f= value ( EQ l= value )*
			{
			pushFollow(FOLLOW_value_in_factor690);
			f=value();
			state._fsp--;

			ast = f;
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:123:6: ( EQ l= value )*
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( (LA10_0==EQ) ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:123:7: EQ l= value
					{
					match(input,EQ,FOLLOW_EQ_in_factor700); 
					pushFollow(FOLLOW_value_in_factor704);
					l=value();
					state._fsp--;

					ast = new EqualNode (ast,l);
					}
					break;

				default :
					break loop10;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "factor"



	// $ANTLR start "value"
	// /home/osboxes/workspace/FOOL_11_25/FOOL.g:128:1: value returns [Node ast] : (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )? );
	public final Node value() throws RecognitionException {
		Node ast = null;


		Token n=null;
		Token i=null;
		Node e =null;
		Node x =null;
		Node y =null;
		Node z =null;
		Node fa =null;
		Node a =null;

		try {
			// /home/osboxes/workspace/FOOL_11_25/FOOL.g:129:2: (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )? )
			int alt14=7;
			switch ( input.LA(1) ) {
			case INTEGER:
				{
				alt14=1;
				}
				break;
			case TRUE:
				{
				alt14=2;
				}
				break;
			case FALSE:
				{
				alt14=3;
				}
				break;
			case LPAR:
				{
				alt14=4;
				}
				break;
			case IF:
				{
				alt14=5;
				}
				break;
			case PRINT:
				{
				alt14=6;
				}
				break;
			case ID:
				{
				alt14=7;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 14, 0, input);
				throw nvae;
			}
			switch (alt14) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:129:4: n= INTEGER
					{
					n=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_value743); 
					ast = new IntNode(Integer.parseInt((n!=null?n.getText():null)));
					}
					break;
				case 2 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:131:4: TRUE
					{
					match(input,TRUE,FOLLOW_TRUE_in_value758); 
					ast = new BoolNode(true);
					}
					break;
				case 3 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:133:4: FALSE
					{
					match(input,FALSE,FOLLOW_FALSE_in_value771); 
					ast = new BoolNode(false);
					}
					break;
				case 4 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:135:4: LPAR e= exp RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_value783); 
					pushFollow(FOLLOW_exp_in_value787);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value789); 
					ast = e;
					}
					break;
				case 5 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:137:4: IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR
					{
					match(input,IF,FOLLOW_IF_in_value801); 
					pushFollow(FOLLOW_exp_in_value805);
					x=exp();
					state._fsp--;

					match(input,THEN,FOLLOW_THEN_in_value807); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value809); 
					pushFollow(FOLLOW_exp_in_value813);
					y=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value815); 
					match(input,ELSE,FOLLOW_ELSE_in_value823); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value825); 
					pushFollow(FOLLOW_exp_in_value829);
					z=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value831); 
					ast = new IfNode(x,y,z);
					}
					break;
				case 6 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:140:4: PRINT LPAR e= exp RPAR
					{
					match(input,PRINT,FOLLOW_PRINT_in_value844); 
					match(input,LPAR,FOLLOW_LPAR_in_value846); 
					pushFollow(FOLLOW_exp_in_value850);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value852); 
					ast = new PrintNode(e);
					}
					break;
				case 7 :
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:142:4: i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )?
					{
					i=(Token)match(input,ID,FOLLOW_ID_in_value865); 
					//cercare la dichiarazione
					           int j=nestingLevel;
					           STentry entry=null; 
					           while (j>=0 && entry==null)
					             entry=(symTable.get(j--)).get((i!=null?i.getText():null));
					           if (entry==null)
					           {System.out.println("Id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" not declared");
					            System.exit(0);}               
						   ast = new IdNode((i!=null?i.getText():null),entry);
					// /home/osboxes/workspace/FOOL_11_25/FOOL.g:152:5: ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )?
					int alt13=2;
					int LA13_0 = input.LA(1);
					if ( (LA13_0==LPAR) ) {
						alt13=1;
					}
					switch (alt13) {
						case 1 :
							// /home/osboxes/workspace/FOOL_11_25/FOOL.g:152:7: LPAR (fa= exp ( COMMA a= exp )* )? RPAR
							{
							match(input,LPAR,FOLLOW_LPAR_in_value880); 
							ArrayList<Node> argList = new ArrayList<Node>();
							// /home/osboxes/workspace/FOOL_11_25/FOOL.g:153:7: (fa= exp ( COMMA a= exp )* )?
							int alt12=2;
							int LA12_0 = input.LA(1);
							if ( (LA12_0==FALSE||(LA12_0 >= ID && LA12_0 <= IF)||LA12_0==INTEGER||LA12_0==LPAR||LA12_0==PRINT||LA12_0==TRUE) ) {
								alt12=1;
							}
							switch (alt12) {
								case 1 :
									// /home/osboxes/workspace/FOOL_11_25/FOOL.g:153:8: fa= exp ( COMMA a= exp )*
									{
									pushFollow(FOLLOW_exp_in_value895);
									fa=exp();
									state._fsp--;

									argList.add(fa);
									// /home/osboxes/workspace/FOOL_11_25/FOOL.g:154:9: ( COMMA a= exp )*
									loop11:
									while (true) {
										int alt11=2;
										int LA11_0 = input.LA(1);
										if ( (LA11_0==COMMA) ) {
											alt11=1;
										}

										switch (alt11) {
										case 1 :
											// /home/osboxes/workspace/FOOL_11_25/FOOL.g:154:10: COMMA a= exp
											{
											match(input,COMMA,FOLLOW_COMMA_in_value909); 
											pushFollow(FOLLOW_exp_in_value913);
											a=exp();
											state._fsp--;

											argList.add(a);
											}
											break;

										default :
											break loop11;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_value929); 
							ast = new CallNode((i!=null?i.getText():null),entry,argList);
							}
							break;

					}

					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "value"

	// Delegated rules



	public static final BitSet FOLLOW_exp_in_prog39 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog41 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LET_in_prog63 = new BitSet(new long[]{0x0000000040008000L});
	public static final BitSet FOLLOW_declist_in_prog84 = new BitSet(new long[]{0x0000000000040000L});
	public static final BitSet FOLLOW_IN_in_prog86 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_prog90 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog92 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_declist157 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ID_in_declist161 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_declist163 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_declist167 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_ASS_in_declist169 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_declist173 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_FUN_in_declist214 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ID_in_declist218 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_declist220 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_declist224 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_LPAR_in_declist256 = new BitSet(new long[]{0x0000000002010000L});
	public static final BitSet FOLLOW_ID_in_declist311 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_declist313 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_declist317 = new BitSet(new long[]{0x0000000002000100L});
	public static final BitSet FOLLOW_COMMA_in_declist358 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ID_in_declist362 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_declist364 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_declist368 = new BitSet(new long[]{0x0000000002000100L});
	public static final BitSet FOLLOW_RPAR_in_declist465 = new BitSet(new long[]{0x0000000021734000L});
	public static final BitSet FOLLOW_LET_in_declist484 = new BitSet(new long[]{0x0000000040008000L});
	public static final BitSet FOLLOW_declist_in_declist488 = new BitSet(new long[]{0x0000000000040000L});
	public static final BitSet FOLLOW_IN_in_declist490 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_declist496 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_SEMIC_in_declist522 = new BitSet(new long[]{0x0000000040008002L});
	public static final BitSet FOLLOW_INT_in_type556 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BOOL_in_type565 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_term_in_exp589 = new BitSet(new long[]{0x0000000000800002L});
	public static final BitSet FOLLOW_PLUS_in_exp600 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_term_in_exp604 = new BitSet(new long[]{0x0000000000800002L});
	public static final BitSet FOLLOW_factor_in_term642 = new BitSet(new long[]{0x0000000010000002L});
	public static final BitSet FOLLOW_TIMES_in_term652 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_factor_in_term656 = new BitSet(new long[]{0x0000000010000002L});
	public static final BitSet FOLLOW_value_in_factor690 = new BitSet(new long[]{0x0000000000001002L});
	public static final BitSet FOLLOW_EQ_in_factor700 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_value_in_factor704 = new BitSet(new long[]{0x0000000000001002L});
	public static final BitSet FOLLOW_INTEGER_in_value743 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_value758 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_value771 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_value783 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_value787 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_RPAR_in_value789 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_value801 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_value805 = new BitSet(new long[]{0x0000000008000000L});
	public static final BitSet FOLLOW_THEN_in_value807 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_CLPAR_in_value809 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_value813 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_CRPAR_in_value815 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_ELSE_in_value823 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_CLPAR_in_value825 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_value829 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_CRPAR_in_value831 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_value844 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_LPAR_in_value846 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_value850 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_RPAR_in_value852 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_value865 = new BitSet(new long[]{0x0000000000400002L});
	public static final BitSet FOLLOW_LPAR_in_value880 = new BitSet(new long[]{0x0000000023534000L});
	public static final BitSet FOLLOW_exp_in_value895 = new BitSet(new long[]{0x0000000002000100L});
	public static final BitSet FOLLOW_COMMA_in_value909 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_value913 = new BitSet(new long[]{0x0000000002000100L});
	public static final BitSet FOLLOW_RPAR_in_value929 = new BitSet(new long[]{0x0000000000000002L});
}
