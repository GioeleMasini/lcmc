// $ANTLR 3.5.2 /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g 2017-04-11 22:53:11

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class Exp1_11_04Lexer extends Lexer {
	public static final int EOF=-1;
	public static final int ERR=4;
	public static final int LPAR=5;
	public static final int NUM=6;
	public static final int PLUS=7;
	public static final int RPAR=8;
	public static final int TIMES=9;
	public static final int WHITESP=10;

	  int lexicalErrors=0;


	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public Exp1_11_04Lexer() {} 
	public Exp1_11_04Lexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public Exp1_11_04Lexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g"; }

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:51:5: ( '+' )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:51:7: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "TIMES"
	public final void mTIMES() throws RecognitionException {
		try {
			int _type = TIMES;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:52:6: ( '*' )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:52:8: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TIMES"

	// $ANTLR start "LPAR"
	public final void mLPAR() throws RecognitionException {
		try {
			int _type = LPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:53:5: ( '(' )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:53:7: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAR"

	// $ANTLR start "RPAR"
	public final void mRPAR() throws RecognitionException {
		try {
			int _type = RPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:54:5: ( ')' )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:54:7: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAR"

	// $ANTLR start "NUM"
	public final void mNUM() throws RecognitionException {
		try {
			int _type = NUM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:55:4: ( ( '1' .. '9' ) ( '0' .. '9' )* | '0' )
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( ((LA2_0 >= '1' && LA2_0 <= '9')) ) {
				alt2=1;
			}
			else if ( (LA2_0=='0') ) {
				alt2=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:55:6: ( '1' .. '9' ) ( '0' .. '9' )*
					{
					if ( (input.LA(1) >= '1' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:55:16: ( '0' .. '9' )*
					loop1:
					while (true) {
						int alt1=2;
						int LA1_0 = input.LA(1);
						if ( ((LA1_0 >= '0' && LA1_0 <= '9')) ) {
							alt1=1;
						}

						switch (alt1) {
						case 1 :
							// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop1;
						}
					}

					}
					break;
				case 2 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:55:30: '0'
					{
					match('0'); 
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NUM"

	// $ANTLR start "WHITESP"
	public final void mWHITESP() throws RecognitionException {
		try {
			int _type = WHITESP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:56:8: ( ( ' ' | '\\t' | '\\n' | '\\r' )+ )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:56:10: ( ' ' | '\\t' | '\\n' | '\\r' )+
			{
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:56:10: ( ' ' | '\\t' | '\\n' | '\\r' )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '\t' && LA3_0 <= '\n')||LA3_0=='\r'||LA3_0==' ') ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			 _channel = HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHITESP"

	// $ANTLR start "ERR"
	public final void mERR() throws RecognitionException {
		try {
			int _type = ERR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:57:4: ( . )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:57:6: .
			{
			matchAny(); 

			  System.err.println("Invalid char: " + getText()); 
			  lexicalErrors++;
			  _channel = HIDDEN;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ERR"

	@Override
	public void mTokens() throws RecognitionException {
		// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:1:8: ( PLUS | TIMES | LPAR | RPAR | NUM | WHITESP | ERR )
		int alt4=7;
		int LA4_0 = input.LA(1);
		if ( (LA4_0=='+') ) {
			alt4=1;
		}
		else if ( (LA4_0=='*') ) {
			alt4=2;
		}
		else if ( (LA4_0=='(') ) {
			alt4=3;
		}
		else if ( (LA4_0==')') ) {
			alt4=4;
		}
		else if ( ((LA4_0 >= '1' && LA4_0 <= '9')) ) {
			alt4=5;
		}
		else if ( (LA4_0=='0') ) {
			alt4=5;
		}
		else if ( ((LA4_0 >= '\t' && LA4_0 <= '\n')||LA4_0=='\r'||LA4_0==' ') ) {
			alt4=6;
		}
		else if ( ((LA4_0 >= '\u0000' && LA4_0 <= '\b')||(LA4_0 >= '\u000B' && LA4_0 <= '\f')||(LA4_0 >= '\u000E' && LA4_0 <= '\u001F')||(LA4_0 >= '!' && LA4_0 <= '\'')||(LA4_0 >= ',' && LA4_0 <= '/')||(LA4_0 >= ':' && LA4_0 <= '\uFFFF')) ) {
			alt4=7;
		}

		else {
			NoViableAltException nvae =
				new NoViableAltException("", 4, 0, input);
			throw nvae;
		}

		switch (alt4) {
			case 1 :
				// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:1:10: PLUS
				{
				mPLUS(); 

				}
				break;
			case 2 :
				// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:1:15: TIMES
				{
				mTIMES(); 

				}
				break;
			case 3 :
				// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:1:21: LPAR
				{
				mLPAR(); 

				}
				break;
			case 4 :
				// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:1:26: RPAR
				{
				mRPAR(); 

				}
				break;
			case 5 :
				// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:1:31: NUM
				{
				mNUM(); 

				}
				break;
			case 6 :
				// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:1:35: WHITESP
				{
				mWHITESP(); 

				}
				break;
			case 7 :
				// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:1:43: ERR
				{
				mERR(); 

				}
				break;

		}
	}



}
