/* Esercizio dell'esercitazione del 04/11
   Grammatica:
   E -> T + E | T
   T -> V * T | V
   V -> n | ( E )
   
   RISOLUZIONE:
   b) non è ricorsivo a sinistra
   c) Left-factoring
   E -> TX
   X -> + E | \epsilon
   T -> VY
   Y -> * T | \epsilon
   V -> n | ( E )
 */
grammar Exp1_11_04;

@lexer::members {
  int lexicalErrors=0;
}

prog
  : expE { System.out.println("Parsing finished!"); }
  ;
  
expE
  : termT termX
  ;
  
termX
  : PLUS expE
  | /* epsilon */
  ;
  
termT
  : valueV termY
  ;
  
termY
  : TIMES termT
  | /* epsilon */
  ;
  
valueV
  : NUM
  | LPAR expE RPAR
  ;

  
  
PLUS: '+';
TIMES: '*';
LPAR: '(';
RPAR: ')';
NUM: ('1'..'9')('0'..'9')* | '0';
WHITESP: (' '|'\t'|'\n'|'\r')+ { $channel = HIDDEN; };
ERR: . {
  System.err.println("Invalid char: " + $text); 
  lexicalErrors++;
  $channel = HIDDEN;
};
