/* Esercizio dell'esercitazione del 04/11
   Grammatica:
	 E -> E + T | T
	 T -> T * V | V
	 V -> n | ( E )
   
   RISOLUZIONE:
   b) Rimozione ricorsione a sinistra
   E -> T E'
   E' -> + T E' | \epsilon
   T -> V T'
   T' -> * V T' | \epsilon
   V -> n | ( E )
   
   c) Già fattorizzato a sinistra
 */
grammar Exp2_11_04;

@lexer::members {
  int lexicalErrors=0;
}

prog
  : exp { System.out.println("Parsing finished!"); }
  ;
  
exp
  : term exp1
  ;

exp1
  : PLUS term exp1
  | /* epsilon */
  ;

term
  : value term1
  ;
  
term1
  : TIMES value term1
  | /* epsilon */
  ;
  
value
  : NUM
  | LPAR exp RPAR
  ;
  
PLUS: '+';
TIMES: '*';
LPAR: '(';
RPAR: ')';
NUM: ('1'..'9')('0'..'9')* | '0';
WHITESP: (' '|'\t'|'\n'|'\r')+ { $channel = HIDDEN; };
ERR: . {
  System.err.println("Invalid char: " + $text); 
  lexicalErrors++;
  $channel = HIDDEN;
};
