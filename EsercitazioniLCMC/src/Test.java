import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.antlr.runtime.*;

public class Test {
    public static void main(String[] args) throws Exception {
        testGrammar("Esempio_Stackoverflow");
        testGrammar("Exp_10_28");
        testGrammar("Exp1_11_04");
        testGrammar("Exp2_11_04");
    }
    
    private static void testGrammar(String grammarName) throws Exception {
    	System.out.println("========= " + grammarName + " =========");
        // Recupero il file con il test
    	// Il nome del file deve essere "nomeGrammatica.testcase.txt" e trovarsi nella cartella src/
    	ANTLRFileStream input = new ANTLRFileStream("testcases/" + grammarName + ".txt");
        
    	// Istanzio il lexer con la reflection
        // Es. senza reflection: 
		// 		Exp_10_28Lexer lexer = new Exp_10_28Lexer(input);
    	Lexer lexer = (Lexer) Class.forName(grammarName + "Lexer")
        		.getDeclaredConstructor(CharStream.class)
				.newInstance(input);
    	
    	// Ottengo i tokens del lexer
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        
        // Istanzio il parser con la reflection
        // Es. senza reflection: 
        // 		Exp_10_28Parser parser = new Exp_10_28Parser(tokens);
        Parser parser = (Parser) Class.forName(grammarName + "Parser")
        		.getDeclaredConstructor(TokenStream.class)
        		.newInstance(tokens);
        

        // Avvio il test della grammatica con la reflection
        // Es. senza reflection:
        //		parser.prog();
        Method prog = parser.getClass().getDeclaredMethod("prog");
        prog.invoke(parser);

        // Recupero il numero di errori di lessico con la reflection
        // Es. senza reflection:
        //		int lexicalErrorsValue = lexer.lexicalErrors;
        Field lexicalErrors = lexer.getClass().getDeclaredField("lexicalErrors");
        int lexicalErrorsValue = (int) lexicalErrors.get(lexer);
        
        // Stampo il risultato del test
        System.out.println("You had: " + lexicalErrorsValue+" lexical errors and "+parser.getNumberOfSyntaxErrors()+" syntax errors.");
        System.out.println("------------------------------\n");
    }
}
