/* Esercizio dell'esercitazione del 28/10*/
grammar Exp_10_28;

@lexer::members {
  int lexicalErrors=0;
}
  
// PARSER RULES
prog
  : exp { System.out.println("Parsing finished!"); }
  ;
  
exp
  : term exp1
  ;

exp1
  : PLUS exp 
  | /* epsilon */
  ;
  
term
  : value term1
  ;

term1
  : TIMES term 
  | /* epsilon */
  ;
  
value
  : NUM 
  | LPAR exp RPAR
  ;
  

// LEXER RULES

PLUS  : '+' ;
TIMES : '*' ;
LPAR  : '(' ;
RPAR  : ')' ;
NUM : ('1'..'9')('0'..'9')* | '0' ;   
WHITESP : (' '|'\t'|'\n'|'\r')+ { $channel = HIDDEN; };
ERR : . {
  System.err.println("Invalid char: " + $text); 
  lexicalErrors++;
  $channel = HIDDEN;
};
