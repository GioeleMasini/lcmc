// $ANTLR 3.5.2 /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g 2017-04-11 22:53:07

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class Exp_10_28Parser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ERR", "LPAR", "NUM", "PLUS", 
		"RPAR", "TIMES", "WHITESP"
	};
	public static final int EOF=-1;
	public static final int ERR=4;
	public static final int LPAR=5;
	public static final int NUM=6;
	public static final int PLUS=7;
	public static final int RPAR=8;
	public static final int TIMES=9;
	public static final int WHITESP=10;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public Exp_10_28Parser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public Exp_10_28Parser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return Exp_10_28Parser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g"; }



	// $ANTLR start "prog"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:9:1: prog : exp ;
	public final void prog() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:10:3: ( exp )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:10:5: exp
			{
			pushFollow(FOLLOW_exp_in_prog26);
			exp();
			state._fsp--;

			 System.out.println("Parsing finished!"); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "prog"



	// $ANTLR start "exp"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:13:1: exp : term exp1 ;
	public final void exp() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:14:3: ( term exp1 )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:14:5: term exp1
			{
			pushFollow(FOLLOW_term_in_exp43);
			term();
			state._fsp--;

			pushFollow(FOLLOW_exp1_in_exp45);
			exp1();
			state._fsp--;

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "exp"



	// $ANTLR start "exp1"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:17:1: exp1 : ( PLUS exp |);
	public final void exp1() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:18:3: ( PLUS exp |)
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==PLUS) ) {
				alt1=1;
			}
			else if ( (LA1_0==EOF||LA1_0==RPAR) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:18:5: PLUS exp
					{
					match(input,PLUS,FOLLOW_PLUS_in_exp158); 
					pushFollow(FOLLOW_exp_in_exp160);
					exp();
					state._fsp--;

					}
					break;
				case 2 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:20:3: 
					{
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "exp1"



	// $ANTLR start "term"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:22:1: term : value term1 ;
	public final void term() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:23:3: ( value term1 )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:23:5: value term1
			{
			pushFollow(FOLLOW_value_in_term82);
			value();
			state._fsp--;

			pushFollow(FOLLOW_term1_in_term84);
			term1();
			state._fsp--;

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "term"



	// $ANTLR start "term1"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:26:1: term1 : ( TIMES term |);
	public final void term1() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:27:3: ( TIMES term |)
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0==TIMES) ) {
				alt2=1;
			}
			else if ( (LA2_0==EOF||(LA2_0 >= PLUS && LA2_0 <= RPAR)) ) {
				alt2=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:27:5: TIMES term
					{
					match(input,TIMES,FOLLOW_TIMES_in_term197); 
					pushFollow(FOLLOW_term_in_term199);
					term();
					state._fsp--;

					}
					break;
				case 2 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:29:3: 
					{
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "term1"



	// $ANTLR start "value"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:31:1: value : ( NUM | LPAR exp RPAR );
	public final void value() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:32:3: ( NUM | LPAR exp RPAR )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0==NUM) ) {
				alt3=1;
			}
			else if ( (LA3_0==LPAR) ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:32:5: NUM
					{
					match(input,NUM,FOLLOW_NUM_in_value121); 
					}
					break;
				case 2 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp_10_28.g:33:5: LPAR exp RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_value128); 
					pushFollow(FOLLOW_exp_in_value130);
					exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value132); 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "value"

	// Delegated rules



	public static final BitSet FOLLOW_exp_in_prog26 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_term_in_exp43 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_exp1_in_exp45 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PLUS_in_exp158 = new BitSet(new long[]{0x0000000000000060L});
	public static final BitSet FOLLOW_exp_in_exp160 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_value_in_term82 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_term1_in_term84 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TIMES_in_term197 = new BitSet(new long[]{0x0000000000000060L});
	public static final BitSet FOLLOW_term_in_term199 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NUM_in_value121 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_value128 = new BitSet(new long[]{0x0000000000000060L});
	public static final BitSet FOLLOW_exp_in_value130 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_RPAR_in_value132 = new BitSet(new long[]{0x0000000000000002L});
}
