// $ANTLR 3.5.2 /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g 2017-04-11 22:53:11

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class Exp1_11_04Parser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ERR", "LPAR", "NUM", "PLUS", 
		"RPAR", "TIMES", "WHITESP"
	};
	public static final int EOF=-1;
	public static final int ERR=4;
	public static final int LPAR=5;
	public static final int NUM=6;
	public static final int PLUS=7;
	public static final int RPAR=8;
	public static final int TIMES=9;
	public static final int WHITESP=10;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public Exp1_11_04Parser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public Exp1_11_04Parser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return Exp1_11_04Parser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g"; }



	// $ANTLR start "prog"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:22:1: prog : expE ;
	public final void prog() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:23:3: ( expE )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:23:5: expE
			{
			pushFollow(FOLLOW_expE_in_prog23);
			expE();
			state._fsp--;

			 System.out.println("Parsing finished!"); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "prog"



	// $ANTLR start "expE"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:26:1: expE : termT termX ;
	public final void expE() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:27:3: ( termT termX )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:27:5: termT termX
			{
			pushFollow(FOLLOW_termT_in_expE40);
			termT();
			state._fsp--;

			pushFollow(FOLLOW_termX_in_expE42);
			termX();
			state._fsp--;

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "expE"



	// $ANTLR start "termX"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:30:1: termX : ( PLUS expE |);
	public final void termX() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:31:3: ( PLUS expE |)
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==PLUS) ) {
				alt1=1;
			}
			else if ( (LA1_0==EOF||LA1_0==RPAR) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:31:5: PLUS expE
					{
					match(input,PLUS,FOLLOW_PLUS_in_termX57); 
					pushFollow(FOLLOW_expE_in_termX59);
					expE();
					state._fsp--;

					}
					break;
				case 2 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:33:3: 
					{
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "termX"



	// $ANTLR start "termT"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:35:1: termT : valueV termY ;
	public final void termT() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:36:3: ( valueV termY )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:36:5: valueV termY
			{
			pushFollow(FOLLOW_valueV_in_termT80);
			valueV();
			state._fsp--;

			pushFollow(FOLLOW_termY_in_termT82);
			termY();
			state._fsp--;

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "termT"



	// $ANTLR start "termY"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:39:1: termY : ( TIMES termT |);
	public final void termY() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:40:3: ( TIMES termT |)
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0==TIMES) ) {
				alt2=1;
			}
			else if ( (LA2_0==EOF||(LA2_0 >= PLUS && LA2_0 <= RPAR)) ) {
				alt2=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:40:5: TIMES termT
					{
					match(input,TIMES,FOLLOW_TIMES_in_termY97); 
					pushFollow(FOLLOW_termT_in_termY99);
					termT();
					state._fsp--;

					}
					break;
				case 2 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:42:3: 
					{
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "termY"



	// $ANTLR start "valueV"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:44:1: valueV : ( NUM | LPAR expE RPAR );
	public final void valueV() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:45:3: ( NUM | LPAR expE RPAR )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0==NUM) ) {
				alt3=1;
			}
			else if ( (LA3_0==LPAR) ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:45:5: NUM
					{
					match(input,NUM,FOLLOW_NUM_in_valueV120); 
					}
					break;
				case 2 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Exp1_11_04.g:46:5: LPAR expE RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_valueV126); 
					pushFollow(FOLLOW_expE_in_valueV128);
					expE();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_valueV130); 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "valueV"

	// Delegated rules



	public static final BitSet FOLLOW_expE_in_prog23 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_termT_in_expE40 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_termX_in_expE42 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PLUS_in_termX57 = new BitSet(new long[]{0x0000000000000060L});
	public static final BitSet FOLLOW_expE_in_termX59 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_valueV_in_termT80 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_termY_in_termT82 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TIMES_in_termY97 = new BitSet(new long[]{0x0000000000000060L});
	public static final BitSet FOLLOW_termT_in_termY99 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NUM_in_valueV120 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_valueV126 = new BitSet(new long[]{0x0000000000000060L});
	public static final BitSet FOLLOW_expE_in_valueV128 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_RPAR_in_valueV130 = new BitSet(new long[]{0x0000000000000002L});
}
