// $ANTLR 3.5.2 /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g 2017-04-11 22:53:12

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class Esempio_StackoverflowParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "Number", "WS", "'('", "')'", 
		"'*'", "'+'", "'-'", "'/'"
	};
	public static final int EOF=-1;
	public static final int T__6=6;
	public static final int T__7=7;
	public static final int T__8=8;
	public static final int T__9=9;
	public static final int T__10=10;
	public static final int T__11=11;
	public static final int Number=4;
	public static final int WS=5;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public Esempio_StackoverflowParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public Esempio_StackoverflowParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return Esempio_StackoverflowParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g"; }



	// $ANTLR start "prog"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:12:1: prog : additionExp ;
	public final void prog() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:13:5: ( additionExp )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:13:10: additionExp
			{
			pushFollow(FOLLOW_additionExp_in_prog30);
			additionExp();
			state._fsp--;

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "prog"



	// $ANTLR start "additionExp"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:17:1: additionExp : multiplyExp ( '+' multiplyExp | '-' multiplyExp )* ;
	public final void additionExp() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:18:5: ( multiplyExp ( '+' multiplyExp | '-' multiplyExp )* )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:18:10: multiplyExp ( '+' multiplyExp | '-' multiplyExp )*
			{
			pushFollow(FOLLOW_multiplyExp_in_additionExp56);
			multiplyExp();
			state._fsp--;

			// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:19:10: ( '+' multiplyExp | '-' multiplyExp )*
			loop1:
			while (true) {
				int alt1=3;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==9) ) {
					alt1=1;
				}
				else if ( (LA1_0==10) ) {
					alt1=2;
				}

				switch (alt1) {
				case 1 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:19:12: '+' multiplyExp
					{
					match(input,9,FOLLOW_9_in_additionExp70); 
					pushFollow(FOLLOW_multiplyExp_in_additionExp72);
					multiplyExp();
					state._fsp--;

					}
					break;
				case 2 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:20:12: '-' multiplyExp
					{
					match(input,10,FOLLOW_10_in_additionExp86); 
					pushFollow(FOLLOW_multiplyExp_in_additionExp88);
					multiplyExp();
					state._fsp--;

					}
					break;

				default :
					break loop1;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "additionExp"



	// $ANTLR start "multiplyExp"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:25:1: multiplyExp : atomExp ( '*' atomExp | '/' atomExp )* ;
	public final void multiplyExp() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:26:5: ( atomExp ( '*' atomExp | '/' atomExp )* )
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:26:10: atomExp ( '*' atomExp | '/' atomExp )*
			{
			pushFollow(FOLLOW_atomExp_in_multiplyExp126);
			atomExp();
			state._fsp--;

			// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:27:10: ( '*' atomExp | '/' atomExp )*
			loop2:
			while (true) {
				int alt2=3;
				int LA2_0 = input.LA(1);
				if ( (LA2_0==8) ) {
					alt2=1;
				}
				else if ( (LA2_0==11) ) {
					alt2=2;
				}

				switch (alt2) {
				case 1 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:27:12: '*' atomExp
					{
					match(input,8,FOLLOW_8_in_multiplyExp139); 
					pushFollow(FOLLOW_atomExp_in_multiplyExp141);
					atomExp();
					state._fsp--;

					}
					break;
				case 2 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:28:12: '/' atomExp
					{
					match(input,11,FOLLOW_11_in_multiplyExp155); 
					pushFollow(FOLLOW_atomExp_in_multiplyExp157);
					atomExp();
					state._fsp--;

					}
					break;

				default :
					break loop2;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "multiplyExp"



	// $ANTLR start "atomExp"
	// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:35:1: atomExp : ( Number | '(' additionExp ')' );
	public final void atomExp() throws RecognitionException {
		try {
			// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:36:5: ( Number | '(' additionExp ')' )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0==Number) ) {
				alt3=1;
			}
			else if ( (LA3_0==6) ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:36:10: Number
					{
					match(input,Number,FOLLOW_Number_in_atomExp192); 
					}
					break;
				case 2 :
					// /home/osboxes/workspace/EsercitazioniLCMC/src/Esempio_Stackoverflow.g:37:10: '(' additionExp ')'
					{
					match(input,6,FOLLOW_6_in_atomExp203); 
					pushFollow(FOLLOW_additionExp_in_atomExp205);
					additionExp();
					state._fsp--;

					match(input,7,FOLLOW_7_in_atomExp207); 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "atomExp"

	// Delegated rules



	public static final BitSet FOLLOW_additionExp_in_prog30 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_multiplyExp_in_additionExp56 = new BitSet(new long[]{0x0000000000000602L});
	public static final BitSet FOLLOW_9_in_additionExp70 = new BitSet(new long[]{0x0000000000000050L});
	public static final BitSet FOLLOW_multiplyExp_in_additionExp72 = new BitSet(new long[]{0x0000000000000602L});
	public static final BitSet FOLLOW_10_in_additionExp86 = new BitSet(new long[]{0x0000000000000050L});
	public static final BitSet FOLLOW_multiplyExp_in_additionExp88 = new BitSet(new long[]{0x0000000000000602L});
	public static final BitSet FOLLOW_atomExp_in_multiplyExp126 = new BitSet(new long[]{0x0000000000000902L});
	public static final BitSet FOLLOW_8_in_multiplyExp139 = new BitSet(new long[]{0x0000000000000050L});
	public static final BitSet FOLLOW_atomExp_in_multiplyExp141 = new BitSet(new long[]{0x0000000000000902L});
	public static final BitSet FOLLOW_11_in_multiplyExp155 = new BitSet(new long[]{0x0000000000000050L});
	public static final BitSet FOLLOW_atomExp_in_multiplyExp157 = new BitSet(new long[]{0x0000000000000902L});
	public static final BitSet FOLLOW_Number_in_atomExp192 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_6_in_atomExp203 = new BitSet(new long[]{0x0000000000000050L});
	public static final BitSet FOLLOW_additionExp_in_atomExp205 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_7_in_atomExp207 = new BitSet(new long[]{0x0000000000000002L});
}
