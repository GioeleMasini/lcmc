package ast;

public class IfNode implements Node {
	private Node conditionExp;
	private Node thenExp;
	private Node elseExp;
	
	public IfNode(Node conditionExp, Node thenExp, Node elseExp) {
		super();
		this.conditionExp = conditionExp;
		this.thenExp = thenExp;
		this.elseExp = elseExp;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "If\n"
				+ conditionExp.toPrint(indent + "\t")
				+ thenExp.toPrint(indent + "\t")
				+ elseExp.toPrint(indent + "\t");
	}

}
