package ast;

public class TimesNode implements Node {
	private Node factor1;
	private Node factor2;

	public TimesNode(Node factor1, Node factor2) {
		super();
		this.factor1 = factor1;
		this.factor2 = factor2;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Times\n"
			+ factor1.toPrint(indent + "\t")
			+ factor2.toPrint(indent + "\t");
	}

}
