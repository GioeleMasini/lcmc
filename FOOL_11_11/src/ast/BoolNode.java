package ast;

public class BoolNode implements Node {
	private Boolean value;

	public BoolNode(Boolean value) {
		super();
		this.value = value;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Bool:" + value + "\n";
	}

}
