package ast;

public class IntNode implements Node {
	private Integer value;

	public IntNode(Integer value) {
		super();
		this.value = value;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Int:" + value + "\n";
	}

}
