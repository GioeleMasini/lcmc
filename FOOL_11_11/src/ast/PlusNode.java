package ast;

public class PlusNode implements Node {
	private Node term1;
	private Node term2;
	
	public PlusNode(Node term1, Node term2) {
		super();
		this.term1 = term1;
		this.term2 = term2;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Plus\n"
				+ term1.toPrint(indent + "\t")
				+ term2.toPrint(indent + "\t");
	}

}
