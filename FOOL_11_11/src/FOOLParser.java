// $ANTLR 3.5.2 /home/osboxes/workspace/FOOL_11_11/src/FOOL.g 2017-04-11 22:53:14

import ast.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "CLPAR", "CRPAR", "ELSE", "EQ", 
		"ERR", "FALSE", "IF", "INTEGER", "LPAR", "PLUS", "PRINT", "RPAR", "SEMIC", 
		"THEN", "TIMES", "TRUE", "WHITESP"
	};
	public static final int EOF=-1;
	public static final int CLPAR=4;
	public static final int CRPAR=5;
	public static final int ELSE=6;
	public static final int EQ=7;
	public static final int ERR=8;
	public static final int FALSE=9;
	public static final int IF=10;
	public static final int INTEGER=11;
	public static final int LPAR=12;
	public static final int PLUS=13;
	public static final int PRINT=14;
	public static final int RPAR=15;
	public static final int SEMIC=16;
	public static final int THEN=17;
	public static final int TIMES=18;
	public static final int TRUE=19;
	public static final int WHITESP=20;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public FOOLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return FOOLParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/osboxes/workspace/FOOL_11_11/src/FOOL.g"; }



	// $ANTLR start "prog"
	// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:16:1: prog returns [Node node] : e= exp SEMIC ;
	public final Node prog() throws RecognitionException {
		Node node = null;


		Node e =null;

		try {
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:16:25: (e= exp SEMIC )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:16:27: e= exp SEMIC
			{
			pushFollow(FOLLOW_exp_in_prog34);
			e=exp();
			state._fsp--;

			match(input,SEMIC,FOLLOW_SEMIC_in_prog36); 
			 node = new ProgNode(e); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return node;
	}
	// $ANTLR end "prog"



	// $ANTLR start "exp"
	// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:18:1: exp returns [Node node] : n= term ( PLUS n= term )* ;
	public final Node exp() throws RecognitionException {
		Node node = null;


		Node n =null;

		try {
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:18:24: (n= term ( PLUS n= term )* )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:18:26: n= term ( PLUS n= term )*
			{
			pushFollow(FOLLOW_term_in_exp56);
			n=term();
			state._fsp--;

			 node = n; 
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:18:54: ( PLUS n= term )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==PLUS) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:18:55: PLUS n= term
					{
					match(input,PLUS,FOLLOW_PLUS_in_exp61); 
					pushFollow(FOLLOW_term_in_exp65);
					n=term();
					state._fsp--;

					 node = new PlusNode(node, n); 
					}
					break;

				default :
					break loop1;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return node;
	}
	// $ANTLR end "exp"



	// $ANTLR start "term"
	// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:20:1: term returns [Node node] : n= factor ( TIMES n= factor )* ;
	public final Node term() throws RecognitionException {
		Node node = null;


		Node n =null;

		try {
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:20:25: (n= factor ( TIMES n= factor )* )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:20:27: n= factor ( TIMES n= factor )*
			{
			pushFollow(FOLLOW_factor_in_term86);
			n=factor();
			state._fsp--;

			 node = n; 
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:20:57: ( TIMES n= factor )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( (LA2_0==TIMES) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:20:58: TIMES n= factor
					{
					match(input,TIMES,FOLLOW_TIMES_in_term91); 
					pushFollow(FOLLOW_factor_in_term95);
					n=factor();
					state._fsp--;

					 node = new TimesNode(node, n); 
					}
					break;

				default :
					break loop2;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return node;
	}
	// $ANTLR end "term"



	// $ANTLR start "factor"
	// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:22:1: factor returns [Node node] : n= value ( EQ n= value )* ;
	public final Node factor() throws RecognitionException {
		Node node = null;


		Node n =null;

		try {
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:22:27: (n= value ( EQ n= value )* )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:22:29: n= value ( EQ n= value )*
			{
			pushFollow(FOLLOW_value_in_factor116);
			n=value();
			state._fsp--;

			 node = n; 
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:22:58: ( EQ n= value )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( (LA3_0==EQ) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:22:59: EQ n= value
					{
					match(input,EQ,FOLLOW_EQ_in_factor121); 
					pushFollow(FOLLOW_value_in_factor125);
					n=value();
					state._fsp--;

					 node = new EqualNode(node, n); 
					}
					break;

				default :
					break loop3;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return node;
	}
	// $ANTLR end "factor"



	// $ANTLR start "value"
	// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:24:1: value returns [Node node] : (i= INTEGER |b= TRUE |b= FALSE | LPAR e= exp RPAR | IF c= exp THEN CLPAR t= exp CRPAR ELSE CLPAR e= exp CRPAR | PRINT LPAR e= exp RPAR );
	public final Node value() throws RecognitionException {
		Node node = null;


		Token i=null;
		Token b=null;
		Node e =null;
		Node c =null;
		Node t =null;

		try {
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:24:26: (i= INTEGER |b= TRUE |b= FALSE | LPAR e= exp RPAR | IF c= exp THEN CLPAR t= exp CRPAR ELSE CLPAR e= exp CRPAR | PRINT LPAR e= exp RPAR )
			int alt4=6;
			switch ( input.LA(1) ) {
			case INTEGER:
				{
				alt4=1;
				}
				break;
			case TRUE:
				{
				alt4=2;
				}
				break;
			case FALSE:
				{
				alt4=3;
				}
				break;
			case LPAR:
				{
				alt4=4;
				}
				break;
			case IF:
				{
				alt4=5;
				}
				break;
			case PRINT:
				{
				alt4=6;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}
			switch (alt4) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:25:5: i= INTEGER
					{
					i=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_value166); 
					 node = new IntNode(Integer.parseInt((i!=null?i.getText():null))); 
					}
					break;
				case 2 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:26:5: b= TRUE
					{
					b=(Token)match(input,TRUE,FOLLOW_TRUE_in_value176); 
					 node = new BoolNode(Boolean.parseBoolean((b!=null?b.getText():null))); 
					}
					break;
				case 3 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:27:5: b= FALSE
					{
					b=(Token)match(input,FALSE,FOLLOW_FALSE_in_value186); 
					 node = new BoolNode(Boolean.parseBoolean((b!=null?b.getText():null))); 
					}
					break;
				case 4 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:28:5: LPAR e= exp RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_value194); 
					pushFollow(FOLLOW_exp_in_value198);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value200); 
					 node = e; 
					}
					break;
				case 5 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:29:5: IF c= exp THEN CLPAR t= exp CRPAR ELSE CLPAR e= exp CRPAR
					{
					match(input,IF,FOLLOW_IF_in_value208); 
					pushFollow(FOLLOW_exp_in_value212);
					c=exp();
					state._fsp--;

					match(input,THEN,FOLLOW_THEN_in_value214); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value216); 
					pushFollow(FOLLOW_exp_in_value220);
					t=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value222); 
					match(input,ELSE,FOLLOW_ELSE_in_value231); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value233); 
					pushFollow(FOLLOW_exp_in_value237);
					e=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value239); 
					 node = new IfNode(c, t, e); 
					}
					break;
				case 6 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:31:5: PRINT LPAR e= exp RPAR
					{
					match(input,PRINT,FOLLOW_PRINT_in_value247); 
					match(input,LPAR,FOLLOW_LPAR_in_value249); 
					pushFollow(FOLLOW_exp_in_value253);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value255); 
					 node = new PrintNode(e); 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return node;
	}
	// $ANTLR end "value"

	// Delegated rules



	public static final BitSet FOLLOW_exp_in_prog34 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_SEMIC_in_prog36 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_term_in_exp56 = new BitSet(new long[]{0x0000000000002002L});
	public static final BitSet FOLLOW_PLUS_in_exp61 = new BitSet(new long[]{0x0000000000085E00L});
	public static final BitSet FOLLOW_term_in_exp65 = new BitSet(new long[]{0x0000000000002002L});
	public static final BitSet FOLLOW_factor_in_term86 = new BitSet(new long[]{0x0000000000040002L});
	public static final BitSet FOLLOW_TIMES_in_term91 = new BitSet(new long[]{0x0000000000085E00L});
	public static final BitSet FOLLOW_factor_in_term95 = new BitSet(new long[]{0x0000000000040002L});
	public static final BitSet FOLLOW_value_in_factor116 = new BitSet(new long[]{0x0000000000000082L});
	public static final BitSet FOLLOW_EQ_in_factor121 = new BitSet(new long[]{0x0000000000085E00L});
	public static final BitSet FOLLOW_value_in_factor125 = new BitSet(new long[]{0x0000000000000082L});
	public static final BitSet FOLLOW_INTEGER_in_value166 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_value176 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_value186 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_value194 = new BitSet(new long[]{0x0000000000085E00L});
	public static final BitSet FOLLOW_exp_in_value198 = new BitSet(new long[]{0x0000000000008000L});
	public static final BitSet FOLLOW_RPAR_in_value200 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_value208 = new BitSet(new long[]{0x0000000000085E00L});
	public static final BitSet FOLLOW_exp_in_value212 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_THEN_in_value214 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_CLPAR_in_value216 = new BitSet(new long[]{0x0000000000085E00L});
	public static final BitSet FOLLOW_exp_in_value220 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_CRPAR_in_value222 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ELSE_in_value231 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_CLPAR_in_value233 = new BitSet(new long[]{0x0000000000085E00L});
	public static final BitSet FOLLOW_exp_in_value237 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_CRPAR_in_value239 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_value247 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_LPAR_in_value249 = new BitSet(new long[]{0x0000000000085E00L});
	public static final BitSet FOLLOW_exp_in_value253 = new BitSet(new long[]{0x0000000000008000L});
	public static final BitSet FOLLOW_RPAR_in_value255 = new BitSet(new long[]{0x0000000000000002L});
}
