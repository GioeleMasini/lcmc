grammar FOOL;

@header {
import ast.*;
}

@lexer::members {
int lexicalErrors=0;
}


/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/

prog returns [Node node]: e=exp SEMIC { $node = new ProgNode($e.node); }; 
    
exp returns [Node node]: n=term { $node = $n.node; } (PLUS n=term { $node = new PlusNode($node, $n.node); })* ; 
  
term returns [Node node]: n=factor { $node = $n.node; } (TIMES n=factor { $node = new TimesNode($node, $n.node); })* ; 
  
factor returns [Node node]: n=value { $node = $n.node; } (EQ n=value { $node = new EqualNode($node, $n.node); })* ;    
              
value returns [Node node]: 
    i=INTEGER { $node = new IntNode(Integer.parseInt($i.text)); }
  | b=TRUE { $node = new BoolNode(Boolean.parseBoolean($b.text)); }
  | b=FALSE { $node = new BoolNode(Boolean.parseBoolean($b.text)); }
  | LPAR e=exp RPAR { $node = $e.node; }
  | IF c=exp THEN CLPAR t=exp CRPAR
       ELSE CLPAR e=exp CRPAR { $node = new IfNode($c.node, $t.node, $e.node); }
  | PRINT LPAR e=exp RPAR { $node = new PrintNode($e.node); }
  ;

      
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/
SEMIC : ';' ;
EQ  : '==' ;
PLUS  : '+' ;
TIMES : '*' ;
INTEGER : ('-')?(('1'..'9')('0'..'9')*) | '0';
TRUE  : 'true' ;
FALSE : 'false' ;
LPAR  : '(' ;
RPAR  : ')' ;
CLPAR   : '{' ;
CRPAR : '}' ;
IF  : 'if' ;
THEN  : 'then' ;
ELSE  : 'else' ;
PRINT : 'print' ; 

WHITESP  : ( '\t' | ' ' | '\r' | '\n' )+    { $channel=HIDDEN; } ;

ERR      : . { System.err.println("Invalid char: "+$text); lexicalErrors++; $channel=HIDDEN; } ; 

