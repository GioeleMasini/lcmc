// $ANTLR 3.5.2 /home/osboxes/workspace/FOOL_11_11/src/FOOL.g 2017-04-11 22:53:15

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLLexer extends Lexer {
	public static final int EOF=-1;
	public static final int CLPAR=4;
	public static final int CRPAR=5;
	public static final int ELSE=6;
	public static final int EQ=7;
	public static final int ERR=8;
	public static final int FALSE=9;
	public static final int IF=10;
	public static final int INTEGER=11;
	public static final int LPAR=12;
	public static final int PLUS=13;
	public static final int PRINT=14;
	public static final int RPAR=15;
	public static final int SEMIC=16;
	public static final int THEN=17;
	public static final int TIMES=18;
	public static final int TRUE=19;
	public static final int WHITESP=20;

	int lexicalErrors=0;


	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public FOOLLexer() {} 
	public FOOLLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/home/osboxes/workspace/FOOL_11_11/src/FOOL.g"; }

	// $ANTLR start "SEMIC"
	public final void mSEMIC() throws RecognitionException {
		try {
			int _type = SEMIC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:38:7: ( ';' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:38:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMIC"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			int _type = EQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:39:5: ( '==' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:39:7: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQ"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:40:7: ( '+' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:40:9: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "TIMES"
	public final void mTIMES() throws RecognitionException {
		try {
			int _type = TIMES;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:41:7: ( '*' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:41:9: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TIMES"

	// $ANTLR start "INTEGER"
	public final void mINTEGER() throws RecognitionException {
		try {
			int _type = INTEGER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:42:9: ( ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* ) | '0' )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='-'||(LA3_0 >= '1' && LA3_0 <= '9')) ) {
				alt3=1;
			}
			else if ( (LA3_0=='0') ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:42:11: ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* )
					{
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:42:11: ( '-' )?
					int alt1=2;
					int LA1_0 = input.LA(1);
					if ( (LA1_0=='-') ) {
						alt1=1;
					}
					switch (alt1) {
						case 1 :
							// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:42:12: '-'
							{
							match('-'); 
							}
							break;

					}

					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:42:17: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:42:18: ( '1' .. '9' ) ( '0' .. '9' )*
					{
					if ( (input.LA(1) >= '1' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:42:28: ( '0' .. '9' )*
					loop2:
					while (true) {
						int alt2=2;
						int LA2_0 = input.LA(1);
						if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
							alt2=1;
						}

						switch (alt2) {
						case 1 :
							// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop2;
						}
					}

					}

					}
					break;
				case 2 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:42:43: '0'
					{
					match('0'); 
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTEGER"

	// $ANTLR start "TRUE"
	public final void mTRUE() throws RecognitionException {
		try {
			int _type = TRUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:43:7: ( 'true' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:43:9: 'true'
			{
			match("true"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TRUE"

	// $ANTLR start "FALSE"
	public final void mFALSE() throws RecognitionException {
		try {
			int _type = FALSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:44:7: ( 'false' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:44:9: 'false'
			{
			match("false"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FALSE"

	// $ANTLR start "LPAR"
	public final void mLPAR() throws RecognitionException {
		try {
			int _type = LPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:45:7: ( '(' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:45:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAR"

	// $ANTLR start "RPAR"
	public final void mRPAR() throws RecognitionException {
		try {
			int _type = RPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:46:7: ( ')' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:46:9: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAR"

	// $ANTLR start "CLPAR"
	public final void mCLPAR() throws RecognitionException {
		try {
			int _type = CLPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:47:9: ( '{' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:47:11: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLPAR"

	// $ANTLR start "CRPAR"
	public final void mCRPAR() throws RecognitionException {
		try {
			int _type = CRPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:48:7: ( '}' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:48:9: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CRPAR"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:49:5: ( 'if' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:49:7: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:50:7: ( 'then' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:50:9: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:51:7: ( 'else' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:51:9: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			int _type = PRINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:52:7: ( 'print' )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:52:9: 'print'
			{
			match("print"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PRINT"

	// $ANTLR start "WHITESP"
	public final void mWHITESP() throws RecognitionException {
		try {
			int _type = WHITESP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:54:10: ( ( '\\t' | ' ' | '\\r' | '\\n' )+ )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:54:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
			{
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:54:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= '\t' && LA4_0 <= '\n')||LA4_0=='\r'||LA4_0==' ') ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHITESP"

	// $ANTLR start "ERR"
	public final void mERR() throws RecognitionException {
		try {
			int _type = ERR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:56:10: ( . )
			// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:56:12: .
			{
			matchAny(); 
			 System.err.println("Invalid char: "+getText()); lexicalErrors++; _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ERR"

	@Override
	public void mTokens() throws RecognitionException {
		// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:8: ( SEMIC | EQ | PLUS | TIMES | INTEGER | TRUE | FALSE | LPAR | RPAR | CLPAR | CRPAR | IF | THEN | ELSE | PRINT | WHITESP | ERR )
		int alt5=17;
		int LA5_0 = input.LA(1);
		if ( (LA5_0==';') ) {
			alt5=1;
		}
		else if ( (LA5_0=='=') ) {
			int LA5_2 = input.LA(2);
			if ( (LA5_2=='=') ) {
				alt5=2;
			}

			else {
				alt5=17;
			}

		}
		else if ( (LA5_0=='+') ) {
			alt5=3;
		}
		else if ( (LA5_0=='*') ) {
			alt5=4;
		}
		else if ( (LA5_0=='-') ) {
			int LA5_5 = input.LA(2);
			if ( ((LA5_5 >= '1' && LA5_5 <= '9')) ) {
				alt5=5;
			}

			else {
				alt5=17;
			}

		}
		else if ( ((LA5_0 >= '1' && LA5_0 <= '9')) ) {
			alt5=5;
		}
		else if ( (LA5_0=='0') ) {
			alt5=5;
		}
		else if ( (LA5_0=='t') ) {
			switch ( input.LA(2) ) {
			case 'r':
				{
				alt5=6;
				}
				break;
			case 'h':
				{
				alt5=13;
				}
				break;
			default:
				alt5=17;
			}
		}
		else if ( (LA5_0=='f') ) {
			int LA5_9 = input.LA(2);
			if ( (LA5_9=='a') ) {
				alt5=7;
			}

			else {
				alt5=17;
			}

		}
		else if ( (LA5_0=='(') ) {
			alt5=8;
		}
		else if ( (LA5_0==')') ) {
			alt5=9;
		}
		else if ( (LA5_0=='{') ) {
			alt5=10;
		}
		else if ( (LA5_0=='}') ) {
			alt5=11;
		}
		else if ( (LA5_0=='i') ) {
			int LA5_14 = input.LA(2);
			if ( (LA5_14=='f') ) {
				alt5=12;
			}

			else {
				alt5=17;
			}

		}
		else if ( (LA5_0=='e') ) {
			int LA5_15 = input.LA(2);
			if ( (LA5_15=='l') ) {
				alt5=14;
			}

			else {
				alt5=17;
			}

		}
		else if ( (LA5_0=='p') ) {
			int LA5_16 = input.LA(2);
			if ( (LA5_16=='r') ) {
				alt5=15;
			}

			else {
				alt5=17;
			}

		}
		else if ( ((LA5_0 >= '\t' && LA5_0 <= '\n')||LA5_0=='\r'||LA5_0==' ') ) {
			alt5=16;
		}
		else if ( ((LA5_0 >= '\u0000' && LA5_0 <= '\b')||(LA5_0 >= '\u000B' && LA5_0 <= '\f')||(LA5_0 >= '\u000E' && LA5_0 <= '\u001F')||(LA5_0 >= '!' && LA5_0 <= '\'')||LA5_0==','||(LA5_0 >= '.' && LA5_0 <= '/')||LA5_0==':'||LA5_0=='<'||(LA5_0 >= '>' && LA5_0 <= 'd')||(LA5_0 >= 'g' && LA5_0 <= 'h')||(LA5_0 >= 'j' && LA5_0 <= 'o')||(LA5_0 >= 'q' && LA5_0 <= 's')||(LA5_0 >= 'u' && LA5_0 <= 'z')||LA5_0=='|'||(LA5_0 >= '~' && LA5_0 <= '\uFFFF')) ) {
			alt5=17;
		}

		else {
			NoViableAltException nvae =
				new NoViableAltException("", 5, 0, input);
			throw nvae;
		}

		switch (alt5) {
			case 1 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:10: SEMIC
				{
				mSEMIC(); 

				}
				break;
			case 2 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:16: EQ
				{
				mEQ(); 

				}
				break;
			case 3 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:19: PLUS
				{
				mPLUS(); 

				}
				break;
			case 4 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:24: TIMES
				{
				mTIMES(); 

				}
				break;
			case 5 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:30: INTEGER
				{
				mINTEGER(); 

				}
				break;
			case 6 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:38: TRUE
				{
				mTRUE(); 

				}
				break;
			case 7 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:43: FALSE
				{
				mFALSE(); 

				}
				break;
			case 8 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:49: LPAR
				{
				mLPAR(); 

				}
				break;
			case 9 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:54: RPAR
				{
				mRPAR(); 

				}
				break;
			case 10 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:59: CLPAR
				{
				mCLPAR(); 

				}
				break;
			case 11 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:65: CRPAR
				{
				mCRPAR(); 

				}
				break;
			case 12 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:71: IF
				{
				mIF(); 

				}
				break;
			case 13 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:74: THEN
				{
				mTHEN(); 

				}
				break;
			case 14 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:79: ELSE
				{
				mELSE(); 

				}
				break;
			case 15 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:84: PRINT
				{
				mPRINT(); 

				}
				break;
			case 16 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:90: WHITESP
				{
				mWHITESP(); 

				}
				break;
			case 17 :
				// /home/osboxes/workspace/FOOL_11_11/src/FOOL.g:1:98: ERR
				{
				mERR(); 

				}
				break;

		}
	}



}
