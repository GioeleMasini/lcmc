//import java.io.*;
import org.antlr.runtime.*;

import ast.Node;

public class Test {
    public static void main(String[] args) throws Exception {
      
        String fileName = "src/prova.fool";
      
        ANTLRFileStream input = new ANTLRFileStream(fileName);
        FOOLLexer lexer = new FOOLLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FOOLParser parser = new FOOLParser(tokens);
         
        Node ast = parser.prog();
        System.out.println("You had: "+lexer.lexicalErrors+" lexical errors and "+parser.getNumberOfSyntaxErrors()+" syntax errors.");

        System.out.println("Visualizing AST...");
        System.out.print(ast.toPrint(""));
        
    }
}
