grammar FOOL;

@header {
import ast.*;
import java.util.HashMap;
import java.util.ArrayList;
}

@lexer::members {
int lexicalErrors=0;
}

@members {
private int nestingLevel=-1;
private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/

prog	returns [Node ast]
	: e=exp SEMIC	
          {$ast = new ProgNode($e.ast);}
  | LET   { 
          nestingLevel++; 
          HashMap<String,STentry> hm= new HashMap<String,STentry>();
          symTable.add(hm);
          }  
          d=declist IN e=exp SEMIC            
          {
          symTable.remove(nestingLevel--);
          $ast = new ProgLetInNode($d.astlist, $e.ast);
          }
	;
	 	
declist returns [ArrayList<Node> astlist]
  :       {$astlist=new ArrayList<Node>();} 
          (

            (   VAR i=ID COLON t=type ASS e=exp 
                    {
                    $astlist.add(new VarNode($i.text, $t.ast, $e.ast));
                    HashMap<String,STentry> hm = symTable.get(nestingLevel);
                    if (hm.put($i.text,new STentry(nestingLevel))!=null) 
                      { System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared!");
                        System.exit(0);}
                    }
              | FUN i=ID COLON t=type               
                    {FunNode f=new FunNode($i.text, $t.ast);
                     $astlist.add(f);
                     HashMap<String,STentry> hm = symTable.get(nestingLevel);
                     if (hm.put($i.text,new STentry(nestingLevel))!=null) 
                      { System.out.println("Fun id "+$i.text+" at line "+$i.line+" already declared!");
                        System.exit(0);}
                     // entro in un nuovo scope
                     nestingLevel++; 
                     HashMap<String,STentry> hmn= new HashMap<String,STentry>();
                     symTable.add(hmn);
                     }
                LPAR 
            
                (i=ID COLON t=type
                  {//creare il ParNode
                   //lo attacco al FunNode f invocando f.addPar 
                   //aggiungo una STentry alla hashmap hmn
                   ParNode firstParam = new ParNode($i.text, $t.ast);
                   f.addPar(firstParam);
                   
                   if (hmn.put($i.text, new STentry(nestingLevel)) != null) 
                      { System.out.println("Param id "+$i.text+" at line "+$i.line+" already declared!");
                        System.exit(0);}
                  }                
                    (COMMA i=ID COLON t=type 
                      {//creare il ParNode
                       //lo attacco al FunNode f invocando f.addPar 
                       //aggiungo una STentry alla hashmap hmn
		                   ParNode anotherParam = new ParNode($i.text, $t.ast);
		                   f.addPar(anotherParam);
                   
		                   if (hmn.put($i.text, new STentry(nestingLevel)) != null) 
		                      { System.out.println("Param id "+$i.text+" at line "+$i.line+" already declared!");
		                        System.exit(0);}
                      }
                    )* 
                )? 
              
                RPAR (LET d=declist IN)? e=exp 
                    {
                    //esco dallo scope
                    symTable.remove(nestingLevel--);
                    f.addBody($d.astlist, $e.ast);
                    }
            ) SEMIC 
          )+
        ;
        	 	
exp	returns [Node ast]
 	: f=term {$ast= $f.ast;}
 	    (PLUS l=term
 	     {$ast= new PlusNode ($ast,$l.ast);}
 	    )*
 	;
 	
term	returns [Node ast]    //
	: f=factor {$ast= $f.ast;}
	    (TIMES l=factor
	     {$ast= new TimesNode ($ast,$l.ast);}
	    )*
	;
	
factor	returns [Node ast]  ///
	: f=value {$ast= $f.ast;}
	    (EQ l=value 
	     {$ast= new EqualNode ($ast,$l.ast);}
	    )*
 	;	 	
 	          	
value	returns [Node ast]
	: n=INTEGER   
	  {$ast= new IntNode(Integer.parseInt($n.text));}  
	| TRUE 
	  {$ast= new BoolNode(true);}  //
	| FALSE
	  {$ast= new BoolNode(false);}  //
	| LPAR e=exp RPAR
	  {$ast= $e.ast;}  
	| IF x=exp THEN CLPAR y=exp CRPAR  ///
		   ELSE CLPAR z=exp CRPAR 
	  {$ast= new IfNode($x.ast,$y.ast,$z.ast);}	 
	| PRINT LPAR e=exp RPAR	///
	  {$ast= new PrintNode($e.ast);} 	
	| i=ID 
	  {//cerco la dichiarazione
	  int j=nestingLevel;
	  STentry entry=null;
	  while (j>=0 && entry==null)
	    entry=(symTable.get(j--)).get($i.text);
	  if (entry==null)
	    { System.out.println("Id "+$i.text+" at line "+$i.line+" not declared!");
        System.exit(0);}
	  $ast= new IdNode($i.text,entry);
	  }
 	; 

type returns [Node ast] 
     : INT      {$ast= new IntTypeNode();}
     | BOOL     {$ast= new BoolTypeNode();}        
     ;  
  		
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

COLON : ':' ;
COMMA : ',' ;
ASS : '=' ;
SEMIC	: ';' ;
EQ	: '==' ;
PLUS	: '+' ;
TIMES	: '*' ;
INTEGER	: ('-')?(('1'..'9')('0'..'9')*) | '0';
TRUE	: 'true' ;
FALSE	: 'false' ;
LPAR 	: '(' ;
RPAR	: ')' ;
CLPAR 	: '{' ;
CRPAR	: '}' ;
IF 	: 'if' ;
THEN 	: 'then' ;
ELSE 	: 'else' ;
PRINT	: 'print' ; 
LET : 'let' ;
IN  : 'in' ;
VAR : 'var' ;
FUN : 'fun' ;
INT : 'int' ;
BOOL  : 'bool' ;

ID  : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')* ;

WHITESP  : ( '\t' | ' ' | '\r' | '\n' )+    { $channel=HIDDEN; } ;

COMMENT : '/*' .* '*/' { $channel=HIDDEN; } ;

ERR      : . { System.err.println("Invalid char: "+$text); lexicalErrors++; $channel=HIDDEN; } ; 

