// $ANTLR 3.5.2 /home/osboxes/workspace/FOOL_11_18/FOOL.g 2017-04-11 22:53:16

import ast.*;
import java.util.HashMap;
import java.util.ArrayList;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ASS", "BOOL", "CLPAR", "COLON", 
		"COMMA", "COMMENT", "CRPAR", "ELSE", "EQ", "ERR", "FALSE", "FUN", "ID", 
		"IF", "IN", "INT", "INTEGER", "LET", "LPAR", "PLUS", "PRINT", "RPAR", 
		"SEMIC", "THEN", "TIMES", "TRUE", "VAR", "WHITESP"
	};
	public static final int EOF=-1;
	public static final int ASS=4;
	public static final int BOOL=5;
	public static final int CLPAR=6;
	public static final int COLON=7;
	public static final int COMMA=8;
	public static final int COMMENT=9;
	public static final int CRPAR=10;
	public static final int ELSE=11;
	public static final int EQ=12;
	public static final int ERR=13;
	public static final int FALSE=14;
	public static final int FUN=15;
	public static final int ID=16;
	public static final int IF=17;
	public static final int IN=18;
	public static final int INT=19;
	public static final int INTEGER=20;
	public static final int LET=21;
	public static final int LPAR=22;
	public static final int PLUS=23;
	public static final int PRINT=24;
	public static final int RPAR=25;
	public static final int SEMIC=26;
	public static final int THEN=27;
	public static final int TIMES=28;
	public static final int TRUE=29;
	public static final int VAR=30;
	public static final int WHITESP=31;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public FOOLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return FOOLParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/osboxes/workspace/FOOL_11_18/FOOL.g"; }


	private int nestingLevel=-1;
	private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();



	// $ANTLR start "prog"
	// /home/osboxes/workspace/FOOL_11_18/FOOL.g:22:1: prog returns [Node ast] : (e= exp SEMIC | LET d= declist IN e= exp SEMIC );
	public final Node prog() throws RecognitionException {
		Node ast = null;


		Node e =null;
		ArrayList<Node> d =null;

		try {
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:23:2: (e= exp SEMIC | LET d= declist IN e= exp SEMIC )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==FALSE||(LA1_0 >= ID && LA1_0 <= IF)||LA1_0==INTEGER||LA1_0==LPAR||LA1_0==PRINT||LA1_0==TRUE) ) {
				alt1=1;
			}
			else if ( (LA1_0==LET) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:23:4: e= exp SEMIC
					{
					pushFollow(FOLLOW_exp_in_prog41);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog43); 
					ast = new ProgNode(e);
					}
					break;
				case 2 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:25:5: LET d= declist IN e= exp SEMIC
					{
					match(input,LET,FOLLOW_LET_in_prog62); 
					 
					          nestingLevel++; 
					          HashMap<String,STentry> hm= new HashMap<String,STentry>();
					          symTable.add(hm);
					          
					pushFollow(FOLLOW_declist_in_prog82);
					d=declist();
					state._fsp--;

					match(input,IN,FOLLOW_IN_in_prog84); 
					pushFollow(FOLLOW_exp_in_prog88);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog90); 

					          symTable.remove(nestingLevel--);
					          ast = new ProgLetInNode(d, e);
					          
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "prog"



	// $ANTLR start "declist"
	// /home/osboxes/workspace/FOOL_11_18/FOOL.g:37:1: declist returns [ArrayList<Node> astlist] : ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (i= ID COLON t= type ( COMMA i= ID COLON t= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ ;
	public final ArrayList<Node> declist() throws RecognitionException {
		ArrayList<Node> astlist = null;


		Token i=null;
		Node t =null;
		Node e =null;
		ArrayList<Node> d =null;

		try {
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:38:3: ( ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (i= ID COLON t= type ( COMMA i= ID COLON t= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ )
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:38:11: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (i= ID COLON t= type ( COMMA i= ID COLON t= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			{
			astlist =new ArrayList<Node>();
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:39:11: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (i= ID COLON t= type ( COMMA i= ID COLON t= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			int cnt6=0;
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0==FUN||LA6_0==VAR) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:41:13: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (i= ID COLON t= type ( COMMA i= ID COLON t= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC
					{
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:41:13: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (i= ID COLON t= type ( COMMA i= ID COLON t= type )* )? RPAR ( LET d= declist IN )? e= exp )
					int alt5=2;
					int LA5_0 = input.LA(1);
					if ( (LA5_0==VAR) ) {
						alt5=1;
					}
					else if ( (LA5_0==FUN) ) {
						alt5=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 5, 0, input);
						throw nvae;
					}

					switch (alt5) {
						case 1 :
							// /home/osboxes/workspace/FOOL_11_18/FOOL.g:41:17: VAR i= ID COLON t= type ASS e= exp
							{
							match(input,VAR,FOLLOW_VAR_in_declist171); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist175); 
							match(input,COLON,FOLLOW_COLON_in_declist177); 
							pushFollow(FOLLOW_type_in_declist181);
							t=type();
							state._fsp--;

							match(input,ASS,FOLLOW_ASS_in_declist183); 
							pushFollow(FOLLOW_exp_in_declist187);
							e=exp();
							state._fsp--;


							                    astlist.add(new VarNode((i!=null?i.getText():null), t, e));
							                    HashMap<String,STentry> hm = symTable.get(nestingLevel);
							                    if (hm.put((i!=null?i.getText():null),new STentry(nestingLevel))!=null) 
							                      { System.out.println("Var id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared!");
							                        System.exit(0);}
							                    
							}
							break;
						case 2 :
							// /home/osboxes/workspace/FOOL_11_18/FOOL.g:49:17: FUN i= ID COLON t= type LPAR (i= ID COLON t= type ( COMMA i= ID COLON t= type )* )? RPAR ( LET d= declist IN )? e= exp
							{
							match(input,FUN,FOLLOW_FUN_in_declist228); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist232); 
							match(input,COLON,FOLLOW_COLON_in_declist234); 
							pushFollow(FOLLOW_type_in_declist238);
							t=type();
							state._fsp--;

							FunNode f=new FunNode((i!=null?i.getText():null), t);
							                     astlist.add(f);
							                     HashMap<String,STentry> hm = symTable.get(nestingLevel);
							                     if (hm.put((i!=null?i.getText():null),new STentry(nestingLevel))!=null) 
							                      { System.out.println("Fun id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared!");
							                        System.exit(0);}
							                     // entro in un nuovo scope
							                     nestingLevel++; 
							                     HashMap<String,STentry> hmn= new HashMap<String,STentry>();
							                     symTable.add(hmn);
							                     
							match(input,LPAR,FOLLOW_LPAR_in_declist293); 
							// /home/osboxes/workspace/FOOL_11_18/FOOL.g:63:17: (i= ID COLON t= type ( COMMA i= ID COLON t= type )* )?
							int alt3=2;
							int LA3_0 = input.LA(1);
							if ( (LA3_0==ID) ) {
								alt3=1;
							}
							switch (alt3) {
								case 1 :
									// /home/osboxes/workspace/FOOL_11_18/FOOL.g:63:18: i= ID COLON t= type ( COMMA i= ID COLON t= type )*
									{
									i=(Token)match(input,ID,FOLLOW_ID_in_declist328); 
									match(input,COLON,FOLLOW_COLON_in_declist330); 
									pushFollow(FOLLOW_type_in_declist334);
									t=type();
									state._fsp--;

									//creare il ParNode
									                   //lo attacco al FunNode f invocando f.addPar 
									                   //aggiungo una STentry alla hashmap hmn
									                   ParNode firstParam = new ParNode((i!=null?i.getText():null), t);
									                   f.addPar(firstParam);
									                   
									                   if (hmn.put((i!=null?i.getText():null), new STentry(nestingLevel)) != null) 
									                      { System.out.println("Param id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared!");
									                        System.exit(0);}
									                  
									// /home/osboxes/workspace/FOOL_11_18/FOOL.g:74:21: ( COMMA i= ID COLON t= type )*
									loop2:
									while (true) {
										int alt2=2;
										int LA2_0 = input.LA(1);
										if ( (LA2_0==COMMA) ) {
											alt2=1;
										}

										switch (alt2) {
										case 1 :
											// /home/osboxes/workspace/FOOL_11_18/FOOL.g:74:22: COMMA i= ID COLON t= type
											{
											match(input,COMMA,FOLLOW_COMMA_in_declist393); 
											i=(Token)match(input,ID,FOLLOW_ID_in_declist397); 
											match(input,COLON,FOLLOW_COLON_in_declist399); 
											pushFollow(FOLLOW_type_in_declist403);
											t=type();
											state._fsp--;

											//creare il ParNode
											                       //lo attacco al FunNode f invocando f.addPar 
											                       //aggiungo una STentry alla hashmap hmn
													                   ParNode anotherParam = new ParNode((i!=null?i.getText():null), t);
													                   f.addPar(anotherParam);
											                   
													                   if (hmn.put((i!=null?i.getText():null), new STentry(nestingLevel)) != null) 
													                      { System.out.println("Param id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared!");
													                        System.exit(0);}
											                      
											}
											break;

										default :
											break loop2;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_declist505); 
							// /home/osboxes/workspace/FOOL_11_18/FOOL.g:88:22: ( LET d= declist IN )?
							int alt4=2;
							int LA4_0 = input.LA(1);
							if ( (LA4_0==LET) ) {
								alt4=1;
							}
							switch (alt4) {
								case 1 :
									// /home/osboxes/workspace/FOOL_11_18/FOOL.g:88:23: LET d= declist IN
									{
									match(input,LET,FOLLOW_LET_in_declist508); 
									pushFollow(FOLLOW_declist_in_declist512);
									d=declist();
									state._fsp--;

									match(input,IN,FOLLOW_IN_in_declist514); 
									}
									break;

							}

							pushFollow(FOLLOW_exp_in_declist520);
							e=exp();
							state._fsp--;


							                    //esco dallo scope
							                    symTable.remove(nestingLevel--);
							                    f.addBody(d, e);
							                    
							}
							break;

					}

					match(input,SEMIC,FOLLOW_SEMIC_in_declist559); 
					}
					break;

				default :
					if ( cnt6 >= 1 ) break loop6;
					EarlyExitException eee = new EarlyExitException(6, input);
					throw eee;
				}
				cnt6++;
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return astlist;
	}
	// $ANTLR end "declist"



	// $ANTLR start "exp"
	// /home/osboxes/workspace/FOOL_11_18/FOOL.g:98:1: exp returns [Node ast] : f= term ( PLUS l= term )* ;
	public final Node exp() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:99:3: (f= term ( PLUS l= term )* )
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:99:5: f= term ( PLUS l= term )*
			{
			pushFollow(FOLLOW_term_in_exp609);
			f=term();
			state._fsp--;

			ast = f;
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:100:7: ( PLUS l= term )*
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( (LA7_0==PLUS) ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:100:8: PLUS l= term
					{
					match(input,PLUS,FOLLOW_PLUS_in_exp620); 
					pushFollow(FOLLOW_term_in_exp624);
					l=term();
					state._fsp--;

					ast = new PlusNode (ast,l);
					}
					break;

				default :
					break loop7;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "exp"



	// $ANTLR start "term"
	// /home/osboxes/workspace/FOOL_11_18/FOOL.g:105:1: term returns [Node ast] : f= factor ( TIMES l= factor )* ;
	public final Node term() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:106:2: (f= factor ( TIMES l= factor )* )
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:106:4: f= factor ( TIMES l= factor )*
			{
			pushFollow(FOLLOW_factor_in_term666);
			f=factor();
			state._fsp--;

			ast = f;
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:107:6: ( TIMES l= factor )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( (LA8_0==TIMES) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:107:7: TIMES l= factor
					{
					match(input,TIMES,FOLLOW_TIMES_in_term676); 
					pushFollow(FOLLOW_factor_in_term680);
					l=factor();
					state._fsp--;

					ast = new TimesNode (ast,l);
					}
					break;

				default :
					break loop8;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "term"



	// $ANTLR start "factor"
	// /home/osboxes/workspace/FOOL_11_18/FOOL.g:112:1: factor returns [Node ast] : f= value ( EQ l= value )* ;
	public final Node factor() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:113:2: (f= value ( EQ l= value )* )
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:113:4: f= value ( EQ l= value )*
			{
			pushFollow(FOLLOW_value_in_factor716);
			f=value();
			state._fsp--;

			ast = f;
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:114:6: ( EQ l= value )*
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( (LA9_0==EQ) ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:114:7: EQ l= value
					{
					match(input,EQ,FOLLOW_EQ_in_factor726); 
					pushFollow(FOLLOW_value_in_factor730);
					l=value();
					state._fsp--;

					ast = new EqualNode (ast,l);
					}
					break;

				default :
					break loop9;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "factor"



	// $ANTLR start "value"
	// /home/osboxes/workspace/FOOL_11_18/FOOL.g:119:1: value returns [Node ast] : (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR |i= ID );
	public final Node value() throws RecognitionException {
		Node ast = null;


		Token n=null;
		Token i=null;
		Node e =null;
		Node x =null;
		Node y =null;
		Node z =null;

		try {
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:120:2: (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR |i= ID )
			int alt10=7;
			switch ( input.LA(1) ) {
			case INTEGER:
				{
				alt10=1;
				}
				break;
			case TRUE:
				{
				alt10=2;
				}
				break;
			case FALSE:
				{
				alt10=3;
				}
				break;
			case LPAR:
				{
				alt10=4;
				}
				break;
			case IF:
				{
				alt10=5;
				}
				break;
			case PRINT:
				{
				alt10=6;
				}
				break;
			case ID:
				{
				alt10=7;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 10, 0, input);
				throw nvae;
			}
			switch (alt10) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:120:4: n= INTEGER
					{
					n=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_value781); 
					ast = new IntNode(Integer.parseInt((n!=null?n.getText():null)));
					}
					break;
				case 2 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:122:4: TRUE
					{
					match(input,TRUE,FOLLOW_TRUE_in_value796); 
					ast = new BoolNode(true);
					}
					break;
				case 3 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:124:4: FALSE
					{
					match(input,FALSE,FOLLOW_FALSE_in_value809); 
					ast = new BoolNode(false);
					}
					break;
				case 4 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:126:4: LPAR e= exp RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_value821); 
					pushFollow(FOLLOW_exp_in_value825);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value827); 
					ast = e;
					}
					break;
				case 5 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:128:4: IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR
					{
					match(input,IF,FOLLOW_IF_in_value839); 
					pushFollow(FOLLOW_exp_in_value843);
					x=exp();
					state._fsp--;

					match(input,THEN,FOLLOW_THEN_in_value845); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value847); 
					pushFollow(FOLLOW_exp_in_value851);
					y=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value853); 
					match(input,ELSE,FOLLOW_ELSE_in_value862); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value864); 
					pushFollow(FOLLOW_exp_in_value868);
					z=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value870); 
					ast = new IfNode(x,y,z);
					}
					break;
				case 6 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:131:4: PRINT LPAR e= exp RPAR
					{
					match(input,PRINT,FOLLOW_PRINT_in_value883); 
					match(input,LPAR,FOLLOW_LPAR_in_value885); 
					pushFollow(FOLLOW_exp_in_value889);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value891); 
					ast = new PrintNode(e);
					}
					break;
				case 7 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:133:4: i= ID
					{
					i=(Token)match(input,ID,FOLLOW_ID_in_value906); 
					//cerco la dichiarazione
						  int j=nestingLevel;
						  STentry entry=null;
						  while (j>=0 && entry==null)
						    entry=(symTable.get(j--)).get((i!=null?i.getText():null));
						  if (entry==null)
						    { System.out.println("Id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" not declared!");
					        System.exit(0);}
						  ast = new IdNode((i!=null?i.getText():null),entry);
						  
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "value"



	// $ANTLR start "type"
	// /home/osboxes/workspace/FOOL_11_18/FOOL.g:146:1: type returns [Node ast] : ( INT | BOOL );
	public final Node type() throws RecognitionException {
		Node ast = null;


		try {
			// /home/osboxes/workspace/FOOL_11_18/FOOL.g:147:6: ( INT | BOOL )
			int alt11=2;
			int LA11_0 = input.LA(1);
			if ( (LA11_0==INT) ) {
				alt11=1;
			}
			else if ( (LA11_0==BOOL) ) {
				alt11=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}

			switch (alt11) {
				case 1 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:147:8: INT
					{
					match(input,INT,FOLLOW_INT_in_type934); 
					ast = new IntTypeNode();
					}
					break;
				case 2 :
					// /home/osboxes/workspace/FOOL_11_18/FOOL.g:148:8: BOOL
					{
					match(input,BOOL,FOLLOW_BOOL_in_type950); 
					ast = new BoolTypeNode();
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "type"

	// Delegated rules



	public static final BitSet FOLLOW_exp_in_prog41 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog43 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LET_in_prog62 = new BitSet(new long[]{0x0000000040008000L});
	public static final BitSet FOLLOW_declist_in_prog82 = new BitSet(new long[]{0x0000000000040000L});
	public static final BitSet FOLLOW_IN_in_prog84 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_prog88 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog90 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_declist171 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ID_in_declist175 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_declist177 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_declist181 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_ASS_in_declist183 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_declist187 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_FUN_in_declist228 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ID_in_declist232 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_declist234 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_declist238 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_LPAR_in_declist293 = new BitSet(new long[]{0x0000000002010000L});
	public static final BitSet FOLLOW_ID_in_declist328 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_declist330 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_declist334 = new BitSet(new long[]{0x0000000002000100L});
	public static final BitSet FOLLOW_COMMA_in_declist393 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ID_in_declist397 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_declist399 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_declist403 = new BitSet(new long[]{0x0000000002000100L});
	public static final BitSet FOLLOW_RPAR_in_declist505 = new BitSet(new long[]{0x0000000021734000L});
	public static final BitSet FOLLOW_LET_in_declist508 = new BitSet(new long[]{0x0000000040008000L});
	public static final BitSet FOLLOW_declist_in_declist512 = new BitSet(new long[]{0x0000000000040000L});
	public static final BitSet FOLLOW_IN_in_declist514 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_declist520 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_SEMIC_in_declist559 = new BitSet(new long[]{0x0000000040008002L});
	public static final BitSet FOLLOW_term_in_exp609 = new BitSet(new long[]{0x0000000000800002L});
	public static final BitSet FOLLOW_PLUS_in_exp620 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_term_in_exp624 = new BitSet(new long[]{0x0000000000800002L});
	public static final BitSet FOLLOW_factor_in_term666 = new BitSet(new long[]{0x0000000010000002L});
	public static final BitSet FOLLOW_TIMES_in_term676 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_factor_in_term680 = new BitSet(new long[]{0x0000000010000002L});
	public static final BitSet FOLLOW_value_in_factor716 = new BitSet(new long[]{0x0000000000001002L});
	public static final BitSet FOLLOW_EQ_in_factor726 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_value_in_factor730 = new BitSet(new long[]{0x0000000000001002L});
	public static final BitSet FOLLOW_INTEGER_in_value781 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_value796 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_value809 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_value821 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_value825 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_RPAR_in_value827 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_value839 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_value843 = new BitSet(new long[]{0x0000000008000000L});
	public static final BitSet FOLLOW_THEN_in_value845 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_CLPAR_in_value847 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_value851 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_CRPAR_in_value853 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_ELSE_in_value862 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_CLPAR_in_value864 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_value868 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_CRPAR_in_value870 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_value883 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_LPAR_in_value885 = new BitSet(new long[]{0x0000000021534000L});
	public static final BitSet FOLLOW_exp_in_value889 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_RPAR_in_value891 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_value906 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INT_in_type934 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BOOL_in_type950 = new BitSet(new long[]{0x0000000000000002L});
}
