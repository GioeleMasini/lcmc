package ast;
public class IdNode implements Node {

  private String id;
  private STentry entry;
  
  public IdNode (String i, STentry e) {
   id=i;
   entry=e;
  }
  
  public String toPrint(String s) {
   return s+"Id:" + id + "\n"+  
          entry.toPrint(s+"  ");
  }
  
}  