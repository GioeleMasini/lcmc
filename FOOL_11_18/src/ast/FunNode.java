package ast;

import java.util.ArrayList;

public class FunNode implements Node {

  private String id;
  private Node type;
  private ArrayList<Node> parlist;
  private ArrayList<Node> declist;
  private Node exp;
  
  public FunNode (String i, Node t) {
    id=i;
    type=t;
    parlist = new ArrayList<Node>();
  }
  
  public void addPar(Node p) {
	  parlist.add(p);
  }
  
  public void addBody (ArrayList<Node> d, Node e) {
	declist=d;
	exp=e;
  }
  
  public String toPrint(String indent) {
   String paramsString = "";
   for (Node par:parlist) { paramsString += par.toPrint(indent+"  ");};
   String declstr="";
   if (declist!=null) for (Node dec:declist) {declstr+=dec.toPrint(indent+"  ");};  
   return indent+"Fun:" + id + "\n" 
                 + type.toPrint(indent+"  ")   
                 + paramsString
                 + declstr
                 + exp.toPrint(indent+"  ") ; 
  }
  
}  