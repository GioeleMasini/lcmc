// $ANTLR 3.5.2 /home/osboxes/workspace/FOOL_12_02/FOOL.g 2017-04-11 22:53:24

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLLexer extends Lexer {
	public static final int EOF=-1;
	public static final int ASS=4;
	public static final int BOOL=5;
	public static final int CLPAR=6;
	public static final int COLON=7;
	public static final int COMMA=8;
	public static final int COMMENT=9;
	public static final int CRPAR=10;
	public static final int ELSE=11;
	public static final int EQ=12;
	public static final int ERR=13;
	public static final int FALSE=14;
	public static final int FUN=15;
	public static final int ID=16;
	public static final int IF=17;
	public static final int IN=18;
	public static final int INT=19;
	public static final int INTEGER=20;
	public static final int LET=21;
	public static final int LPAR=22;
	public static final int PLUS=23;
	public static final int PRINT=24;
	public static final int RPAR=25;
	public static final int SEMIC=26;
	public static final int THEN=27;
	public static final int TIMES=28;
	public static final int TRUE=29;
	public static final int VAR=30;
	public static final int WHITESP=31;

	int lexicalErrors=0;


	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public FOOLLexer() {} 
	public FOOLLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/home/osboxes/workspace/FOOL_12_02/FOOL.g"; }

	// $ANTLR start "SEMIC"
	public final void mSEMIC() throws RecognitionException {
		try {
			int _type = SEMIC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:159:7: ( ';' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:159:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMIC"

	// $ANTLR start "COLON"
	public final void mCOLON() throws RecognitionException {
		try {
			int _type = COLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:160:7: ( ':' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:160:9: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLON"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:161:7: ( ',' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:161:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			int _type = EQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:162:4: ( '==' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:162:6: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQ"

	// $ANTLR start "ASS"
	public final void mASS() throws RecognitionException {
		try {
			int _type = ASS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:163:5: ( '=' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:163:7: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASS"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:164:6: ( '+' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:164:8: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "TIMES"
	public final void mTIMES() throws RecognitionException {
		try {
			int _type = TIMES;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:165:7: ( '*' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:165:9: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TIMES"

	// $ANTLR start "INTEGER"
	public final void mINTEGER() throws RecognitionException {
		try {
			int _type = INTEGER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:166:9: ( ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* ) | '0' )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='-'||(LA3_0 >= '1' && LA3_0 <= '9')) ) {
				alt3=1;
			}
			else if ( (LA3_0=='0') ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// /home/osboxes/workspace/FOOL_12_02/FOOL.g:166:11: ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* )
					{
					// /home/osboxes/workspace/FOOL_12_02/FOOL.g:166:11: ( '-' )?
					int alt1=2;
					int LA1_0 = input.LA(1);
					if ( (LA1_0=='-') ) {
						alt1=1;
					}
					switch (alt1) {
						case 1 :
							// /home/osboxes/workspace/FOOL_12_02/FOOL.g:166:12: '-'
							{
							match('-'); 
							}
							break;

					}

					// /home/osboxes/workspace/FOOL_12_02/FOOL.g:166:17: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					// /home/osboxes/workspace/FOOL_12_02/FOOL.g:166:18: ( '1' .. '9' ) ( '0' .. '9' )*
					{
					if ( (input.LA(1) >= '1' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					// /home/osboxes/workspace/FOOL_12_02/FOOL.g:166:28: ( '0' .. '9' )*
					loop2:
					while (true) {
						int alt2=2;
						int LA2_0 = input.LA(1);
						if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
							alt2=1;
						}

						switch (alt2) {
						case 1 :
							// /home/osboxes/workspace/FOOL_12_02/FOOL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop2;
						}
					}

					}

					}
					break;
				case 2 :
					// /home/osboxes/workspace/FOOL_12_02/FOOL.g:166:43: '0'
					{
					match('0'); 
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTEGER"

	// $ANTLR start "TRUE"
	public final void mTRUE() throws RecognitionException {
		try {
			int _type = TRUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:167:6: ( 'true' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:167:8: 'true'
			{
			match("true"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TRUE"

	// $ANTLR start "FALSE"
	public final void mFALSE() throws RecognitionException {
		try {
			int _type = FALSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:168:7: ( 'false' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:168:9: 'false'
			{
			match("false"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FALSE"

	// $ANTLR start "LPAR"
	public final void mLPAR() throws RecognitionException {
		try {
			int _type = LPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:169:7: ( '(' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:169:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAR"

	// $ANTLR start "RPAR"
	public final void mRPAR() throws RecognitionException {
		try {
			int _type = RPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:170:6: ( ')' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:170:8: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAR"

	// $ANTLR start "CLPAR"
	public final void mCLPAR() throws RecognitionException {
		try {
			int _type = CLPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:171:8: ( '{' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:171:10: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLPAR"

	// $ANTLR start "CRPAR"
	public final void mCRPAR() throws RecognitionException {
		try {
			int _type = CRPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:172:7: ( '}' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:172:9: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CRPAR"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:173:5: ( 'if' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:173:7: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:174:7: ( 'then' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:174:9: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:175:7: ( 'else' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:175:9: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			int _type = PRINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:176:7: ( 'print' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:176:9: 'print'
			{
			match("print"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PRINT"

	// $ANTLR start "LET"
	public final void mLET() throws RecognitionException {
		try {
			int _type = LET;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:177:5: ( 'let' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:177:7: 'let'
			{
			match("let"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LET"

	// $ANTLR start "IN"
	public final void mIN() throws RecognitionException {
		try {
			int _type = IN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:178:4: ( 'in' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:178:6: 'in'
			{
			match("in"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IN"

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:179:5: ( 'var' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:179:7: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "FUN"
	public final void mFUN() throws RecognitionException {
		try {
			int _type = FUN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:180:5: ( 'fun' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:180:7: 'fun'
			{
			match("fun"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FUN"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:181:5: ( 'int' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:181:7: 'int'
			{
			match("int"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "BOOL"
	public final void mBOOL() throws RecognitionException {
		try {
			int _type = BOOL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:182:6: ( 'bool' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:182:8: 'bool'
			{
			match("bool"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOL"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:184:5: ( ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )* )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:184:7: ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:185:5: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= '0' && LA4_0 <= '9')||(LA4_0 >= 'A' && LA4_0 <= 'Z')||(LA4_0 >= 'a' && LA4_0 <= 'z')) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// /home/osboxes/workspace/FOOL_12_02/FOOL.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop4;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "WHITESP"
	public final void mWHITESP() throws RecognitionException {
		try {
			int _type = WHITESP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:187:10: ( ( '\\t' | ' ' | '\\r' | '\\n' )+ )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:187:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
			{
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:187:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
			int cnt5=0;
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( ((LA5_0 >= '\t' && LA5_0 <= '\n')||LA5_0=='\r'||LA5_0==' ') ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// /home/osboxes/workspace/FOOL_12_02/FOOL.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt5 >= 1 ) break loop5;
					EarlyExitException eee = new EarlyExitException(5, input);
					throw eee;
				}
				cnt5++;
			}

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHITESP"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:189:9: ( '/*' ( . )* '*/' )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:189:11: '/*' ( . )* '*/'
			{
			match("/*"); 

			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:189:16: ( . )*
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0=='*') ) {
					int LA6_1 = input.LA(2);
					if ( (LA6_1=='/') ) {
						alt6=2;
					}
					else if ( ((LA6_1 >= '\u0000' && LA6_1 <= '.')||(LA6_1 >= '0' && LA6_1 <= '\uFFFF')) ) {
						alt6=1;
					}

				}
				else if ( ((LA6_0 >= '\u0000' && LA6_0 <= ')')||(LA6_0 >= '+' && LA6_0 <= '\uFFFF')) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// /home/osboxes/workspace/FOOL_12_02/FOOL.g:189:16: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop6;
				}
			}

			match("*/"); 

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "ERR"
	public final void mERR() throws RecognitionException {
		try {
			int _type = ERR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:191:9: ( . )
			// /home/osboxes/workspace/FOOL_12_02/FOOL.g:191:11: .
			{
			matchAny(); 
			 System.out.println("Invalid char: "+getText()); lexicalErrors++; _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ERR"

	@Override
	public void mTokens() throws RecognitionException {
		// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:8: ( SEMIC | COLON | COMMA | EQ | ASS | PLUS | TIMES | INTEGER | TRUE | FALSE | LPAR | RPAR | CLPAR | CRPAR | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | INT | BOOL | ID | WHITESP | COMMENT | ERR )
		int alt7=28;
		alt7 = dfa7.predict(input);
		switch (alt7) {
			case 1 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:10: SEMIC
				{
				mSEMIC(); 

				}
				break;
			case 2 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:16: COLON
				{
				mCOLON(); 

				}
				break;
			case 3 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:22: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 4 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:28: EQ
				{
				mEQ(); 

				}
				break;
			case 5 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:31: ASS
				{
				mASS(); 

				}
				break;
			case 6 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:35: PLUS
				{
				mPLUS(); 

				}
				break;
			case 7 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:40: TIMES
				{
				mTIMES(); 

				}
				break;
			case 8 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:46: INTEGER
				{
				mINTEGER(); 

				}
				break;
			case 9 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:54: TRUE
				{
				mTRUE(); 

				}
				break;
			case 10 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:59: FALSE
				{
				mFALSE(); 

				}
				break;
			case 11 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:65: LPAR
				{
				mLPAR(); 

				}
				break;
			case 12 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:70: RPAR
				{
				mRPAR(); 

				}
				break;
			case 13 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:75: CLPAR
				{
				mCLPAR(); 

				}
				break;
			case 14 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:81: CRPAR
				{
				mCRPAR(); 

				}
				break;
			case 15 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:87: IF
				{
				mIF(); 

				}
				break;
			case 16 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:90: THEN
				{
				mTHEN(); 

				}
				break;
			case 17 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:95: ELSE
				{
				mELSE(); 

				}
				break;
			case 18 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:100: PRINT
				{
				mPRINT(); 

				}
				break;
			case 19 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:106: LET
				{
				mLET(); 

				}
				break;
			case 20 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:110: IN
				{
				mIN(); 

				}
				break;
			case 21 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:113: VAR
				{
				mVAR(); 

				}
				break;
			case 22 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:117: FUN
				{
				mFUN(); 

				}
				break;
			case 23 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:121: INT
				{
				mINT(); 

				}
				break;
			case 24 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:125: BOOL
				{
				mBOOL(); 

				}
				break;
			case 25 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:130: ID
				{
				mID(); 

				}
				break;
			case 26 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:133: WHITESP
				{
				mWHITESP(); 

				}
				break;
			case 27 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:141: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 28 :
				// /home/osboxes/workspace/FOOL_12_02/FOOL.g:1:149: ERR
				{
				mERR(); 

				}
				break;

		}
	}


	protected DFA7 dfa7 = new DFA7(this);
	static final String DFA7_eotS =
		"\4\uffff\1\36\2\uffff\1\31\2\uffff\2\44\4\uffff\6\44\2\uffff\1\31\11\uffff"+
		"\2\44\1\uffff\2\44\4\uffff\1\70\1\72\5\44\2\uffff\3\44\1\103\1\uffff\1"+
		"\104\1\uffff\2\44\1\107\1\110\1\44\1\112\1\113\1\44\2\uffff\1\115\1\44"+
		"\2\uffff\1\117\2\uffff\1\120\1\uffff\1\121\3\uffff";
	static final String DFA7_eofS =
		"\122\uffff";
	static final String DFA7_minS =
		"\1\0\3\uffff\1\75\2\uffff\1\61\2\uffff\1\150\1\141\4\uffff\1\146\1\154"+
		"\1\162\1\145\1\141\1\157\2\uffff\1\52\11\uffff\1\165\1\145\1\uffff\1\154"+
		"\1\156\4\uffff\2\60\1\163\1\151\1\164\1\162\1\157\2\uffff\1\145\1\156"+
		"\1\163\1\60\1\uffff\1\60\1\uffff\1\145\1\156\2\60\1\154\2\60\1\145\2\uffff"+
		"\1\60\1\164\2\uffff\1\60\2\uffff\1\60\1\uffff\1\60\3\uffff";
	static final String DFA7_maxS =
		"\1\uffff\3\uffff\1\75\2\uffff\1\71\2\uffff\1\162\1\165\4\uffff\1\156\1"+
		"\154\1\162\1\145\1\141\1\157\2\uffff\1\52\11\uffff\1\165\1\145\1\uffff"+
		"\1\154\1\156\4\uffff\2\172\1\163\1\151\1\164\1\162\1\157\2\uffff\1\145"+
		"\1\156\1\163\1\172\1\uffff\1\172\1\uffff\1\145\1\156\2\172\1\154\2\172"+
		"\1\145\2\uffff\1\172\1\164\2\uffff\1\172\2\uffff\1\172\1\uffff\1\172\3"+
		"\uffff";
	static final String DFA7_acceptS =
		"\1\uffff\1\1\1\2\1\3\1\uffff\1\6\1\7\1\uffff\2\10\2\uffff\1\13\1\14\1"+
		"\15\1\16\6\uffff\1\31\1\32\1\uffff\1\34\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1"+
		"\10\2\uffff\1\31\2\uffff\1\13\1\14\1\15\1\16\7\uffff\1\32\1\33\4\uffff"+
		"\1\17\1\uffff\1\24\10\uffff\1\26\1\27\2\uffff\1\23\1\25\1\uffff\1\11\1"+
		"\20\1\uffff\1\21\1\uffff\1\30\1\12\1\22";
	static final String DFA7_specialS =
		"\1\0\121\uffff}>";
	static final String[] DFA7_transitionS = {
			"\11\31\2\27\2\31\1\27\22\31\1\27\7\31\1\14\1\15\1\6\1\5\1\3\1\7\1\31"+
			"\1\30\1\11\11\10\1\2\1\1\1\31\1\4\3\31\32\26\6\31\1\26\1\25\2\26\1\21"+
			"\1\13\2\26\1\20\2\26\1\23\3\26\1\22\3\26\1\12\1\26\1\24\4\26\1\16\1\31"+
			"\1\17\uff82\31",
			"",
			"",
			"",
			"\1\35",
			"",
			"",
			"\11\41",
			"",
			"",
			"\1\43\11\uffff\1\42",
			"\1\45\23\uffff\1\46",
			"",
			"",
			"",
			"",
			"\1\53\7\uffff\1\54",
			"\1\55",
			"\1\56",
			"\1\57",
			"\1\60",
			"\1\61",
			"",
			"",
			"\1\63",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\64",
			"\1\65",
			"",
			"\1\66",
			"\1\67",
			"",
			"",
			"",
			"",
			"\12\44\7\uffff\32\44\6\uffff\32\44",
			"\12\44\7\uffff\32\44\6\uffff\23\44\1\71\6\44",
			"\1\73",
			"\1\74",
			"\1\75",
			"\1\76",
			"\1\77",
			"",
			"",
			"\1\100",
			"\1\101",
			"\1\102",
			"\12\44\7\uffff\32\44\6\uffff\32\44",
			"",
			"\12\44\7\uffff\32\44\6\uffff\32\44",
			"",
			"\1\105",
			"\1\106",
			"\12\44\7\uffff\32\44\6\uffff\32\44",
			"\12\44\7\uffff\32\44\6\uffff\32\44",
			"\1\111",
			"\12\44\7\uffff\32\44\6\uffff\32\44",
			"\12\44\7\uffff\32\44\6\uffff\32\44",
			"\1\114",
			"",
			"",
			"\12\44\7\uffff\32\44\6\uffff\32\44",
			"\1\116",
			"",
			"",
			"\12\44\7\uffff\32\44\6\uffff\32\44",
			"",
			"",
			"\12\44\7\uffff\32\44\6\uffff\32\44",
			"",
			"\12\44\7\uffff\32\44\6\uffff\32\44",
			"",
			"",
			""
	};

	static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
	static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
	static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
	static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
	static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
	static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
	static final short[][] DFA7_transition;

	static {
		int numStates = DFA7_transitionS.length;
		DFA7_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
		}
	}

	protected class DFA7 extends DFA {

		public DFA7(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 7;
			this.eot = DFA7_eot;
			this.eof = DFA7_eof;
			this.min = DFA7_min;
			this.max = DFA7_max;
			this.accept = DFA7_accept;
			this.special = DFA7_special;
			this.transition = DFA7_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( SEMIC | COLON | COMMA | EQ | ASS | PLUS | TIMES | INTEGER | TRUE | FALSE | LPAR | RPAR | CLPAR | CRPAR | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | INT | BOOL | ID | WHITESP | COMMENT | ERR );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA7_0 = input.LA(1);
						s = -1;
						if ( (LA7_0==';') ) {s = 1;}
						else if ( (LA7_0==':') ) {s = 2;}
						else if ( (LA7_0==',') ) {s = 3;}
						else if ( (LA7_0=='=') ) {s = 4;}
						else if ( (LA7_0=='+') ) {s = 5;}
						else if ( (LA7_0=='*') ) {s = 6;}
						else if ( (LA7_0=='-') ) {s = 7;}
						else if ( ((LA7_0 >= '1' && LA7_0 <= '9')) ) {s = 8;}
						else if ( (LA7_0=='0') ) {s = 9;}
						else if ( (LA7_0=='t') ) {s = 10;}
						else if ( (LA7_0=='f') ) {s = 11;}
						else if ( (LA7_0=='(') ) {s = 12;}
						else if ( (LA7_0==')') ) {s = 13;}
						else if ( (LA7_0=='{') ) {s = 14;}
						else if ( (LA7_0=='}') ) {s = 15;}
						else if ( (LA7_0=='i') ) {s = 16;}
						else if ( (LA7_0=='e') ) {s = 17;}
						else if ( (LA7_0=='p') ) {s = 18;}
						else if ( (LA7_0=='l') ) {s = 19;}
						else if ( (LA7_0=='v') ) {s = 20;}
						else if ( (LA7_0=='b') ) {s = 21;}
						else if ( ((LA7_0 >= 'A' && LA7_0 <= 'Z')||LA7_0=='a'||(LA7_0 >= 'c' && LA7_0 <= 'd')||(LA7_0 >= 'g' && LA7_0 <= 'h')||(LA7_0 >= 'j' && LA7_0 <= 'k')||(LA7_0 >= 'm' && LA7_0 <= 'o')||(LA7_0 >= 'q' && LA7_0 <= 's')||LA7_0=='u'||(LA7_0 >= 'w' && LA7_0 <= 'z')) ) {s = 22;}
						else if ( ((LA7_0 >= '\t' && LA7_0 <= '\n')||LA7_0=='\r'||LA7_0==' ') ) {s = 23;}
						else if ( (LA7_0=='/') ) {s = 24;}
						else if ( ((LA7_0 >= '\u0000' && LA7_0 <= '\b')||(LA7_0 >= '\u000B' && LA7_0 <= '\f')||(LA7_0 >= '\u000E' && LA7_0 <= '\u001F')||(LA7_0 >= '!' && LA7_0 <= '\'')||LA7_0=='.'||LA7_0=='<'||(LA7_0 >= '>' && LA7_0 <= '@')||(LA7_0 >= '[' && LA7_0 <= '`')||LA7_0=='|'||(LA7_0 >= '~' && LA7_0 <= '\uFFFF')) ) {s = 25;}
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 7, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
